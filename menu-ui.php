
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Em2016</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fichier</span></a>
          <ul class="dropdown-menu">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
             <li class="divider"></li>
          <?php if (intval($POWER) >= 2 ) {?>  <li><a href="history.php"><i class="fa fa-history"></i> Historique</a></li><?php }?>
            <li><a href="searchform.php"><i class="fa fa-search"></i> Recherche</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="index.php"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
            <li><a href="javascript:window.close()"><i class="fa fa-times"></i> Quiter</a></li>

          </ul>
        </li>



        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">impression</span></a>
          <ul class="dropdown-menu">

              <li><a href="mphome.php"><i class="fa fa-cubes"></i> Imprimer </a></li>
              <li><a href="mpliste.php"><i class="fa fa-user"></i> Liste des documments</a></li>
              <li><a href="mpparam.php"><i class="fa fa-user"></i> Options</a></li>


                    <li class="divider"></li>
                    <li><a href="ABS.php"><i class="fa fa-user"></i> Abonnements</a></li>
                    <li><a href="cards.php"><i class="fa fa-user"></i> Total des cartes</a></li>
                    <li><a href="rfhome.php"><i class="fa fa-card"></i> Payements Par cartes</a></li>


          </ul>
        </li>







        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Liste</span></a>
          <ul class="dropdown-menu">
            <?php if (intval($POWER) >= 2 ) {?>
              <li><a href="pstock.php"><i class="fa fa-cubes"></i> Stock</a></li>
              <li><a href="fournisseurs.php"><i class="fa fa-user"></i> Fournisseurs</a></li>
              <li><a href="clients.php"><i class="fa fa-users"></i> Clients</a></li>
              <li><a href="promotions.php"><i class="fa fa-flag"></i> Promotions</a></li>
                    <li class="divider"></li>
        <?php }?>
              <li><a href="listefa.php"><i class="fa fa-history"></i> Bons de réception</a></li>
              <li><a href="listefv.php"><i class="fa fa-history"></i> Bons de livraison</a></li>
              <li><a href="userlist.php"><i class="fa fa-users"></i> Utilisateurs</a></li>

          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Achat</span></a>
          <ul class="dropdown-menu">
            <li><a href="fa.php"><i class="fa fa-file-text-o"></i> Bon de réception </a></li>
                <li><a href="listefa.php"><i class="fa fa-history"></i> Historique d'achat</a></li>
       <?php if (intval($POWER) >= 2 ) {?><li class="divider"></li>   <li><a href="frel.php"><i class="fa fa-file-o"></i> Relevé des fournisseurs</a></li>
      <?php }?>
          </ul>
        </li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vente</span></a>
          <ul class="dropdown-menu">
            <li><a href="comptoir.php"><i class="fa fa-desktop"></i> Vente au comptoir</a></li>

                <li><a href="fv.php"><i class="fa fa-file-text-o"></i> Bon de livraison / Facture</a></li>
            <li><a href="listefv.php"><i class="fa fa-history"></i> Historique de vente</a></li>
        <li><a href="return.php"><i class="fa fa-eraser"></i> Retours</a></li>
  <li class="divider"></li>
  <?php if (intval($POWER) >= 2 ) {?> <li><a href="total.php"><i class="fa fa-calendar-o"></i> Recettes</a></li><li class="divider"></li><?php }?>

   <?php if (intval($POWER) >= 2 ) {?>     <li><a href="crel.php"><i class="fa fa-file"></i> Relevé des clients</a></li><?php }?>
        <li><a href="facture_simple.php"><i class="fa fa-file-o"></i> Facture simple</a></li>

          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Action</span></a>
          <ul class="dropdown-menu">
            <?php if (intval($POWER) >= 2 ) {?>    <li><a href="inventory.php"><i class="fa fa-list"></i> INVENTAIRE</a></li><?php }?>
             <li><a href="vers.php"><i class="fa fa-eraser"></i> VERSEMENTS</a></li>
         <?php if (intval($POWER) >= 2 ) {?>    <li><a href="dep.php"><i class="fa fa-money"></i> DEPENSES</a></li><?php }?>
             <li><a href="bcp.php"><i class="fa fa-barcode"></i> IMPRIMMER CODE-BARRE</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Outils</span></a>
          <ul class="dropdown-menu">
            <?php if (intval($POWER) >= 2 ) {?>    <li><a href="userlist.php"><i class="fa fa-users"></i>UTILISATEURS</a></li><?php }?>
            <?php if (intval($POWER) >= 3 ) {?>  <li><a href="datamanagment.php" target="blank"><i class="fa fa-database"></i> Gestion des données</a></li><?php }?>
            <?php if (intval($POWER) >= 2 ) {?>    <li><a href="addons.php"><i class="fa fa-puzzle-piece"></i>ADDONS</a></li>  <li class="divider"></li><?php }?>

                 <li><a href="javascript:phpdesktop.ToggleFullscreen()"><i class="fa fa-arrows-alt"></i> PLEIN ECRAN</a></li>
                 <li><a href="settings.php"><i class="fa fa-wrench"></i> PARAMETRES</a></li>
          </ul>
        </li>



      </ul>




    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
