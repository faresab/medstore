<?php
error_reporting(0);
include 'user.php';
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PROMOTIONS</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
<link href="add.css" rel="stylesheet" media="screen">
<style media="screen">
*{-webkit-user-select : none;}
body {background: #F7FBF7;overflow: hidden;}
.gr {display: block;background: #fff;border:1px solid #E8F5E9 ;min-height: 300px;margin:10px;overflow-y: auto;overflow-x: hidden}
.grTop {padding:10px;border-bottom: 1px solid  #E8F5E9; }
.promo {height:1.2cm;line-height: calc(1.2cm - 20px);padding:10px;border-bottom:1px solid #E8F5E9;cursor: pointer;font-size: 16px;font-weight: 700;color: #1B5E20;}
.promo:hover , .promo:active ,.promo.active  {
  background: #4CAF50;color:#fff;
}
h0 {background: #1B5E20;border:none;}
</style>

  </head>


  <body>
    <?php include 'menu-ui.php';?>
<h0>PROMOTIONS </h0>
<div class="container-fluid">
<div class="row">
<div class="col-xs-3" style="padding-right:0;margin:0;">
<div class="gr groupes">

</div>
</div>


<div class="col-xs-9" style="padding-left:0;margin:0;">
  <div class="gr">
<div class="grTop">
  <div class="row">
    <div class="col-xs-9">
<input type="text" class="form-control sugg" placeholder="recherche" id="inputprod" />
<div class="suggestionsBox" id="suggestions" style="display: none;">
<div class="suggestionList " id="autoSuggestionsList"></div></div>

    </div>

    <div class="col-xs-3">
<button type="button" class="btn btn-success btn-block" onclick="savePromo()"><span class="glyphicon glyphicon-floppy-disk"></span> SAUVEGARDER</button>
    </div>





  </div>
</div>


<div style="padding:10px">

  <div class="row">

    <div class="col-xs-3">
<input type="date" class="form-control" name="dateBegin" value="">    </div>


    <div class="col-xs-3">
<input type="date" class="form-control" name="dateEnd" value="">    </div>

<div class="col-xs-3 addfam">
  </div>
  <div class="col-xs-3">
    <button class="btn btn-block btn-info addfambtn" onclick="addfam()">Ajouter</button>
    </div>

  </div>
  <br>
<table class="table table-bordered">
<tr><th>ART</th><th width="30%">PRIX</th><th width="30%">NV PRIX</th><th width="10%"><input type="checkbox" onchange="toggleAll(this)"  checked/></th><th width="10%"><span class="glyphicon glyphicon-trash"></span></th></tr>
<tbody id="promoField"></tbody>
</table>



</div>



  </div>

</div>


</div>



</div>

  </body>
  <script  src="assets/js/jquery.js" type="text/javascript" ></script>
  <script  src="assets/js/sugg.js" type="text/javascript" ></script>
  <script src="assets/js/promotions.js" ></script>
  <script src="dist/js/bootstrap.min.js"></script>
</html>
