<?php
error_reporting(0);
include 'user.php';

$bc = $pr = $prf =  $qt = $ref = null;

if (isset($_GET['bc'])) {
$bc = $_GET['bc'];
}
if (isset($_GET['pr'])) {
$pr = $_GET['pr'];
$prf = number_format($pr, 2, ',', ' ');
}
if (isset($_GET['qt'])) {
$qt = $_GET['qt'];
}
if (isset($_GET['ref'])) {
$ref = $_GET['ref'];
}

$INFOS = array();
include 'config.php';
$result = $file_db->query("SELECT * FROM settings WHERE (key == 'Cname') OR ( key='Cadress' ) OR ( key='Cphone' ) ORDER BY id ASC ");
foreach($result as $row) {
$INFOS[] = $row['value'];
}

$sub = '<div align="center">'.$INFOS[0].' - '.$INFOS[1].'</div>';

if ($qt >= 100 ) $qt = 100;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EM 2015</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet" media="screen">
    <!-- Bootstrap theme
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet"> -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template 
    <link href="theme.css" rel="stylesheet">-->
    <link href="add.css" rel="stylesheet">
<style type="text/css" media="print">
   html,body,.jumbotronmid,.container{padding:0 !important;}
   .navbar , .jumbotrontop { display: none !important; }
.jumbotronmid  , .container{padding :0;border:none;margin:0;
box-shadow:none;
}
   .cbpar {width:3.6cm;border:1px solid #AAAAAA;display:inline-block;float:left;margin-right:-1px;margin-bottom: -1px}
   .cb{margin:0 auto;display:block;}
   .cbpar span {font:normal 10px sans serif;}
.sub {background-color: #fff !important;color:#333;margin:3px 0 0 0;
font-size: 11px;
}
   </style>
   <style media="screen">
.cbpar {width:180px;border:1px solid #AAAAAA;float:left;
padding:5px 5px;display:inline-block;margin:0 -1px;display: none;
}
.cbpar:first-child{display:block;}
.jumbotronmid  , .container{padding :0 ;border:none;display:block;margin:0 auto;height:auto;}
.cbpar span {font:bold 14px sans serif;margin:0 auto;}	
.sub {background-color: #333;color:#fff;margin:3px -5px -5px -5px;}
	</style>
  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container-fluid">
<?php if ($qt >=1 && (isset($_GET['print']))) {?>	
	<body Onload="javascript:window.print();">
	
<?php } else {
print "<body>";
}
include 'menu-ui.php';

?>

    </div>

    <div class="container-fluid   no-print">

	

	
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotrontop">
<div class='row'>	  
	<form method="get" action="bcp.php">  

	<div class="col-md-4 col-lg-4 ">
 <label>Code a barre</label>
<input type="text" class="form-control code" name="bc"  value="<?php print $bc;?>">


</div>	
			
	<div class="col-md-2 col-lg-2 ">
	<label>Prix</label>
			    <input type="text" class="form-control prix" name="pr" value="<?php print $pr;?>">

</div>	  
	<div class="col-md-2 col-lg-2 ">
	<label>Reference</label>
			    <input type="text" class="form-control reference" name="ref" value="<?php print $ref;?>">

</div>	
<div class="col-md-2 col-lg-2 ">
	<label>Quantité</label>
			    <input type="text" class="form-control reference" name="qt" value="<?php print $qt;?>">
</div>
	<div class="col-md-2 col-lg-2 ">
	<label>imprimmer</label>
<button class="btn btn-default" style="vertical-align:top;" type="submit" name="print" value="yes"><i class="fa fa-print"></i> IMPRIMMER</button>
</div>
	</form>  
	  
	   </div>
	   </div>
	   
	   
	   <div class="jumbotronmid">


	   

<?php 
if (isset($bc)) {
for ($i=1;$i<= $qt;$i++) { ?>
<div class='cbpar' ><span><?php print $ref;?></span>
<span class='pull-right'><?php print $prf;?></span><div class='cb'></div>
<div class="sub"><?php print $sub;?></div>
</div>
<?php } 

} ?>


	  
	  
	 </div>  
	 </div>  <!-- /container -->

	 



	 

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/jquery-barcode.min.js"></script>
	
		<script>
	
	$(document).ready(function (){
	<?php if (($qt >= 1))  { ?>
	$('.cb').each( function (){
	$(this).barcode("<?php print $bc;?>", "code128",{barHeight:40 ,barWidth:1 , fontSize :0 ,bgColor:"white"})
	});
	<?php } ;?>
	
	});
	</script>
    <script src="dist/js/bootstrap.min.js"></script>
      <script src="assets/js/ALL.js"></script>

  </body>
</html>
