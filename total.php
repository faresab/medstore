<?php
error_reporting(0);
include 'user.php';
?>
<?php
error_reporting(0);
date_default_timezone_set('Africa/Algiers');
$date = date('Y-m-d');
$fadate = fadate($date);

function fadate ($date) {
$ex = explode ('-',$date);
$r = ($ex[0] * 10000) + ($ex[1] * 100 ) + $ex[2];
return $r;
}



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>EM14</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
	    <link href="add.css" rel="stylesheet">
 <script src="assets/js/Chart.js"></script>

    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/daterangepicker-bs3.css" rel="stylesheet">
<style>
body{background-color:#DFEEF4;}
.jumbotronmid {min-height:700px;}
canvas{
        width: 100% !important;
    }
    .tot,.totp{
      font: Bold 22px 'Play-Regular';
    }
    table tr td{font-family: 'Roboto'}
    tr.red th {background-color: #FF1744;color:#fff;font-size: 18px}

.nav-tabs > li > a {
color:#555555;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
  color: #FF1744;
}

.btn-info {
  background-color: #FF1744;border-color: #FF1744;
}

.btn-info.active , .btn-info:hover, .btn-info:focus {background-color: #FB0032;border-color: #FB0032;}
tr.green {background: #DCEDC8 !important}
td.orange {background: #FFECB3 !important}
 tr.orange {background: #FFE082 !important}
td.red {background: #FFCDD2 !important}
 tr.red {background: #EF9A9A !important}
 td h3 {margin-top: 0}
 td[right] {text-align: right}
 td[ptwo] {width:20%}
</style>
<style media="print">
.so {display: none!important;}
.jumbotrontop ,.jumbotronmid {padding:0;border:none;}



</style>


  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container-fluid no-print">
<?php include 'menu-ui.php';?>

    </div>

    <div class="container-fluid  no-print">




      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotrontop bg-info">
<div class="row">
	<div class="col-md-6 col-lg-6">

<div class="input-group">
    <input type="text" class="form-control" id="reportrange" placeholder="Date" data-start="<?php print $fadate;?>" data-end="<?php print $fadate;?>">
  <span class="input-group-btn">
        <button class="so btn btn-info changedate" type="button">Changer</button>
      </span>
    </div>








	</div>

	   </div>
	   </div>


	   <div class="jumbotronmid">

	   <div class="row">




	   </div>
	 </div>
	 </div>  <!-- /container -->


<div id="null"></div>





   <script src="assets/js/jquery.js"></script>
    <script src="assets/js/daterangepicker.js"></script>
    <script src="assets/js/moment.int.js"></script>
    <script src="assets/js/total2.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
      <script src="assets/js/ALL.js"></script>

  <?php include "plug.php";?>
</body>
</html>
