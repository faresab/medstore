<?php
error_reporting(0);
include 'user.php';


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LISTE DES COURS</title>
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/main.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
    <link href="add.css" rel="stylesheet" media="screen">
  </head>

<style media="screen">
body {padding-top: 40px}
.form-control{border-radius: 0;height: 28px;padding: 3px 12px;}
.form-control.half{width:50%}
.form-control:focus{border-color: #757575;box-shadow: none;}
.set {width: 100%}
.set tr td {padding: 5px;}
</style>

  <body>

<?php include 'menu-ui.php' ?>


<div class="container-fluid">
<div class="row">
<div class="col-md-12">
  <div class="well well-sm">

  <table>
     <a   onclick="setPrinters()"  class="btn btn-default">METTRE A JOUR</a>
  </table>
</div>
</div>

<div class="col-xs-6">
<div class="well">
<h1>IMPRIMANTES</h1>
<table class="set">

<tr><td>#1</td><td><input type="text" name="p1" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p1o" value="" class="printeron"></td></tr>
<tr><td>#2</td><td><input type="text" name="p2" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p2o" value="" class="printeron"></td></tr>
<tr><td>#3</td><td><input type="text" name="p3" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p3o" value="" class="printeron"></td></tr>
<tr><td>#4</td><td><input type="text" name="p4" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p4o" value="" class="printeron"></td></tr>
<tr><td>#5</td><td><input type="text" name="p5" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p5o" value="" class="printeron"></td></tr>
<tr><td>#6</td><td><input type="text" name="p6" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p6o" value="" class="printeron"></td></tr>
<tr><td>#7</td><td><input type="text" name="p7" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p7o" value="" class="printeron"></td></tr>
<tr><td>#8</td><td><input type="text" name="p8" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p8o" value="" class="printeron"></td></tr>
<tr><td>#9</td><td><input type="text" name="p9" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p9o" value="" class="printeron"></td></tr>
<tr><td>#10</td><td><input type="text" name="p10" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p10o" value="" class="printeron"></td></tr>
<tr><td>#11</td><td><input type="text" name="p11" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p11o" value="" class="printeron"></td></tr>
<tr><td>#12</td><td><input type="text" name="p12" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p12o" value="" class="printeron"></td></tr>
<tr><td>#13</td><td><input type="text" name="p13" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p13o" value="" class="printeron"></td></tr>
<tr><td>#14</td><td><input type="text" name="p14" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p14o" value="" class="printeron"></td></tr>
<tr><td>#15</td><td><input type="text" name="p15" value="" class=" printername form-control" ></td><td><input type="checkbox" name="p15o" value="" class="printeron"></td></tr>


</table>
</div>














</div>

<div class="col-xs-6">

  <div class="well">
  <h1>INFORMATIONS</h1>
  <table class="set" >
  <tr><td>NOM</td><td><input type="text" name="name" value="" class="form-control" ></td></tr>
  <tr><td>ADRESSE</td><td><input type="text" name="adress" value="" class="form-control" ></td></tr>
  <tr><td>TEL</td><td><input type="text" name="tel" value="" class="form-control" ></td></tr>
  </table>

  </div>




  <div class="well">
  <h1>PRIX</h1>
  <table class="set" >
  <tr><td>PRIX</td><td><input type="number" name="price" value="" class="form-control" ></td></tr>
  </table>

  </div>

  <div class="well">
  <h1>IMPRIMANTE PAR DEFAUT</h1>
  <table class="set" >
  <tr><td>NOM D'IMPRIMANTE</td><td><input type="text" name="mainprinter" value="" class="form-control" ></td></tr>
  </table>

  </div>



</div>

</div>
</div>




<script src="js/settings.js" charset="utf-8"></script>

<script src="assets/js/jquery.js"></script>

<script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
