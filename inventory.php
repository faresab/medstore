<?php
error_reporting(0);
include 'user.php';
include 'ajax/safe.php';
include $db;
$stock = 'var stock = new Array();';
$xcount = 0;

$result_one = $file_db->query("SELECT * from stock ");
foreach($result_one as $row) {
$stock.= 'stock['.$xcount.'] = ["'.$row['ID'].'", "'.$row['REF'].'", "'.$row['BARCODE'].'","'.$row['QT'].'","'.$row['PRICE_A'].'","'.$row['QTMIN'].'"];';
$xcount++;
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EM 2015</title>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">

        <style media="screen">
html,body {
	height:100%;

}

.orange {color:orange;}
.red {color:red;}
.green {color:green;}



#res {overflow-y: scroll;width:100%;min-height: 500px;height: 536px;
border:1px solid rgba(0,0,0,0.5);background-color: #fff;
  box-shadow: 0 4px 2px -2px rgba(0,0,0,0.4);}
#res::-webkit-scrollbar-track {border-radius: 0px; background-color: #FFF;}
#res::-webkit-scrollbar{height: 10px;width: 10px;background-color: #FFF; }
#res::-webkit-scrollbar-thumb{border-radius: 0px;background-color: #292C2F;}
table.rel{-webkit-user-select: none;width:100%;background-color: #fff; width:100%;cursor:pointer;font-family: 'Segoe UI', Tahoma, sans-serif;font-size: 14px;}
table.rel tr{margin:0;padding: 0;height:28px;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:1px;}
table.rel tr th{background-color:#EFEFEF;text-align: center;padding:5px; }
table.rel tr th {border-top: none}
table.rel tr td:first-child,table.rel tr th:first-child {border-left: none}
table.rel tr[data-pid]:hover {background-color: #E6EFF7 }

td[right] {text-align:right}
td[center] {text-align:center}
/*td[bold] {font-weight: bold}*/
table.rel tr td.warning{background-color:#FBD560;}
table.rel tr td.danger{background-color:#FF5B5B; }

table.rel tr.danger,table.rel tr.danger:hover {background-color:#FFC1C1}
table.rel tr.success,table.rel tr.success:hover {background-color:#B7FFB7}

.upper {text-transform: uppercase;}
*{border-radius: 0}
.visible-print-block {display: none!important;}
.control {display:block;height:50px;padding:10px;background-color: #272822;color:#5F9AFC;}
td[data-pointer] ,td[data-res]{font-weight: bold;color:#0349BE;}
td.red[data-res] {color:red;}
        </style>



            <link href="assets/css/inv.css" rel="stylesheet" media="print">

  </head>

  <body>
	      <div class="container-fluid">
<?php include 'menu-ui.php';?>

    </div>
<h0 class=" screen"><i class="fa fa-file-text-o"></i>INVENTAIRE </h0>
    <div class="visible-print-block" align="center"><h1>FREQUENCY</h1></div>
<div class="control screen">
<div class="row">

<div class="col-md-3">
<a href="#" onclick="window.print()" class="btn btn-default btn-block"><i class="fa fa-print"></i></a>
</div>
<div class="col-md-3">
<a class="btn btn-primary btn-block" data-toggle="modal" href='#inv-modal'>Terminaux</a>
</div>

<div class="col-md-5">

</div>
<div class="col-md-1">
<i class="fa fa-spin fa-spinner progress" style="font-size:20px;display:none"></i>
</div>

</div>
</div>


<div id="res">
<table class="rel" id="stocklist">

</table>

</div>



<div class="modal fade" id="inv-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Terminaux</h4>
      </div>
      <div class="modal-body">

<textarea class="form-control pda" rows='5'></textarea>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary runInventory">Démarer</button>
      </div>
    </div>
  </div>
</div>



<script>
 <?php  print $stock;?>
</script>
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
  <script src="assets/js/inv.js"></script>

  <?php include "plug.php";?>
</body>
</html>
