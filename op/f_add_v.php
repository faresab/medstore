<?php

include "safe.php";
include $db;
$fid = $_GET['fid'];
$fourns = Array();
$result_one = $file_db->query("SELECT * from fournisseurs ");
foreach($result_one as $row) {
	$id = $row['ID'];
$fourns[$id] = $row['NAME'];
	}

$fournName = $fourns[$fid]; 

$today = date('Y-m-d');
?>

<!DOCTYPE html>
<html>
    <meta charset="utf-8">

<head>
	<title>AJOUTER UN VERSEMENT</title>
  
	    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/css/animate.min.css" rel="stylesheet">
    <link href="../add.css" rel="stylesheet">
<style>body{padding-top: 0}</style>
</head>
<body>
<h0>AJOUTER UN VERSEMENT >> <?php print $fournName;?></h0><br>
<div class="container">
<div class="well">
<form id="addform">

<input type="hidden" name="fid" value="<?php print $fid;?>"/>

<table width="100%">
<tr><td width="35%">
DATE DE VERSEMENT</td>
<td><input type="date" name="date" class="form-control" required value="<?php print $today ;?>" /></td></tr>

<tr><td>DESCRIPTION</td>
<td><input type="text" name="descr" class="form-control" required/> 
</td></tr>

<tr><td>VALEUR</td>
<td><input type="text" name="value" class="form-control number" required/> 
</td></tr>

<tr><td colspan="2"><br>
<button type="submit" class="btn btn-primary btn-block save">Ajouter</button>
</td></tr>

</table>
</form>
</div>
</div>

<div class="x"></div>

</body>
    <script src="../assets/js/jquery.js"></script>
 <script src="../assets/js/jquery.number.min.js"></script>
 <script type="text/javascript">
$(function(){
	$('.number').number( true, 2,'.',' ' );
$('form#addform').on('submit',function(){
  save();return false;
});
});

      added = false;


function save() {
    $('.save').toggleClass('btn-primary btn-default').attr('disabled','disabled');
$.ajax({
      type: 'POST',
      url: 'f_add_v_submit.php',
      data: $('#addform').serialize(),
      success: function(data) {
  $('.save').toggleClass('btn-primary btn-default').removeAttr('disabled');
added = true;
window.close();

      }
    });
    return false;
}


 </script>

      <script>
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.getlist('');
        window.opener.loadinfos('<?php print $fid;?>');
       if (added) window.opener.alert('Versement a ete ajouté');

    }
</script>
</html>