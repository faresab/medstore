<?php

include "safe.php";
include $db;
$fid = $_GET['fid'];
$result_one = $file_db->query("SELECT * from clients WHERE ID='$fid'");
foreach($result_one as $row) {
  $ID = $row['ID'];
  $TYPE = $row['TYPE'];
  $NAME = $row['NAME'];
  $ADRESS = $row['ADRESS'];
  $WILAYA = $row['WILAYA'];
  $TEL1 = $row['TEL1'];
  $TEL2 = $row['TEL2'];
  $FAX = $row['FAX'];
	$SOLDE_INIT = $row['SOLDE_INIT'];

	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>MODIFICATION</title>
    <meta charset="utf-8">

	    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/css/animate.min.css" rel="stylesheet">
    <link href="../add.css" rel="stylesheet">
<style>body{padding-top: 0}</style>
</head>
<body>
<h0 class="green"> <?php print $NAME;?> >> MODIFIER</h0><br>
<div class="container">
<div class="well">

<form id="addform" method="POST" >
    <input type="hidden" name="fid" value="<?php print $ID;?>"/>
    NOM
    <input type="text" name="name" class="form-control" placeholder="Nom" required=""  value="<?php print $NAME;?>">

    TYPE
    <select class="form-control" name="type">
    <option value="1">Détaillant</option>
    <option value="2">Revendeur</option>
    <option value="3">Grossiste</option>
    </select>


ADRESSE
  <input type="text" name="adress" class="form-control" placeholder="Adresse"  value="<?php print $ADRESS;?>">


<div class="row">
<div class="col-md-6">
WILAYA
<input type="text" name="wilaya" class="form-control" placeholder="WILAYA"  value="<?php print $WILAYA;?>">
</div>
<div class="col-md-6">
FAX
<input type="text" name="fax" class="form-control" placeholder="FAX"  value="<?php print $FAX;?>">
</div>
</div>




<div class="row">
<div class="col-md-6">
TEL1
<input type="text" name="tel1" class="form-control" placeholder="TEL 1"  value="<?php print $TEL1;?>">
</div>

<div class="col-md-6">
TEL 2
<input type="text" name="tel2" class="form-control" placeholder="TEL 2"  value="<?php print $TEL2;?>">
</div>
</div>


SOLDE INITIAL
<input type="number" name="solde_init" class="form-control" placeholder="Solde initial"  value="<?php print $SOLDE_INIT;?>">

<BR>
  <button type="submit" class="btn btn-success btn-block addsubmit">Modifier</button>
</form>


</div>
</div>

<div class="x"></div>

</body>
    <script src="../assets/js/jquery.js"></script>
 <script src="../assets/js/jquery.number.min.js"></script>
 <script type="text/javascript">
$(function(){
	$('.number').number( true, 2,'.',' ' );
$('form#addform').on('submit',function(){
  save();return false;
});
});

      added = false;


function save() {
    $('.save').toggleClass('btn-primary btn-default').attr('disabled','disabled');
$.ajax({
      type: 'POST',
      url: 'c_edit_submit.php',
      data: $('#addform').serialize(),
      success: function(data) {
  $('.save').toggleClass('btn-primary btn-default').removeAttr('disabled');
added = true;
window.close();

      }
    });
    return false;
}


 </script>

      <script>
    window.onunload = refreshParent;
    function refreshParent() {
        window.opener.getlist('');
        window.opener.loadinfos('<?php print $fid;?>');
       if (added) window.opener.alert('Informations a été modifé');

    }
</script>
</html>
