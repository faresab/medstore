<?php
error_reporting(0);
include 'user.php';
include 'ajax/safe.php';
include $db;
$famselect = null;
$result_one = $file_db->query("SELECT FAM FROM stock WHERE FAM NOT LIKE '' GROUP BY LOWER(TRIM(FAM))");
foreach($result_one as $row) {
$famselect.= "<option value='".$row['FAM']."'>".$row['FAM']."</option>";
}

$locselect = null;
$result_one = $file_db->query("SELECT LOC FROM stock WHERE LOC NOT LIKE '' GROUP BY LOWER(TRIM(LOC)) ");
foreach($result_one as $row) {
$locselect.= "<option value='".$row['LOC']."'>".$row['LOC']."</option>";
}


$showOnly = (isset($_GET['p'])?'p':'');
$showOnly = (isset($_GET['w'])?'w':$showOnly);
?>





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EM 2015</title>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
    <!-- <link href="assets/css/animate.min.css" rel="stylesheet"> -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">

        <style media="screen">
html,body {
	height:100%;
  overflow: hidden;
}


#res {overflow-y: scroll;overflow-x: hidden;width:100%;min-height: 500px;height: 500px;
background-color: #fff;
  box-shadow: 0 4px 2px -2px rgba(0,0,0,0.4);}

#res::-webkit-scrollbar-track {border-radius: 0px; background-color: #FFF;}
#res::-webkit-scrollbar{height: 10px;width: 15px;background-color: #FFF; }
#res::-webkit-scrollbar-thumb{border-radius: 0px;background-color: #292C2F;}
table.rel{-webkit-user-select: none;width:100%;background-color: #fff; width:100%;cursor:pointer;font-family: 'Segoe UI', Tahoma, sans-serif;font-size: 14px;}
table.rel tr{margin:0;padding: 0;height:28px;}
table.rel tr td,table.rel tr th{border:1px solid #CFD8DC;padding:1px;}
table.rel tr th{background-color:#CFD8DC;text-align: center;padding:5px; }
table.rel tr th {border-top: none}
table.rel tr th.order {background-color: #ECEFF1}
table.rel tr td:first-child,table.rel tr th:first-child {border-left: none}
table.rel tr[data-pid]:hover {background-color: #ECEFF1 }

td[right] {text-align:right}
td[center] {text-align:center}
/*td[bold] {font-weight: bold}*/
table.rel tr td.warning{background-color:#FBD560;}
table.rel tr td.danger{background-color:#FF5B5B; }
tr.curr {  background-color: #DAE8F3;}
tr.curr td{ font-weight: bold;}
td[min] {background-color:#FEDFDA;-webkit-animation: clign 1s ease infinite; }
td[priceError] {color:#fff;background-color:#FEDFDA;-webkit-animation: clign2 1s ease infinite; }



.upper {text-transform: uppercase;}
*{border-radius: 0}


@-webkit-keyframes clign {
    0% {background-color: rgba(255,248,225,1);}
 100% {background-color: rgba(255,224,130,1);}
}



i.fine {color:green}
i.warning {color:orange}
i.danger {color:red}






h0 {cursor: pointer;}



.rightP {display:block;padding-left:0;transition:all 0.3s  cubic-bezier(0.445, 0, 0.635, 1);}
.rightP.togg {padding-left:240px;transition:all 0.3s  cubic-bezier(0.445, 0, 0.635, 1);}
.rightP h0 {background: #FE7418;}
.rightP.togg h0 {background: #FF1744;margin-left: -240px;padding-left: 250px;}



.leftP {position:fixed;top:25px;padding:0;left:0;bottom:0;width:0;bottom:0;background:#37474F;color:#fff;border-radius:20px;pointer-events: none;transition:all 0.5s  cubic-bezier(0.445, 0, 0.635, 1);}
/*.leftP {position:fixed;top:98vh;padding:0;left:5vw;bottom:2vw;width:0;bottom:0;background:#37474F;color:#fff;border-radius:20px;pointer-events: none;transition:all 0.5s  cubic-bezier(0.445, 0, 0.635, 1);}*/
.rightP.togg + .leftP {position:fixed;top:25px;padding:15px;left:0;bottom:0;width:240px;background:#37474F;color:#fff;border-radius:0;pointer-events: auto;transition:all 0.5s  cubic-bezier(0.445, 0, 0.635, 1);}

.rightP + .leftP .cont {display: none;opacity: 0;width:0;}
.rightP.togg + .leftP .cont {display: block;opacity: 1;transition:opacity 10s ease;width:100%;}
.leftP hr {border-top-color:rgba(255,255,255,0.1)}



        </style>
  </head>

  <body>
	      <div class="container-fluid">
<?php include 'menu-ui.php';?>

    </div>









<div class="rightP togg">
  <h0  class="redb sb-toggle-left" onclick="toggleSide()">  <i class="fa fa-bars"></i>  PRODUITS /STOCK </h0>

<div id="res">
<table class="rel">
<tr><th  data-order='QT-QTMIN' width="40px"></th><th width="100px"  data-order='REF'>REF</th><th data-order='PRODUCT'>PRODUIT</th><th  width="150px" data-order='FAM'>FAMILE</th> <th width="50px" data-order='QT'>QT</th><th width="110px" data-order='PRICE_A'>PRIX D'ACHAT</th><th width="120px" data-order='PRICE_V'>PRIX DE VENTE</th><th data-order='PRICE_A*QT' width="130px">MONTANT</th></tr>
<tbody id="stocklist">
</tbody>
</table>
</div>
</div>







<div class="leftP">
<div class="cont">
  <h4>RECHERCHE</h4>
  <input type="text" class="form-control search" placeholder="recherche">

  <hr>
  <h4>FILTRE / FAMILE</h4>

  <select class="form-control fam">
  	<option value="0">TOUS</option>
  <?php print $famselect;?>
  </select>

  <hr>
  <h4>FILTRE / LOCATION</h4>

  <select class="form-control loc">
    <option value="0">TOUS</option>
  <?php print $locselect;?>
  </select>
  <hr>
  <a class="btn btn-default btn-block" href="javascript:window.open('pstock_add.php?pid=0', '', 'width=800px,height=800')">AJOUTER UN PRODUIT</a><br>
  <hr>

  <table width="100%">
    <tr><td>VALEUR TOTAL</td><td class="text-right"><span id="allpa"></span></td></tr>
    <tr><td>VALEUR (EN PV)</td><td class="text-right"><span id="allpv"></span></td></tr>
    <tr><td>NOMBRE </td><td class="text-right"><span id="allqt"></span></td></tr>
    <tr><td>ZAKAT</td><td class="text-right"><span id="zakat"></span></td></tr>

  </table>

		</div>
		</div>





    <script src="assets/js/main-functions.js"></script>

    <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery-barcode.min.js"></script>
<script  src="assets/js/mousetrap.js" type="text/javascript" ></script>
<script  src="assets/js/moment-with-locales.js" type="text/javascript" ></script>

  <script src="assets/js/newstock.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/slidebars.js"></script>
      <script src="assets/js/ALL.js"></script>

<script>
var showOnly = '<?php print $showOnly; ?>';

function toggleSide(){
document.querySelector('.rightP').classList.toggle('togg');
}





</script>
  <?php include "plug.php";?>
</body>
</html>
