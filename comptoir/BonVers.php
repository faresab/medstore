<?php
include '../ajax/safe.php';
include '../conf/inc.php';

include $db;


$ini_printer = $ini['PRINTER'];
$ini_printername= $ini['PRINTERNAME'] ;
error_reporting(0);

$INFOS = array();
$result_one = $file_db->query("SELECT * FROM settings ");
foreach($result_one as $row) {
$INFOS[$row['id']] = $row['value'];
}

$subfile = fopen('sub.txt', 'r');
$subText = fread($subfile, filesize('sub.txt'));


$PRINTARRAY = array();
$HID = $_GET['HID'];
$TABLE = '<table width="100%"><tr><th>PROD</th><th>QT</th><th>PRIX</th></tr>';
$result_one = $file_db->query("SELECT * FROM vers WHERE HID = '$HID' ");
foreach($result_one as $row) {
$TABLE.='<tr><td>'.$row['PRODUCT'].'</td><td>'.$row['QT'].'</td><td align="right">'.fm($row['PRICE_V']).'</td></tr>';
$HID = $row['HID'];
$VERS = $row['VERS'];
$REST = $row['REST'];
$TOTAL = $REST + $VERS;
$NAME = $row['NAME'];
$DATE = $row['DATE'];
$BARCODE = $row['BARCODE'];
$PRINTARRAY[] = array($row['QT'],$row['PRODUCT'],$row['PRICE_V']);
}


$TABLE.= "<tr><td colspan='3'></tr>";
$TABLE.= "<tr><td colspan='2'><b>TOTAL</b> </td><td align='right'><b>".fm($TOTAL) ."</b> </td></tr>";
$TABLE.= "<tr><td colspan='2'><b>VERSEMENT</b></td><td align='right'><b>".fm($VERS)." </b></td></tr>";
$TABLE.= "<tr><td colspan='2'><b>REST</b></td><td align='right'><b>".fm($REST)." </b></td><td>";
$TABLE.= "</table>";


function fm($number) {
	return number_format($number, 2, ',', ' ');
}

function replaceDate($date){
	$dt = explode(' ', $date);
	$expdt = explode('-', $dt[0]);
	$newdt = $expdt[2].'/'.$expdt[1].'/'.$expdt[0];
	return str_replace($dt[0], $newdt, $date);
}

include 'escpos/versreceipt.php';
die();

?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
<style type="text/css" media="all">
	html,body{padding:0;margin:0;font-family: 'Helvetica';font-size: 12px;padding:2mm;}
@page{padding: 0;margin:0;width:100%;}

th {text-align: left;}
td[colspan='3'] {border-top: 1px solid black;height:10px;}
h6 {font-size:12px;}
</style>
</head>

<body>
<p align="center">   </p>
<h2 align="center">VERSEMENT N° <?php print $HID;?></h2>
<p>NOM : <?php print $NAME;?><br>
DATE : <?php print replaceDate($DATE);?></p>

<?php print $TABLE;?>
<br>
<div align="center">
<div class="barcode"></div>

</div>
</body>

    <script src="../assets/js/jquery.js"></script>
	<script src="../assets/js/jquery-barcode.min.js"></script>
<script type="text/javascript">
	$(function(){
$(".barcode").barcode("<?php print $BARCODE;?>", "code128",{ showHRI:false , barHeight:60});
//	window.print();
	//window.close();
	});

</script>
</html>
