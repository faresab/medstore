<?php
require_once (dirname ( __FILE__ ) . "/Escpos.php");
$connector = new WindowsPrintConnector("XP");
$printer = new Escpos($connector);


$printer->setBarcodeHeight ( 40 );

/* Text position */
$printer->selectPrintMode ( Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH );
$printer->text ( "Text position\n" );
$printer->selectPrintMode ();
$hri = array (
		Escpos::BARCODE_TEXT_NONE => "No text",
		Escpos::BARCODE_TEXT_ABOVE => "Above",
		Escpos::BARCODE_TEXT_BELOW => "Below",
		Escpos::BARCODE_TEXT_ABOVE | Escpos::BARCODE_TEXT_BELOW => "Both"
);
foreach ( $hri as $position => $caption ) {
	$printer->text ( $caption . "\n" );
	$printer->setBarcodeTextPosition ( $position );
	$printer->barcode ( "012345678901", Escpos::BARCODE_JAN13 );
	$printer->feed ();
}





$printer->close ();
