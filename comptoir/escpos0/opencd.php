<?php
require_once(dirname(__FILE__) . "/Escpos.php");




try {
$connector = new WindowsPrintConnector("$ini_printername");
$printer = new Escpos($connector);
$printer->selectPrintMode ( Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH );

	$printer -> pulse(0, 10, 10);
	$printer -> close();
} catch(Exception $e) {
  //  echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}



























class item {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 38;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
