<?php
require_once(dirname(__FILE__) . "/Escpos.php");
require_once(dirname(__FILE__) . "/cel.php");
$now = date('d/m/Y H:i:s');




try {
$connector = new WindowsPrintConnector("$ini_printername");
$printer = new Escpos($connector);
if ($logo !== '' && $logo_recu == '1') {


$tux = new EscposImage("../ajax/up/pmh.png");
$printer -> graphics($tux);
} else {
  $printer -> setJustification(Escpos::JUSTIFY_CENTER);
  $printer->selectPrintMode ( Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH );
  $printer -> text($INFOS['4']." \n");

}
$printer -> feed();
$printer->selectPrintMode ();
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
    $printer -> text($INFOS['5']." \n _______________________________________________\n");
    $printer -> text(new item( date('d/m/Y') , date('H:i:s') ));
    // $printer -> text($now." \n");
$printer -> setJustification(Escpos::JUSTIFY_LEFT);


$TOTALVAL = 0;
$printer -> feed(3);
for ($i = 0; $i < count($PRINTARRAY); $i++) {
$TOTALVAL+= $PRINTARRAY[$i][2];
  $printer -> text(new item($PRINTARRAY[$i][0].'  '.strtoupper($PRINTARRAY[$i][1]), number_format($PRINTARRAY[$i][2] , 2, ',', ' ')));

}


$printer -> feed();
	$printer -> setEmphasis(false);
  $printer -> text(new item('TOTAL', number_format($TOTALVAL , 2, ',', ' ')));


  if ($Discount !== '0') {
  $printer -> text(new item('REMISE', number_format($Discount , 2, ',', ' ')));
  $printer -> feed();
$printer -> text(new item('MONTANT A PAYER', number_format($TOTALVAL - $Discount , 2, ',', ' ')));
}


  $printer -> feed();




$a = new chiffreEnLettre();
 $totallettre = $a->ConvNumberLetter($TOTALVAL,1,0);

  $printer -> text(strtoupper($totallettre) . "\n");



$printer->selectPrintMode ( Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH );
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer->setBarcodeHeight ( 80 );
$printer->selectPrintMode ();
$printer->setBarcodeTextPosition ( Escpos::BARCODE_TEXT_NONE );
$barcodeHid = str_pad($HID, 12, "0", STR_PAD_LEFT);
$printer->barcode ( "P".$barcodeHid , Escpos::BARCODE_CODE39 );


	$printer -> feed();
  $printer -> setJustification(Escpos::JUSTIFY_CENTER);


	$printer -> text("--------------------- \n ".strtoupper($subText)." \n");
	$printer -> feed(4);
	$printer -> cut();
	$printer -> pulse(0, 10, 10);
    $printer -> close();
} catch(Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}



























class item {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 38;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
