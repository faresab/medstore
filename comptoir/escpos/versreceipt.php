<?php

require_once(dirname(__FILE__) . "/cel.php");
$now = date('d/m/Y H:i:s');

require __DIR__ . '/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;



$connector = new WindowsPrintConnector("$ini_printername");
$printer = new Printer($connector);
$printer->selectPrintMode ( Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH );
$printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text($INFOS['4']." \n");
$printer -> feed();
$printer->selectPrintMode ();
    $printer -> text($INFOS['5']." \n ___________________ \n");

    $printer->selectPrintMode ( Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH );
$printer -> text("VERSEMENT N# ".$HID ."\n");
$printer->selectPrintMode ();
    $printer -> text($now." \n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);

$printer -> text(new item("NOM", $NAME));
$printer -> feed();

/*
$printer -> text(new item("NOM", "fares");


$printer -> text(" ___________________ \n");
*/

for ($i = 0; $i < count($PRINTARRAY); $i++) {
  $printer -> text(new item($PRINTARRAY[$i][0].'  '.$PRINTARRAY[$i][1], fm($PRINTARRAY[$i][2])));
}
$printer -> setJustification(Printer::JUSTIFY_CENTER);
  $printer -> text(" ___________________ \n");
$printer -> setJustification(Printer::JUSTIFY_LEFT);

$printer -> feed();
$printer -> setEmphasis(false);
$printer -> text(new item('TOTAL', number_format($TOTAL , 2, ',', ' ')));
$printer -> text(new item('VERSEMENT', fm($VERS)));
$printer -> text(new item('RESTE', fm($REST)));






  $printer -> feed();




$a = new chiffreEnLettre();
 $totallettre = $a->ConvNumberLetter($REST,1,0);

  $printer -> text('RESTE:' .strtoupper($totallettre) . "\n");



$printer->selectPrintMode ( Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH );
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer->setBarcodeHeight ( 80 );
$printer->selectPrintMode ();
$printer->setBarcodeTextPosition ( Printer::BARCODE_TEXT_NONE );
$printer->barcode ( $BARCODE , Printer::BARCODE_CODE39 );


	$printer -> feed();
  $printer -> setJustification(Printer::JUSTIFY_CENTER);


	$printer -> text("--------------------- \n ".strtoupper($subText)." \n");
	$printer -> feed(4);
	$printer -> cut();
	$printer -> pulse(0, 10, 10);
    $printer -> close();




























class item {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 38;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
