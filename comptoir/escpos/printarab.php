<?php
// error_reporting(0);
require __DIR__ . '/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\EscposImage;
require_once(dirname(__FILE__) . "/cel.php");
$now = date('d/m/Y H:i:s');



echo mb_internal_encoding() . "\n\n";
$connector = new WindowsPrintConnector("POS");
$printer = new Printer($connector);
// $printer -> initialize();

$str = "��������������";


echo mb_strlen($str) . "\n";
echo mb_strlen($str, 'UTF-8') . "\n";
// Epson character tables
//$try = array(
//  37 => 'CP864',
//  50 => 'CP1256');

// E-pos TEP 200M character tables
$try = array(
    22 => 'PC864',
    28 => 'CP864',
    29 => 'CPW1001',
    33 => 'CP720',
    34 => 'CP1256' ,
    63 => 'CP864',
    34 => 'CP1256',
    82 => 'CP1001'
    );


    $map = makeMap('CP1001');
    $outp = convert($str, $map);
    $printer -> selectCharacterTable(82);
    $printer -> text("22  PC864 \n");
    $printer -> textRaw(strrev($outp) . "\n");


$printer -> feed(2);
$printer -> cut();
$printer -> CLOSE();

function convert($str, $map) {
    // Convert UTF8 to the target encoding
    $len = mb_strlen($str);
    $outp = str_repeat("?", $len);
    for($i = 0; $i < $len; $i++) {
        $utf8 = mb_substr($str, $i, 1);
        if(isset($map[$utf8])) {
            $outp[$i] = $map[$utf8];
        }
    }
    return $outp;
}

function makeMap($targetEncoding) {
    // Make map of target encoding v UTF-8
    $map = array();
    for($i = 0; $i < 255; $i++) {
        $native = chr($i);
        $utf8 = @iconv($targetEncoding, 'UTF-8', $native);
        $utf8 = @iconv($targetEncoding, 'UTF-8', $native);
        if($utf8 == '') {
            continue;
        }
        $map[$utf8] = $native;
    }
    return $map;
}
