<?php
error_reporting(E_ALL);
require __DIR__ . '/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
 use Mike42\Escpos\EscposImage;
require_once(dirname(__FILE__) . "/cel.php");
$now = date('d/m/Y H:i:s');

$barcodeHid = str_pad($HID, 12, "0", STR_PAD_LEFT);


$connector = new WindowsPrintConnector("$ini_printername");
 $printer = new Printer($connector);
$printer -> initialize();

 



if ($logo !== '' && $logo_recu == '1') {
$tux = EscposImage::load("../ajax/up/$logo", false);
$printer -> bitImage($tux);
// $printer -> graphics($tux);
} else {
  $printer -> setJustification(Printer::JUSTIFY_CENTER);
  $printer->selectPrintMode ( Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH );
  $printer -> text($INFOS['4']."   \n");

}
$printer -> feed();
$printer->selectPrintMode ();
$printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> text($INFOS['5']." \n");
if ($INFOS['6'] !== '')    $printer -> text("TEL:".$INFOS['6']." \n");
    $printer -> text(" _______________________________________________\n");
    $printer -> text(new item( date('d/m/Y') , date('H:i:s') ));

$printer -> setJustification(Printer::JUSTIFY_LEFT);


$TOTALVAL = 0;
$printer -> feed(3);


for ($i = 0; $i < count($PRINTARRAY); $i++) {
$TOTALVAL+= $PRINTARRAY[$i][2];
  $printer -> text(new item($PRINTARRAY[$i][0].'  '.strtoupper($PRINTARRAY[$i][1]), number_format($PRINTARRAY[$i][2] , 2, ',', ' ')));
}


$printer -> feed();
	$printer -> setEmphasis(false);
$printer -> text(" \n _______________________________________________\n");
  $printer->selectPrintMode (  Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH);
  $printer -> text(new itemBig('TOTAL', number_format($TOTALVAL , 2, ',', ' ')));
  $printer->selectPrintMode ();
  $printer -> feed();

  if ($Discount !== '0') {
  $printer -> text(new item('REMISE', number_format($Discount , 2, ',', ' ')));
  $printer -> feed();
$printer -> text(new item('MONTANT A PAYER', number_format($TOTALVAL - $Discount , 2, ',', ' ')));
}


  $printer -> feed();






  $printer -> setJustification(Printer::JUSTIFY_CENTER);
  $a = new chiffreEnLettre();
   $totallettre = $a->ConvNumberLetter($TOTALVAL,1,0);
  if ($TOTALVAL != '0')  $printer -> text(strtoupper($totallettre) . "\n");


$printer->selectPrintMode ( Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH );

$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer->setBarcodeHeight ( 64 );
$printer->setBarcodeWidth(8);

 $printer->setBarcodeTextPosition ( Printer::BARCODE_TEXT_NONE );

$printer->barcode($barcodeHid , Printer::BARCODE_CODE39 );
//$printer->barcode($barcodeHid , Printer::BARCODE_ITF );

//$printer->barcode($barcodeHid , Printer::BARCODE_CODE93 );



  $printer->selectPrintMode ();

// $printer -> setJustification();

	$printer -> feed();



	$printer -> text("--------------------- \n ".strtoupper($subText)." \n");
	$printer -> feed(2);
	$printer -> cut();
	$printer -> pulse(0, 10, 10);
    $printer -> close();




























class item {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 38;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}


class itemBig {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 14;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
