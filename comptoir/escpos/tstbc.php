<?php
require __DIR__ . '/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

$connector = new WindowsPrintConnector("POS");
$printer = new Printer($connector);

/* Height and width */
 $printer->selectPrintMode();
      $printer->setBarcodeWidth(8);
    $printer->setBarcodeHeight(60);
    $printer->barcode("12315646874867", Printer::BARCODE_CODE39);
$printer->feed();


$printer->cut();
$printer->close();
