<?php
include 'user.php';

$page = 'REL';

  $fid = "<script>var sel = '-'</script>";
if(isset($_GET['fid'])) {
  $fid = "<script>var sel = '".$_GET['fid']."'</script>";
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CREDOC</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">

    <link href="assets/css/relprint.css" rel="stylesheet" media="print">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">

  </head>

  <body>
  <div class="container-fluid ">
<?php include 'menu-ui.php';?>
</div>

    <div class="container-fluid ">
<div class="row">
<div class="col-md-12" style="min-height:500px;">
<br>
<div class="well informations notscreen">
</div>
<div class="well well-sm"  style="min-height:80px;display:none;">
<div class="row">
<div class="col-md-4"><div class="topinfos upper"></div></div>
<div class="col-md-4"><div class="topinfos2 upper"></div></div>
<div class="col-md-4 btns">
  <div class="pull-right buttons" style="display:none;">
<a type="text" class="btn btn-block btn-default upper filter">filtrer</a>

  <div class="btn-group">
  <button type="button" class="btn btn-default  dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#" id="vlink">Ajouter un versement</a></li>
    <li><a href="#" id="crlink">Ajouter un credit</a></li>
    <li><a href="#" class="editbtn">Modifier</a></li>
    <li class="divider"></li>
    <li><a href="#">Suprimmer</a></li>
  </ul>
</div>

  <a class="btn  btn-default adjust"><i class="fa fa-adjust"></i></a>
  <a class="btn btn-default" onClick="window.print()"><i class="fa fa-print"></i></a>
  <a class="btn btn-default editbtn"><i class="fa fa-edit"></i></a>

</div>

</div>

</div>






</div>



<div id="field">

</div>




</div>
</div>

<br><br><br>
</div>


<div class="modal fade" id="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title upper"><i class="fa fa-warning"></i> DATE </h4>
      </div>
      <div class="modal-body del upper">
<form id="datefilter" >
<input type="date" name="dt1" class="form-control dt1" required/><br>
<input type="date" name="dt2" class="form-control dt2" required/><br>
<input type="hidden" name="fid" value=""/>
<button type="submit " class="btn-block btn btn-primary">SUBMIT</button><br>

</form>
      </div>

    </div>
  </div>
</div>







    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
  <script src="dist/js/bootstrap.min.js"></script>

    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/script_money.js"></script>


    <?php print $fid;?>
    <script src="js/rel-app.js"></script>
  </body>
</html>
