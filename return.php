<?php
error_reporting(0);
include 'user.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>EM14</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">

<style type="text/css">
body {background-color: #F8F8F8}
td .btn {margin:-5px;}
span.grey {margin-left:10px;color:#888;}
</style>


  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container-fluid">

<?php include 'menu-ui.php';?>

    </div>

<h0 class="ret">GESTION DES RETOURS</h0>
    <div class="container-fluid theme-showcase">





	        <div style="height:70px;padding:20px;background-color:#EEB0AE;color:#fff;box-shadow: 0px 2px 8px rgba(0,0,0,0.4)">

			<div class='row'>

					<div class="col-md-6 col-lg-6 " style='margin:0;'>

			</div>

			<div class="col-md-6 col-lg-6 ">
<form id="form">
<div class="input-group">
      <input type="text" class="form-control searchiput" placeholder="Recherche..">
      <span class="input-group-btn">
        <button class="btn btn-danger searchctrl" type="button"><i class="fa fa-search"></i> Recherche</button>
      </span>
    </div></form>
			</div>




			</div>
			</div>




</div>



<br><br>
<div class="container">
	  <div class="searchresults"></div>
</div>






    <script src="assets/js/jquery.js"></script>
  <script src="assets/js/moment.int.js"></script>

	<script>

moment.lang('fr');

function showDates(){
$('span.time').each(function(){
var time = $(this).text();
var showFromnow = moment(time , "YYYY-MM-DD HH:mm").fromNow();
$(this).append('<span class="grey">'+showFromnow+'</span>');
});
}


var findbc = function(){
var searchkey = $('.searchiput').val();
$('.searchctrl').css('opacity','0.5');
$('.searchresults').html('<span style="position:relative;margin-left:45%;top:100px;"><i class=" fa fa-spin fa-spinner"></i> recherche en cours..</span>').load('ajax/search_return.php?key='+searchkey , function(){
$('.searchctrl').css('opacity','1');
showDates();


});
return false;
}




function removethis(thisrel) {
var rel = thisrel;
var tr = $('tr[data-rel="'+rel+'"]');
var btn = $('button[data-rel="'+rel+'"]');
tr.addClass('danger');
btn.addClass('btn-danger').html('<i class="fa fa-spin fa-spinner"></i> suppression..').load('ajax/ret_del.php?id='+rel , function(x) {
  console.log(x);
tr.addClass('animated bounceOutRight ').slideUp('slow');
});

return false;
}


$(function() {
$('.searchiput').focus();
$('.searchctrl').on('click',findbc);
$('#form').on('submit',findbc);
});
	</script>



    <script src="dist/js/bootstrap.min.js"></script>
  <script src="assets/js/ALL.js"></script>
    <?php include "plug.php";?>


  </body>
</html>
