<?php
error_reporting(0);
include 'user.php';

include 'ajax/safe.php';
$today = date('Y-m-d');


include $db;
 $result = $file_db->query("SELECT COUNT(ID) AS COUNTID ,SUM(PRICE_A * QT) AS MONTANT_A,SUM(PRICE_V * QT) AS MONTANT_V FROM stock");
foreach($result as $row) {
$SUMA = number_format($row['MONTANT_A'], 2, ',', ' ');
$SUMV = number_format($row['MONTANT_V'], 2, ',', ' ');
$COUNTID = $row['COUNTID'];
}
$RECETTE = 0;
 $result = $file_db->query("SELECT SUM(MONT) AS RECETTE  FROM vents WHERE DATETIME LIKE '$today%'");
foreach($result as $row) {
$RECETTE+= $row['RECETTE'];
}

$RECETTE = number_format($RECETTE, 2, ',', ' ');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title></title>
	    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
</head>
<style>
table.big {width:100%;}
table.big tr td {font-size: 18px;font-weight: bold;paddin:3px;border-bottom: 1px solid rgba(0,0,0,0.1)}
.well h0 {margin: -10px -10px 0 -10px;padding:10px;}
.fullw {width:100%;}
.btn-group.btn-block {
    display: table;
}
.btn-group.btn-block > .btn {
    display: table-cell;
}


</style>
<body>
<div class="container-fluid">
<?php include "menu-ui.php";?>
</div>

<h0>LISTE DES FOURNISSEURS</h0>

<div class="container-fluid" style="padding:15px">

	<div class="row">
<div class="col-md-8">
<div class="well">

<div class="input-group">
              <input type="text" class="form-control search" placeholder="Recherche">
              <div class="input-group-addon ftot">1</div>
          </div>

<div class="lft">
<table class="clist upper"><tbody><tr onclick="loadinfos(1);"><td><a href="#" data-id="1" class="link">FARES0</a></td><td>setif</td><td><span class="pull-right">0,00</span></td><td><b class="pull-right">3</b></td><td><span class="text-danger pull-right">-400 000,00 <span class="glyphicon glyphicon-arrow-down"> </span></span></td>
</tr></tbody></table>
</div>
</div>

</div>

<div  class="col-md-4">
<div class="well well-sm">
  <div id="finfos" class="magictime perspectiveRight"></div>
</div>


<div class="well well-sm">
  <h0>Nouveau fournisseur</h0><br>
<form role="form" id="addform" method="POST" action="ajax/add_fourn.php">
    
    <input type="text" name="name" class="form-control" placeholder="Nom" required="">
<br>  <input type="text" name="adress" class="form-control" placeholder="Adresse">
<br> 

<div class="row">
<div class="col-md-6">
<input type="text" name="wilaya" class="form-control" placeholder="WILAYA">
</div>
<div class="col-md-6">
<input type="text" name="fax" class="form-control" placeholder="FAX">
</div>
</div>
<br>



<div class="row">
<div class="col-md-6">
<input type="text" name="tel1" class="form-control" placeholder="TEL 1">
</div>
<div class="col-md-6">
<input type="text" name="tel2" class="form-control" placeholder="TEL 2">
</div>
</div>
<br>








<input type="number" name="solde_init" class="form-control" placeholder="Solde initial">


<br>
  <button type="submit" class="btn btn-primary btn-block addsubmit">AJOUTER</button>
</form>

</div>
</div>



	</div>
</div>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
  <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/ALL.js"></script>

<script>

function getlist(key) {
$('.lft').html('<span  style="position:absolute;left:45%;top:40%;"><i class="fa fa-spin fa-circle-o-notch"></i> Chargement..</span>');
$('.lft').load('ajax/fournlist.php?key='+key ,function(){
  
});
}









$(function(){
      setTimeout(function(){$('input[name="name"]').focus();},500);
    getlist('');
$('.search').on('input',function(){
getlist($(this).val());
});
var form = $('#addform');
form.on('submit',function(){
    var btnhtml = $('.addsubmit').html();
$('.addsubmit').html('<i class=" fa fa-spin fa-circle-o-notch"></i>');
$.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize(),
      success: function(data) {
        alert('Fournisseur a été ajouté');
        getlist('');
        $('.addsubmit').html(btnhtml);
      }
    });
    return false;
  });
      });

function empty(){
$('#addform input').each(function(){
  $(this).val("");
});
$('.search').val("");
$('.hass').removeClass('has-error');
getlist('');
}


function exist(){
$('.hass').addClass('has-error').children('input').focus();
}

function loadinfos(fid){
$('#finfos').show().animate({
    height: "320px"
  }, 500, function() {
$('#finfos').html('<span  style="position:absolute;left:35%;top:60px;"><i class="fa fa-spin fa-circle-o-notch"></i> Chargement..</span>').load('ajax/finfos.php?id='+fid ,function(){
    $('.closespan').on('click',function(){
$('#finfos').animate({height: "0px"},500,function(){$(this).hide()});
    setTimeout(function(){$('input[name="name"]').focus();},200);
  });
});
 });
}






</script>

</body>
</html>