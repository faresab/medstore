<?php
error_reporting(0);
include 'user.php';


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title>LISTE DES CLIENTS</title>
	    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
</head>
<style>
table.big {width:100%;}
table.big tr td {font-size: 18px;font-weight: bold;paddin:3px;border-bottom: 1px solid rgba(0,0,0,0.1)}
.well h0 {margin: -10px -10px 0 -10px;padding:10px;}

.body-infos {display:none}
.body-infos.act {display:block}



</style>
<body>
<div class="container-fluid">
<?php include "menu-ui.php";?>
</div>

<h0 class="green " >LISTE DES CLIENTS</h0>

<div class="container-fluid" style="padding:15px">

	<div class="row">
<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Liste des clients</h3>
    </div>
    <div class="panel-body" style="min-height:70vh;">

<div class="input-group">
              <input type="text" class="form-control search" oninput="filter()" placeholder="Recherche">
              <div class="input-group-addon ftot">1</div>
          </div>

<div class="lft">
<table class="clist upper"><tr><td>NOM</td><td>TYPE</td><td>ADRESSE</td><td>TEL</td><td class="text-right">CREDIT INIT</td><td class="text-right">CREDIT</td></tr>
<tbody id="rs">

</tbody>
</table>
</div>
</div>
</div>

</div>

<div  class="col-md-4">

  <div class="panel panel-success">
    <div class="panel-heading" onclick="togglebd">
      <h3 class="panel-title">Infos</h3>
      </div>
<div class="panel-body body-infos" style="padding:15px 5px;">

<table class="table table-bordered infos">
<tr><td>Nom</td><td></td></tr>
<tr><td>Type</td><td></td></tr>
<tr><td>Adresse</td><td></td></tr>
<tr><td>Tel</td><td></td></tr>
<tr><td>Solde</td><td class="text-right"></td></tr>
<tr><td>Crédit totalL</td><td class="text-right"></td></tr>
<tr><td>Versement total</td><td class="text-right"></td></tr>
</table>


</div>
</div>




  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Ajouter un client</h3>
    </div>
    <div class="panel-body" style="padding:15px 5px;">
<form class="addform" action="index.html" method="post">

<table style="width:90%;margin:0 auto;" onsubmit="return false">
<tr>
<td>Nom</td><td><input  required type="text" name="name" value="" autofocus class="form-control"></td>
</tr><tr>
<td>Type</td><td>
<select class="form-control" name="type">
<option value="1">Détaillant</option>
<option value="2">Revendeur</option>
<option value="3">Grossiste</option>
</select>
</td>
</tr>
<tr><td>Adresse</td><td><input type="text" name="adress" value="" autofocus class="form-control"></td></tr>
<tr><td>Wilaya</td><td><input type="text" name="wilaya" value="" autofocus class="form-control"></td></tr>
<tr><td>Fax</td><td><input type="text"  name="fax" value="" autofocus class="form-control"></td></tr>
<tr><td>Tel</td><td><input type="text"  name="tel1" value="" autofocus class="form-control"></td></tr>
<tr><td>Tel 2</td><td><input type="text"  name="tel2" value="" autofocus class="form-control"></td></tr>
<tr><td>Solde init</td><td><input type="text" name="solde_init" value="0" autofocus class="form-control"></td></tr>
<tr><td></td><td>
<button type="submit" name="button" class="btn btn-block btn-success">Ajouter</button></td></tr>


</table>
</form>

    </div>
  </div>










  <!--
<div class="well well-sm" style="padding:0;min-height:0;border:none;margin-bottom:0">
  <div id="cinfos" class="magictime perspectiveRight"></div>
</div> -->


<!--<div class="well well-sm">
  <h0 class="green afs">Nouveau client</h0><br>
<form role="form" id="addform" method="POST" action="ajax/add_client.php">

    <input type="text" name="name" autofocus class="form-control" placeholder="Nom" required="">
<br>  <input type="text" name="adress" class="form-control" placeholder="Adresse">
<br>

<div class="row">
<div class="col-md-6">
<input type="text" name="wilaya" class="form-control" placeholder="WILAYA">
</div>
<div class="col-md-6">
<input type="text" name="fax" class="form-control" placeholder="FAX">
</div>
</div>
<br>



<div class="row">
<div class="col-md-6">
<input type="text" name="tel1" class="form-control" placeholder="TEL 1">
</div>
<div class="col-md-6">
<input type="text" name="tel2" class="form-control" placeholder="TEL 2">
</div>
</div>
<br>








<input type="number" name="solde_init" class="form-control" placeholder="Credit initial">


<br>
  <button type="submit" class="btn btn-success btn-block addsubmit">AJOUTER</button>
</form>

</div>-->
</div>



	</div>
</div>
<script src="assets/js/main-functions.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
  <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/ALL.js"></script>

<script>

// function getlist(key) {
// $('.lft').html('<span  style="position:absolute;left:45%;top:40%;"><i class="fa fa-spin fa-circle-o-notch"></i> Chargement..</span>');
// $('.lft').load('ajax/clientsnlist.php?key='+key ,function(){
// });
// }

filter();

function filter(){
  var search = document.querySelector('.search').value;
  var jsonLink = 'ajax/jsonclients.php?search='+encodeURIComponent(search);
  fetchJSONFile(jsonLink , function(z){
    var tbl = '';
if (z && z.length) {
  for (var i = 0; i < z.length; i++) {
    tbl += '<tr onClick="loadinfos('+z[i].ID+');"><td><a href=# data-id="'+z[i].ID+'"  class="link">'+z[i].NAME.toUpperCase()+'</a>';
    tbl += '</td><td>'+showType(z[i].TYPE)+' </td><td>'+z[i].ADRESS+' </td><td>'+z[i].FAX+' '+z[i].TEL1+' '+z[i].TEL2+'</td><td><b class="pull-right">'+fm(z[i].SOLDE_INIT)+'</b></td><td class="text-right">'+fm(z[i].SOLDE)+'</td></tr>';

  }
}
document.querySelector("#rs").innerHTML = tbl;
document.querySelector(".ftot").innerText = z.length;

  });

}

function showType(e){
  if (!e) e = 1;
  var types = ['','Détaillant','Revendeur','Grossiste'];
  return  types[parseInt(e)] ;
}

document.querySelector('.addform').onsubmit = function(){
  var dt = serialize(this);
  console.log(dt);

   var httpRequest = new XMLHttpRequest();
  httpRequest.onload  = function () {
  	if (httpRequest.readyState==4 && httpRequest.status==200){
  console.log( httpRequest.responseText);
  alert('Client a été ajouté');
  document.querySelector('.addform').reset();
  filter();
  }
  }
  httpRequest.open('POST', 'ajax/add_client.php');
  httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  httpRequest.setRequestHeader( "Pragma", "no-cache" );
  httpRequest.setRequestHeader( "Cache-Control", "no-cache" );
  httpRequest.setRequestHeader( "Expires", 0 );
  httpRequest.send(dt);



  return false;
}





function fetchJSONFile(path, callback) {
       var httpRequest = new XMLHttpRequest();
       httpRequest.onreadystatechange = function() {
           if (httpRequest.readyState === 4) {
               if (httpRequest.status === 200) {
                 spydata = JSON.stringify(httpRequest.responseText);
                   var data = JSON.parse(httpRequest.responseText);
                   if (callback) callback(data);
               }
           }
       };
       httpRequest.open('GET', path);
       httpRequest.send();
   }
   function serialize(form) {
       var field, s = [];
       if (typeof form == 'object' && form.nodeName == "FORM") {
           var len = form.elements.length;
           for (i=0; i<len; i++) {
               field = form.elements[i];
               if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                   if (field.type == 'select-multiple') {
                       for (j=form.elements[i].options.length-1; j>=0; j--) {
                           if(field.options[j].selected)
                               s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[j].value);
                       }
                   } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                       s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
                   }
               }
           }
       }
       return s.join('&').replace(/%20/g, '+');
   }
   function fm(Money) {
   	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
   }

   function mfd(sss) {
     return parseFloat(sss.replace(',','.').replace(' ',''));
   }



$(function(){
      // setTimeout(function(){$('input[name="name"]').focus();},500);
    // getlist('');



// $('.search').on('input',function(){
// getlist($(this).val());
// });
// var form = $('#addform');
// form.on('submit',function(){
//     var btnhtml = $('.addsubmit').html();
// $('.addsubmit').html('<i class=" fa fa-spin fa-circle-o-notch"></i>');
// $.ajax({
//       type: form.attr('method'),
//       url: form.attr('action'),
//       data: form.serialize(),
//       success: function(data) {
//         alert('client a été ajouté');
//         getlist('');
//         $('.addsubmit').html(btnhtml);
//       }
//     });
//     return false;
//   });
      });


function togglebd(){
  document.querySelector('.body-infos').classList.toggle('act');
}






function exist(){
$('.hass').addClass('has-error').children('input').focus();
}


// function loadinfos(fid){
// $('#cinfos').show().animate({
//     height: "320px"
//   }, 500, function() {
// $('#cinfos').html('<span  style="position:absolute;left:35%;top:60px;"><i class="fa fa-spin fa-circle-o-notch"></i> Chargement..</span>').load('ajax/cinfos.php?id='+fid ,function(){
//     $('.closespan').on('click',function(){
// $('#cinfos').animate({height: "0px"},500,function(){$(this).hide()});
//     setTimeout(function(){$('input[name="name"]').focus();},200);
//   });
// });
//  });
// }

function loadinfos(cid) {
fetchJSONFile('ajax/cinfos.php?id='+cid,function(d){
console.log(d);
 var htmlInfos = '<tr><td width="120px;">Nom</td><td>'+d.name.toUpperCase()+'</td></tr>';
 htmlInfos += '<tr><td>Type</td><td>'+ showType(d.type) +'</td></tr>';
 htmlInfos += '<tr><td>Adresse</td><td>'+d.adress.toUpperCase()+'</td></tr>';
 htmlInfos += '<tr><td>Crédit Total</td><td class="text-right">'+fm(d.credittotal)+'</td></tr>';
 htmlInfos += '<tr><td>Versement Total</td><td class="text-right">'+fm(d.verstotal)+'</td></tr>';
  htmlInfos += '<tr><td>Solde</td><td class="text-right"><b>'+fm(d.solde)+'</b></td></tr>';
  htmlInfos += '<tr><td colspan="2">  <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">   <a href="javascript:newwin(\'op/c_add_v.php?fid='+d.cid+'\',600,500)" class="btn btn-default" role="button" >Versement</a> <a href="javascript:newwin(\'op/c_edit.php?fid='+d.cid+'\',700,720)" class="btn btn-default" role="button">Modifier</a>   <a href="crel.php?fid='+d.cid+'" class="btn btn-default" role="button">RELEVE</a> </div>  </td> </tr>';

document.querySelector('table.infos').innerHTML = htmlInfos;
document.querySelector('.body-infos').classList.add('act');

})
}

function newwin (link,ww,wh) {
  window.open(link, '', 'width='+ww+',height='+wh);
}




</script>

</body>
</html>
