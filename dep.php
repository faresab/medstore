<?php
error_reporting(0);
include 'user.php';
include 'ajax/safe.php';
include $db;

$SELECT = '<option value="ALL">TOUS</option>';
 $result_one = $file_db->query("SELECT *  FROM dep GROUP BY LOWER(TRIM(DEP))");
foreach($result_one as $row) {
$SELECT.= '<option value="'.$row['DEP'].'">'.$row['DEP'].'</option>';
}



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DEPENSES</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template
    <link href="theme.css" rel="stylesheet">-->
<link href="assets/css/bootstrap-select.min.css" rel="stylesheet">
	    <link href="dist/css/bootstrap-theme.css" rel="stylesheet">
      <link href="add.css" rel="stylesheet">
<style>
.well h0 {margin: -10px -10px 0 -10px;padding:10px;}
</style>

  </head>

  <body oncontextmenu="return false">

    <!-- Fixed navbar -->
	      <div class="container-fluid">

<?php include 'menu-ui.php';?>

    </div>

    <h0 class="orange">Gestion des dépenses</h0>


    <div class="container-fluid theme-showcase">






	        <div class="jumbotrontop bg-warning">

			<div class='row'>
			<form id="addform">
			<div class="col-md-6 col-lg-6 ">
			    <input type="text" class="form-control dep" name="dep" placeholder="dépenses" required>
			</div>
			<div class="col-md-6 col-lg-3 ">
			    <input type="text" class="form-control val number" name="val" placeholder="valeur" required>
			</div>
			<div class="col-md-3 col-lg-3 ">
			<button type="submit" class="btn btn-warning btn-block save" id="addb" style="vertical-align:top;">AJOUTER (F3)</button>
			</div>
			</form>

			</div>
			</div>





 <div class="jumbotronmid">

	   <br><br>


<div class="row">
<div class="col-sm-8">

	  <table class="table table-bordered">
	   <tr><th>N°</th><th>DATE</th><th>DEPENSES</th><th>VALEUR</th></tr>
	   <tbody id="tb">
	   </tbody>
	   </table>
   </div>

<div class="col-sm-4" >
<div class="well well-sm">
<h0 class="orange">TOTAL : <span class='totval pull-right'></span></h0>
<br><br>
<table width="100%">
	<tr><td>DEPENSES</td>
	<td> 	<select class="form-control depfilter"  data-style="btn-warning" data-live-search="true">
<?php print $SELECT;?>
	   	</select></td></tr>

	<tr><td>DATE DE</td><td><input type="date" class="form-control date1" />
 </td><tr>
	<tr><td> JUSQU'AU</td><td><input type="date" class="form-control date2" />
 </td></tr>


</table>






</div>
</div>

	   </div>









</div>
</div>


    <script src="assets/js/jquery.js"></script>
        <script src="assets/js/mousetrap.js"></script>
        <script src="assets/js/bootstrap-select.min.js"></script>
      <script src="assets/js/jquery.number.min.js"></script>
	<script>

var TOTVAL;
	$(function() {
		$('.depfilter').selectpicker();
		$('input').first().focus();
		$('.number').number( true, 2,'.',' ' );
filter();


$('.depfilter,.date1,.date2').on('change',function(){
filter();
});
	});


function filter(){
	var date1 = encodeURIComponent($('.date1').val());
	var date2 = encodeURIComponent($('.date2').val());
	var depfilter = encodeURIComponent($('.depfilter').val());
 $('#tb').load('ajax/load_dep.php?d1='+date1+'&d2='+date2+'&filter='+depfilter ,function(){
$('.totval').text(fm(TOTVAL));
	});
}




function save() {
$('.save').attr('disabled','disabled');
$.ajax({
      type: 'POST',
      url: 'ajax/depense.php',
      data: $('#addform').serialize(),
      success: function(data) {
      	music("Done",100);
  $('.save').removeAttr('disabled');
filter();
$('.dep,.val').val('');
$('input').first().focus();
      }
    });
return false;
}

Mousetrap.bind('f3', function() { save(); });


$(function(){
$('#addform').on('submit',function(){
save();
return false;
});
});

function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

	</script>



    <script src="dist/js/bootstrap.min.js"></script>

  <script src="assets/js/ALL.js"></script>

 <?php include "plug.php";?>
 </body>
</html>
