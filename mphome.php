<?php
include 'user.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LISTE DES COURS</title>
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/home.css" media="screen" title="no title" charset="utf-8">

<style media="screen">

.navbar-default {
  background: rgba(0, 0, 0, 0.5);
  border:none;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;

}
.navbar-default .navbar-nav > li > a {
  /*color: #777777;*/
  color:#fff;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  background: rgba(255, 255, 255, 0.5);
}
.navbar-default .navbar-brand, .navbar-brand {
  float: left;
   font-size: 16px;
  line-height: 20px;
  font-family: 'Roboto';
  font-weight: bold;
  text-transform: uppercase;
    color:#FFEB3B;
}
.btn-group {padding:0}

</style>

  </head>
  <body>


<?php include 'menu-ui.php';?>


    <div id="left" class="split split-horizontal">
<div class="content">

<div class="search">
<input type="text" class="form-control key" value="" placeholder="recherche" autofocus>
</div>

<div id="results">
<table id="tbl">
  <tr><th width="60px">Code</th><th>Titre</th><th width="180px">Enseignant</th><th width="50px">R</th><th width="60px">P</th><th width="50px">Format</th></tr>
  <tbody id="res">
  <tr></td></td><td></td></tr>
</tbody>
</table>


</div>

</div>




    </div>
    <div id="right" class="split split-horizontal">
      <div id="stop" class="split content">
<div class="pricefield">
0.00
</div>
<table id="sales">






</table>
<div class="control">



  <div class="btn-group btn-group-justified" role="group" aria-label="...">
    <div class="btn-group" role="group">
      <a onclick="validate(true)" class="btn btn-success"><i class="fa fa-print"></i></a>
    </div>
    <div class="btn-group" role="group">
      <a onclick="validate()" class="btn btn-default"><i class="fa fa-check"></i></a>
    </div>
  </div>




</div>




</div>
           <div id="sbottom" class="split content">
<div class="printers">





           </div>

    </div>




  </body>
<script src="js/split.js" charset="utf-8"></script>
<script src="js/homebase.js?r=<?php echo time(); ?>" charset="utf-8"></script>
<script src="js/homesale.js?r=<?php echo time(); ?>" charset="utf-8"></script>
<script type="text/javascript">
Split(['#left', '#right'], {
  gutterSize: 8,
  sizes: [65, 35],
  cursor: 'col-resize'
});


Split(['#stop', '#sbottom'], {
    direction: 'vertical',
    sizes: [60, 40],
    gutterSize: 8,
    cursor: 'row-resize'
  })
</script>
<script src="assets/js/jquery.js"></script>
<script src="dist/js/bootstrap.min.js"></script>

  </html>
