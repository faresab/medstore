<?php 
error_reporting(0);
include 'user.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>EM14</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
        <style media="screen">
.printing {display:none;}
        </style>

<body>
<div class="container-fluid">
<div class="row">
<div class="col-md-6">

    <div class="well">
<form id="addform" method="post">
    <input type="text" name="REF" class="form-control" placeholder="Reference" required><br>
    <input type="text" name="PRODUCT" class="form-control" placeholder="Produit" required><br>
   
   <input type="text" name="PRICE_A" class="form-control" placeholder="prix d'achat" required><br>
   <input type="text" name="PRICE_V" class="form-control" placeholder="prix de vente" required><br>
   <input type="text" name="QT" class="form-control" placeholder="Qtt" required><br>
   <div class="input-group">
      <input type="text" name="BARCODE" class="form-control bcbc" placeholder="Code a barre" >
      <span class="input-group-btn">
        <button class="btn btn-default" type="button" id="randombc" ><i class="fa fa-random"></i></button>
      </span>
    </div><br>
    <button type="submit" class="btn btn-primary  btn-block addsubmit">ajouter</button>
    <a class="btn btn-default  btn-block ">Fermer</a>


</form>
</div>




</div>

<div class="col-md-6">

<div style="text-align:center;margin-top:100px;display:none;" class="printbtn">
<a class="btn btn-default btn-lg " >IMPRIMMER</a>
</div>


</div>

</div>

</div>


</body>
    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
<script type="text/javascript">



function printhat(ref,qt,price,barcode) {
  var link = 'bcp.php?bc='+barcode+'&pr='+price+'&ref='+ref+'&qt='+qt ;
 $('.printbtn').fadeIn('slow');
  $('.printbtn').children('a').attr('href',link);
 $('input[name="REF"] , input[name="PRODUCT"], input[name="PRODUCT"], input[name="PRICE_A"], input[name="PRICE_A"], input[name="PRICE_V"], input[name="QT"], input[name="BARCODE"]').val('');
 return false;
}


$(function(){
$('input').first().focus();

$('body').on('keypress',function(e){
    if(e.which == 13) {
        alert('enter');
        return false;
    }
});

        var form = $('#addform');

form.on('submit',function(){
$('.addsubmit').html('<i class=" fa fa-spin fa-spinner"></i>');
$.ajax({
      type: form.attr('method'),
      url: 'ajax/add_submit.php',
      data: form.serialize(),
      success: function(data) {
        $('button.addsubmit').html(data);
      }
    });
    return false;
  });


    $('#randombc').on('click',function() {
var target = $('.bcbc');
if (target.val() == '') {
var x = Math.round((Math.random() * 9999999999999) + 10000000);;
target.val(x);
} else {
alert('code a barre existe');
}
return false;
});



$('input[name="REF"]').on('keyup',function(){
var bcc = $(this).val();
var reff = $('input[name="REF"]').val();
$('#null').load('ajax/vstock.php?bc='+bcc+'&ref='+reff , function(){
});
});


});
</script>


</html> 