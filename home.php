<?php 
error_reporting(0);
include 'user.php';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CODE A BARRE </title>
<link rel="shortcut icon" href="assets/ico/icon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet"> 
    <link href="assets/css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template 
    <link href="theme.css" rel="stylesheet">-->
    <link href="add.css" rel="stylesheet">
<style type="text/css" media="print">
   .no-print { display: none !important; }
   .table { display: table !important; }

</style>
  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container no-print">
<?php include 'menu.php';?>

    </div>

    <div class="container theme-showcase  no-print">

	

	
      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotrontop inverse">
<div class='row'>	  
	  
	  
	<div class="col-md-6 col-lg-6 ">
	<div class="input-group">
    <input type="text" class="form-control barcodefield" placeholder="code a barre">
      <span class="input-group-btn">
        <button class="btn btn-info tgl" type="button"><i class="fa fa-chevron-circle-down"></i></button>
      <!--  <button class="btn btn-default" type="button" id="play"><i class="fa fa-volume-up"></i></button>-->
      </span>
</div></div>	
			
<div class="col-md-6 col-lg-6 ">
<div class="price pull-right">0.00</div>
</div>	
	  

	  
<div style="display:none;" id="extra">	  
<div class="col-md-2 col-lg-2 ">
    <input type="text" class="form-control focused pricefield" placeholder ="prix">
</div> 	  
 	  
	  
	  <div class="col-md-2 col-lg-2 ">

    <input type="number" class="form-control focused qtfield" value="1">
</div>
<div class="col-md-5 col-lg-5 ">
    <input type="text" class="form-control focused productfield flat" placeholder ="Produit">
<div class="suggestionsBox" id="suggestions" style="display: none;">
        <div class="suggestionList " id="autoSuggestionsList"></div></div>

	</div>  

</div>
	  
	  
	   </div>
	   </div>
	   
	   
	
	   
	   
	   <div class="jumbotronmid"><br>
	     <ul id="myTab" class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-barcode"></i> Code a barre </a></li>
      <li><a href="#option2" data-toggle="tab"><i class="fa fa-calendar"></i> Date </a></li>

    </ul><br>
	    <div id="myTabContent" class="tab-content">
		<div class="tab-pane fade in active" id="home">
		<div class="infobar blue" >
  <ul class='ticker'>
  <li></li>
  <li><strong>BIENVENUE</strong></li>
  <li><b class="datetime"></b></li>
  <li><b class="time"></b></li>
</ul>
</div>


	  
<table class="table vtable ">
<tr class="active"><th>N</th><th>REF</th><th width="50%">Produit</th><th>Prix</th><th>Qt</th><th>montant</th></tr>
<tbody>

</tbody>
</table>
</div>
<div class="tab-pane fade in " id="option2">

<!--


-->
<div align="center"><h1 class="time" style="color:#42A7E0;text-shadow:0 1px rgba(13,55,79,0.7);font:bold 68px Arial;line-height:68px"></h1><br>
<h2 class="datetime"></h2></div>
</div>
</div>

	  
	  
	 </div>  </div>
	 </div>  <!-- /container -->
<div class="modal fade printshow">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TOTAL</h4>
      </div>
      <div class="modal-body">
        <h1 id="modaltot">0.00</h1>

		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">anuller</button>
        <a type="button" href="home.php" class="btn btn-primary"><i class="fa fa-angle-double-right"></i> Suivant</a>
        <button type="button" class="btn btn-primary" onclick="PrintElem('.vtable')" ><i class="fa fa-print"></i> Imprimmer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="null"></div>
<div class="onlyprint" style="display:none;">
<h4 style="border:1px solid #6E6E6E; background-color:#E4E4E4 !important;text-align:center;padding:10px;display:block;"></h4>
<h5 style="margin:0;"></h5>
</div>
<audio id="errorplayer" >
<source id="wav" src="assets/error.wav" type="audio/wav">
</audio>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/moment.int.js"></script>
    <script src="assets/js/app-bc.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
	    <script src="dist/mespeak.js"></script>
       <script src="assets/js/newsTicker.js"></script>
		<script>
		meSpeak.loadConfig("dist/mespeak_config.json");
                meSpeak.loadVoice("dist/fr.json");
				function talk(txt) {
					meSpeak.speak(txt,{ variant: 'f3' , Speed : '130'});
				}
				



$(function(){
$('#play,.price').on('click',function(){
//$.get("ajax/lettre.php?total="+total,function(data){
talk('salut fares');
  //  });
return false;
});
});

$(function(){
$('.ticker').newsTicker({
 row_height: 44,
  max_rows: 1,
  duration: 4000,
});
});





idleTime = 0;
$(document).ready(function () {
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 5) { // 20 minutes
        window.location.reload();
    }
}
		</script>
  </body>
</html>
