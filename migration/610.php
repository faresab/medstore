<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Material Design Lite</title>
<link rel="stylesheet" type="text/css" href="../assets/css/material.blue-light_blue.min.css">

<link rel="stylesheet" type="text/css" href="../assets/css/mtl.css">
<link href="../assets/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>



<div class="mdl-spinner mdl-js-spinner is-active"></div>


</body>
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/settings.js"></script>
<script src="../assets/js/material.js"></script>

</html>
