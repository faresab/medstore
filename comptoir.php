<?php




error_reporting(0);
include 'user.php';
include 'conf/inc.php';
$audioclass= ($ini['AUDIO'] == '1'?'glyphicon glyphicon-volume-up':'glyphicon glyphicon-volume-off');
$audiost= ($ini['AUDIO'] == '1'?'true':'false');
$display= ($ini['DISPLAY'] == '1'?'true':'false');



include 'ajax/safe.php';
include $db;

$INFOS = array();
$result_one = $file_db->query("SELECT * FROM settings ");
foreach($result_one as $row) {
$INFOS[$row['key']] = $row['value'];
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>VENTE AU COMPTOIR</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet" media="screen">
    <link href="comptoir/comptoir2.css" rel="stylesheet" media="screen">
<!--    <link href="comptoir/theme.css" rel="stylesheet" media="screen">-->
<style type="text/css" media="screen">
.onlyprint {display:none;}
@media screen {
  .printdiv {display:none;}
}

body {padding-top: 25px;}
/*.pannel,.catlist {height:300px;overflow-y: scroll}*/
</style>

  </head>

  <body>

    <!-- Fixed navbar -->
        <div class="container-fluid no-print">
<?php include 'menu-ui.php';?>
    </div>

<div class="comptoirtopnav">
<div class="comptoirtopnavInfos">
<div class="row">
    <div class="col-sm-5">
<span class="Cname"><?php print $INFOS['Cname'] . '  </span>
   -   <span class="Cadress">' . $INFOS['Cadress'] . '</span>';
?>
</div>





 <div class="col-sm-7 ">
<span class="currentProd"></span>
   <span class="currentQtAvaible"></span>

</div>

</div>
</div>




<div class="comptoirtopnavmainbar">
<div class="row">
<div class="col-md-8">


<div class="row">

<div class="col-sm-3">
<input type="text" data-order ="1" class="form-control refcode" id="inputref" placeholder="REF/CODE"/>
</div>

<div class="col-sm-3">
<input type="text" data-order ="2" class="form-control numbersOnly qt" id="inputqt" placeholder="QUANTITÉ" />
</div>

<div class="col-sm-3">
<input type="text" data-order ="3" class="form-control priceInput numbersOnly" id="inputprice" placeholder="PRIX"/>
</div>
</div>

<div>
<input type="text" data-order ="4" class="form-control prodInput" id="inputprod" placeholder="PRODUIT"/>
  <div class="suggestionsBox" id="suggestions" style="display: none;">
 <div class="suggestionList " id="autoSuggestionsList"></div></div>


</div>



</div>

<div class="col-sm-4">
<div class="comptoirPrix" id="comptoirPrix" style="opacity:0">0.00</div>
</div>


</div>




</div>



</div>



<div class="container-fluid">
<div class="row">
<div class="col-sm-7 tablediv dragscroll " style="padding-right:0;">
<table class="table vtable table-bordered ">
<tr class="active"><th width="50px">N°</th><th width="100px">REF</th><th>DESIGNATION</th><th  width="100px">PRIX</th><th width="60px">QT</th><th width="120px">MONT</th></tr>
<tbody>
</tbody>
</table>
</div>


<div class="col-sm-5 divtouch dragscroll" >


<div class="btns dragscroll">
</div>
<div class="fams dragscroll">
</div>
</div>




</div>
</div>
<img src="" class="picnav">

</img>
   <div class="bottomcontrol animated fadeInUp" >
   <div class="elem"><a href="#"  class="prevButton"><span class="glyphicon glyphicon-backward" tooltip data-placement="top" title="Précédent (CTRL + <-)"></span></a></div>
   <div class="elem"><a href="#" tooltip data-placement="top" title="Pause (CTRL+ESPACE)" class="pauseBtn"><span class="glyphicon glyphicon-pause"></span></a></div>
   <div class="elem" ><a href="#" tooltip data-placement="top" class="nextButton" title="Suivant (CTRL+ ->)"><span class="glyphicon glyphicon-forward"></span></a></div>
   <div class="elem clock">12:00</div>
   <div class="elem">
<a href="#" tooltip class="f8" id="f8" data-placement="top" title="Remise"><span class="glyphicon glyphicon-stats"></span></a></div>
   <div class="elem">
<a href="#" tooltip class="f10" data-placement="top" title="Valider   (F10)"><span class="glyphicon glyphicon-shopping-cart "></span></a></div>
   <div class="elem">
<a href="#" tooltip class="f9" data-placement="top" title="Valider et imprimer (F9)"><span class="glyphicon glyphicon-print"></span></a></div>
  <div class="elem">
<a href="#" tooltip onclick="removerow()" data-placement="top" title="Supprimer (SUPPR)"><span class="glyphicon glyphicon-trash"></span></a></div>
   <div class="elem">
<a tooltip href='#Vers' data-toggle="modal" data-placement="top" title="Versement (CTRL + V)"><span class="glyphicon glyphicon-pencil"></span></a></div>


<div class="elem error" style="display:none;">
<a tooltip href='#'  data-placement="top" title="Erreur"><span  class="glyphicon glyphicon-remove"></span></a></div>

<div class="elem prodname green" >
  </div>





<inf>


</inf>

   </div>




<div style="display:none;width:0;height:0">
<form id="sales">
<input type="hidden" name="HID" ID="HID" value="0" />
<input type="hidden" name="remise"  value="0" />
<input type="hidden" name="card" value=""/>

</form>

</div>












<div class="modal" id="Vers">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">VERSEMENT</h4>
      </div>
      <div class="modal-body">
      <form id="FormVers">
 <table width="100%">
   <tr width="100px"><td>VERSEMENT</td><td><input data-v="0" type="text" class="form-control number" name="versvalue" /></td></tr>
    <tr><td>REST</td><td><input data-v="1" type="text" class="form-control number"  name="restvalue" /></td></tr>
    <tr><td>Nom de client</td><td><input data-v="2" type="text" class="form-control "  name="ClientName" /></td></tr>
  <tr><td>Commentaire </td><td><input data-v="3" type="text" class="form-control "  name="Comment" /></td></tr>
 </table>
<versform></versform>
</form>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal">Annuler</a>
        <a  class="btn btn-success versAdd">Ajouter un Versement</a>
      </div>
    </div>
  </div>
</div>





<div class="modal " id="dialog" aria-labelledby="largeModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TOTAL</h4>
      </div>
      <div class="modal-body">
<div class="row">
<div class="col-xs-6">
<div align="center">A PAYER</div>

    <div class="comptoirPrix small topay" style="">0.00</div>
</div>

<div class="col-xs-6">
<div align="center">MONNAIE</div>
    <div class="comptoirmoney small error change "></div>
</div>

</div>
<hr/>



<table width="100%" style="display:none">
<tr><td width="33%">REMISE</td><td width="33%">MONTANT</td><td width="33%">RECU</td></tr>
<tr><td><input data-order="6" class="form-control number numbersOnly" name="iremise"></td>
<td><input data-order="7" class="form-control number numbersOnly" name="topay"></td>
<td><input data-order="8" class="form-control number numbersOnly" name="given"></td></tr>
</table>



<hr />
<p id="clname" style="color:#fff">

</p>
<table width="100%">
<tr><td width="33%"><input type="text" class="form-control" id='cardReader' name="rfid" placeholder="carte" data-order="9"/></td>
  <td width="33%" class="rfid refid-before" solde align="center">0.00</td>
  <td width="33%" class="rfid refid-after" updatedsolde align="center"></td></tr>
</table>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <a  class="btn btn-success " onclick="validateCard()"><span class="glyphicon glyphicon-print"></span></a>
        <a  class="btn btn-success " onclick="validateCard()"><span class="glyphicon glyphicon-floppy-disk"></span></a>
      </div>
    </div>
  </div>
</div>









<div class="modal" id="subtext">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TEXT</h4>
      </div>
      <div class="modal-body">
      <form id="Sub">
<input rows="5" class="form-control" name="subdata" />
<br><a href="#" class="btn btn-default subApply">MODIFIER</a>
</form>
      </div>

    </div>
  </div>
</div>

<script>
function is_touch_device() {
  return !!('ontouchstart' in window);
}

var stock,fams,promos, alreadyBuilt = false;
getStock();

  function getStock(){
 fetchJSONFile('ajax/json_s.php', function(data){
   if (data.stock && data.stock.length) document.getElementById('comptoirPrix').style.opacity = '1';
 stock = data.stock;
 fams = data.fams;
 promos = data.promos;
 //console.log(data);
if (!alreadyBuilt) {
  if (is_touch_device()) {
    setFams();
     filterFam();
    alreadyBuilt = true;
} else {
document.querySelector('.divtouch').remove();
document.querySelector('.tablediv').classList.remove('col-sm-7');
document.querySelector('.tablediv').classList.add('col-sm-12');
document.querySelector('.tablediv').style.paddingRight = '15px';
  }
  }





 });
 }

 function fetchJSONFile(path, callback) {
 		var httpRequest = new XMLHttpRequest();
 		httpRequest.onreadystatechange = function() {
 				if (httpRequest.readyState === 4) {
 						if (httpRequest.status === 200) {
 							spydata = JSON.stringify(httpRequest.responseText);
 								var data = JSON.parse(httpRequest.responseText);
 								if (callback) callback(data);
 						}
 				}
 		};
 		httpRequest.open('GET', path);
 		httpRequest.send();
 }


</script>


<script src="assets/js/main-functions.js"></script>
<script  src="assets/js/jquery.js" type="text/javascript" ></script>
<script  src="assets/js/sugg.js" type="text/javascript" ></script>
<script  src="assets/js/moment.int.js" type="text/javascript" ></script>
<script  src="assets/js/mousetrap.js" type="text/javascript" ></script>

<script  src="assets/js/comptoir-vanilla.js" type="text/javascript" ></script>
<script  src="comptoir/rfid.js" type="text/javascript" ></script>

<script  src="comptoir/versement.js" type="text/javascript" ></script>
<script  src="assets/js/dragscroll.js" type="text/javascript" ></script>
<script  src="dist/js/bootstrap.min.js" type="text/javascript" ></script>

<script  src="api/api.js" type="text/javascript" ></script>


<script>
var audiostat = <?php print $audiost;?>;
var lcd_display = <?php print $display;?>;




</script>


</body>
</html>
