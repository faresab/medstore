<?php
include 'user.php';
include 'ajax/safe.php';
include $db;

// USERS
$users = '<option value="0">UTILISATEUR</option>';
$result_one = $file_db->query("SELECT * from users");
foreach($result_one as $row) {

$USERS.= '<option value="'.$row['USERNAME'].'">'.$row['USERNAME'].'</option>';
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FIXED</title>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/jquery.pagepiling.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet">
<style type="text/css">
html,body{height:100%;background-color: #fff}
.btn-lg {margin-left:10px;}
.well-sm {border:1px solid rgba(0,0,0,0.5);}
#res {overflow-y: scroll;width:100%;min-height: 400px;height: 500px;
border:1px solid rgba(0,0,0,0.5);background-color: #fff;
  box-shadow: 0 4px 2px -2px rgba(0,0,0,0.4);}
#res::-webkit-scrollbar-track
{
  border-radius: 0px;
  background-color: #FFF;
}

#res::-webkit-scrollbar
{
  height: 10px;
  width: 10px;
  background-color: #FFF;
}

#res::-webkit-scrollbar-thumb
{
  border-radius: 0px;
  background-color: #F50057;
}
table.rel{width:100%;background-color: #fff; width:100%;cursor:pointer;font-family: 'Segoe UI', Tahoma, sans-serif;font-size: 14px;}
table.rel tr{margin:0;padding: 0;height:30px;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:3px;}
table.rel tr th{background-color:#EFEFEF;text-align: center;padding:5px; }
table.rel tr th {border-top: none}
table.rel tr td:first-child,table.rel tr th:first-child {border-left: none}

td[right] {text-align:right}
td[bold] {font-weight: bold}


</style>

  </head>
  <body>
          <div class="container-fluid no-print">

<?php include "menu-ui.php";?> </div>
<h0 class="lime">Historique</h0>

<br><br>

<div class="container">
<div class="well well-sm" style="margin-bottom:-1px">
<div class="row"><div class="col-sm-10">
<select class="form-control users" onchange="filter()">
  <?php print $USERS;?>
</select>
</div>


<div class="col-sm-2"><button class="btn btn-default btn-block">REF</button></div>
</div>
</div>
<div id="res">
 <table class="rel">
     <tr><th>N*</th><th>ACTION</th><th>VALEUR</th><th>UTILISATEUR</th><th width="50px">ANULLER</th></tr>
<tbody></tbody>

 </table>
</div>
</div>



</body>

    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>

<script>

  function filter(){

    $('table.rel').load('ajax/history-load.php',function(){
      $('a.undo').on('click',function(){
var id = $(this).attr('data-undo');
undo(id);
return false;
      });
    });
  }

function undo(x){
  var r = confirm("Confirmer ?");
if (r == true) {

$.ajax({
  url: "ajax/history-undo.php?id="+x,
  context: document.body
}).done(function(data) {
filter();
//alert(data);
});

} else {
}
}


$(document).ready(function(){
    $.ajaxSetup({
    cache: false
});
filter();
});

</script>

</html>
