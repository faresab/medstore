<?php
error_reporting(0);
$lang = 'fr';
 include "../lang/$lang.php";

include '../user.php';
include '../ajax/safe.php';
include $db;
error_reporting(E_ALL);
$start_date = $_POST['date1'];
$end_date = $_POST['date2'];
$start = str_replace('-', '', $start_date);
$end = str_replace('-', '', $end_date);


 $result_one = $file_db->query("SELECT SUM(VALUE) AS SP,SUM(BVALUE) AS SB FROM HID WHERE (DATETIME BETWEEN '$start' AND '$end') ");
foreach($result_one as $row) {
$RECETTE = number_format($row['SP'], 2, ',', ' ');
$RECETTEBEN = number_format($row['SB'], 2, ',', ' ');
}

 $result_one = $file_db->query("SELECT SUM(QT) AS SI FROM vents WHERE (FADATE BETWEEN '$start' AND '$end')  AND REF <> 'RETOUR' AND REF <> 'DEP' AND ( SHOW='1')");
foreach($result_one as $row) {
$RECETTEQT = ($row['SI'] == ''?'0':$row['SI']);
}


$infos = Array();
$result = $file_db->query("SELECT * FROM settings");
foreach($result as $row) {
$infos[$row['id']]  =  $row['value'];
}


// TABLE BY USER
$excell = $tbluser = '';
$result_x =  $file_db->query("SELECT *,SUM(VALUE) AS SUMNT FROM HID WHERE  DATETIME BETWEEN '$start' AND '$end'  GROUP BY USER ORDER BY SUMNT DESC");
foreach($result_x as $row) {
$tbluser.=  '<tr><td>'.$row['USER'].'</td><td class="text-right">'.number_format($row['SUMNT'], 2, ',', ' ').'</td></tr>';
$excell.=  '<tr><td>'.$row['USER'].'</td><td>'.$row['SUMNT'].'</td></tr>';
}
$excell.= "<tr><td></td><td></td></tr><tr><td></td><td></td></tr>";
//sale table
$saletable = '<table class="rel"><tr><th>REF</th><th>PRODUIT</th><th>QT</th><th>MONT</th></tr>';






 $result_thre = $file_db->query("SELECT *,SUM(QT) AS SQV,SUM(MONT) AS SUMNT FROM vents WHERE  FADATE BETWEEN '$start' AND '$end'  AND REF <>'DEP' AND REF<> 'RETURN' AND SHOW='1' GROUP BY PRODUCT ORDER BY SUMNT DESC");
foreach($result_thre as $row) {
$TOPPRODUCT = $row['PRODUCT'];
$TOPREF = $row['REF'];
$TOPBARCODE = $row['BARCODE'];
$SUMNT = $row['SUMNT'];
$SUMNT = number_format($row['SUMNT'], 2, ',', ' ');
$TOPSQV = $row['SQV'];
$saletable.= "<tr><td> $TOPREF </td><td>  $TOPPRODUCT </td><td><b class='pull-right'>$TOPSQV</b></td><td class='text-right'>$SUMNT</td></tr>";
$excell.= "<tr><td>$TOPSQV  $TOPPRODUCT </td><td> $SUMNT</td></tr>";
}





//$file_db->sqliteCreateFunction( 'fadate2', 'sqlitedate', 1);

 $result_thre = $file_db->query("SELECT * FROM COP WHERE  FADATE BETWEEN '$start' AND '$end' AND METHOD ='V'  ORDER BY ID DESC");
foreach($result_thre as $row) {
$VERSEMENT = number_format($row['VERSEMENT'], 2, ',', ' ');
$saletable.= "<tr><td>   </td><td>  VERSEMENT </td><td><b class='pull-right'> </b></td><td class='text-right'>$VERSEMENT</td></tr>";
$excell.= "<tr><td> VERSEMENT</td><td> $VERSEMENT</td></tr>";
}



 $result_thre = $file_db->query("SELECT * FROM dep WHERE  FADATE BETWEEN '$start' AND '$end'  ORDER BY ID DESC");
foreach($result_thre as $row) {
$VAL = number_format((0 - $row['VAL']), 2, ',', ' ');
$DEP = $row['DEP'];
$saletable.= "<tr><td>   </td><td>  DEPENSES $DEP  </td><td><b class='pull-right'> </b></td><td class='text-right'>$VAL</td></tr>";
$excell.= "<tr><td> DEPENSES $DEP </td><td> $VAL</td></tr>";
}



$saletable.= "</table>";




function showdate($tstdate) {
$expdate = explode('-', $tstdate);
return $expdate[2].'/'.$expdate[1].'/'.$expdate[0] ;
}


function sqlitedate($original){
 return str_replace('-', '', $original);
}
?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title></title>
	    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/css/animate.min.css" rel="stylesheet">
    <link href="../add.css" rel="stylesheet">
<style>
	table.rel{width:100%;}
table.rel tr{margin:0;padding: 0;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:3px;}

</style>
<style type="text/css" media="screen">
body {padding-top: 0;}
.ngreen {color:#D50000;}
.nred {color:#00C853;}
	.col-md-3 .well {min-height: 80px;}
	.newp {width:0;height:0;display: none;}
	.onlyprint  {display:none;}
</style>
    <style type="text/css" media="print">
.noprint{display:none!important;}
@page{padding:1cm;}
body {padding:0;margin:0;}
.big1 {font-size: 1.5em;text-transform: uppercase;}
.noborder {border:none !important; padding:0;}
.newp {page-break-after: always;}
.half{width:49%;float:left;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:2px 4px;font-size: 10px;}

</style>

</head>
<body>
<div class="well noprint">
	<a download="RECETTE_<?php print $start_date.'_'.$end_date;?>.xls" href="#" onclick="return ExcellentExport.excel(this, 'dataexcell', 'XS');" class='btn btn-default'> <i class="fa fa-file-excel-o"></i> <?php print SAVEXCELL;?></a>
<a href="#" class="btn btn-default" onclick="window.print()"><i class='fa fa-print'></i> <?php print PRINTER;?></a>
</div>
<div class="container">
<h3 align='center' style='margin-top:0'><?php print $infos['4'];?></h3>
<h4 align='center'><?php print $infos['5'];?></h4>
<table class="rel">
<tr><th>DATE</th><th class="text-right"><?php print showdate($start_date) .' JUSQU\'À  ' .showdate($end_date) ;?> </th></tr>
<tr><td>RECETTE</td><td class="text-right"><b><?php print $RECETTE;?> DA</b></td></tr>
<tr><td>QUANTITE</td><td class="text-right"><?php print $RECETTEQT;?></td></tr>
<?php if (intval($POWER) >= 3 ) {?><tr><td>BENEFICES</td><td class="text-right"><?php print $RECETTEBEN;?></td></tr><?php }?>
</table>
<hr />
<table class="rel" id="datatable">
<tr><th>UTILISATEUR</th><th>RECETTE</th></tr>
<?php print $tbluser;?>
</table>

<br>
<?php print $saletable;?>


</div>


<br><br>
<table class='noprint onlyprint' id="dataexcell">
<tr><td>DATE</td><td><?php print showdate($start_date) .' JUSQU\'À  ' .showdate($end_date) ;?> </td></tr>
<tr><td>RECETTE</td><td><?php print $RECETTE;?></td></tr>
<tr><td>QUANTITE</td><td><?php print $RECETTEQT;?></td></tr>
<?php if (intval($POWER) >= 3 ) {?><tr><td>BENEFICES</td><td><?php print $RECETTEBEN;?></td></tr><?php } ?>
<tr><td></td><td></td></tr>
<tr><td></td><td></td></tr>
<?php print $excell;?>
</table>

    <script src="../assets/js/jquery.js"></script>
<script type="text/javascript" src="../assets/js/excellentexport.min.js"></script>




</body>
</html>
