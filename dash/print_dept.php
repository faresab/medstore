<?php
$lang = 'fr';
 include "../lang/$lang.php";

include '../user.php';
include '../ajax/safe.php';
include $db;


 $result_one = $file_db->query("SELECT SUM(SOLDE) AS SS,COUNT(ID) AS QI FROM clients ");
foreach($result_one as $row) {
$SoldeTotal = number_format($row['ss'], 2, ',', ' ');
$NombreDesClients = ($row['SB'] == null?'0':$NombreDesClients);
}
$table = null;
$result_one = $file_db->query("SELECT * from clients");
foreach($result_one as $row) {
$NAME = $row['NAME'];
$ADRESS = $row['ADRESS'];
$TEL = $row['FAX'] . ' '.$row['TEL1'].' '.$row['TEL2'];
$SOLDE_INIT = number_format($row['SOLDE_INIT'], 2, ',', ' ');
$SOLDE = number_format($row['SOLDE'], 2, ',', ' ');
$table.= "<tr><td>$NAME</td><td>$ADRESS</td><td>$TEL</td><td class='text-right'>$SOLDE_INIT</td><td class='text-right'>$SOLDE</td></tr>";
}


?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title></title>
	    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/font-awesome.css" rel="stylesheet">
    <link href="../assets/css/animate.min.css" rel="stylesheet">
    <link href="../add.css" rel="stylesheet">
<style>
	table.rel{width:100%;}
table.rel tr{margin:0;padding: 0;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:3px;}

</style>
<style type="text/css" media="screen">
body {padding-top: 0;}
.ngreen {color:#D50000;}
.nred {color:#00C853;}
	.col-md-3 .well {min-height: 80px;}
	.newp {width:0;height:0;display: none;}
	.onlyprint  {display:none;}
</style>
    <style type="text/css" media="print">
.noprint{display:none!important;}
@page{padding:1cm;}
body {padding:0;margin:0;}
.big1 {font-size: 1.5em;text-transform: uppercase;}
.noborder {border:none !important; padding:0;}
.newp {page-break-after: always;}
.half{width:49%;float:left;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:2px 4px;font-size: 10px;}

</style>

</head>
<body>
<div class="well noprint">
	<a download="DETTES.xls" href="#" onclick="return ExcellentExport.excel(this, 'dataexcell', 'XS');" class='btn btn-default'> <i class="fa fa-file-excel-o"></i> <?php print SAVEXCELL;?></a>
<a href="#" class="btn btn-default" onclick="window.print()"><i class='fa fa-print'></i> <?php print PRINTER;?></a>
</div>
<div class="container">
<h3 align='center' style='margin-top:0'><?php print $infos['4'];?></h3>
<h4 align='center'><?php print $infos['5'];?></h4>
<table class="rel">
<tr><th>NOMBRE DES CLIENTS</th><th class="text-right"><?php print $NombreDesClients;?></th></tr>
<tr><td>CREDIT TOTAL</td><td class="text-right"><b><?php print $SoldeTotal;?> DA</b></td></tr>

</table>
<hr />
<table class="rel" id="datatable">
<tr><th>CLIENT</th><th>ADRESSE</th><th>TEL</th><th>CREDIT INITAL</th><th>CREDIT</th></tr>
<?php print $table;?>
</table>

<br>



</div>


<br><br>
<table class='noprint onlyprint' id="dataexcell">
<tr><th colspan="4">NOMBRE DES CLIENTS</th><th class="text-right"><?php print $NombreDesClients;?></th></tr>
<tr><td colspan="4">CREDIT TOTAL</td><td class="text-right"><b><?php print $SoldeTotal;?> DA</b></td></tr>

<tr><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td></td><td></td><td></td><td></td><td></td></tr>
<?php print $table;?>
</table>

    <script src="../assets/js/jquery.js"></script>
<script type="text/javascript" src="../assets/js/excellentexport.min.js"></script>




</body>
</html>