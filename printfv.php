﻿<?php
include 'user.php';

include 'ajax/safe.php';
include $db;
$fid = $_GET['fid'];


$infos = Array();
$infos[] = null;
$result = $file_db->query("SELECT * FROM settings ");
foreach($result as $row) {
$infos[] = $row;
}


$inifile = "conf/conf.ini";
$ini = parse_ini_file($inifile);
$logosrc= $ini['LOGOSRC'];
$logoShow= $ini['LOGOBL'];
$SHOWCREDIT = ($ini['SHOWCREDIT'] == '1'?'block':'none');






?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title></title>
    <link href="assets/css/font-awesome.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet">
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

<style>

	table.rel{width:100%;}
table.rel tr{margin:0;padding: 0;}
table.rel tr td,table.rel tr th{border:1px solid #000;padding:3px;}
td.edition , th.edition{display:none;}

/*}*/
</style>

<style type="text/css" media="screen">
body {padding-top: 0px;}
.ngreen {color:#D50000;}
.nred {color:#00C853;}
	.col-md-3 .well {min-height: 80px;}
	.newp {width:0;height:0;display: none;}
	.onlyprint  {display:none;}
  /** {border:1px solid red;}*/
  .container {margin-top:40px;}
</style>


<style type="text/css" media="print">
.noprint{display:none!important;}
@page{padding:0 1cm;}
@page,body{padding:0;margin:0}
body {padding:0;margin:0;}
.big1 {font-size: 1.5em;text-transform: uppercase;font-weight: 300}
.noborder {border:none !important; padding:0;}
.newp {page-break-after: always;}
.half{width:49%;float:left;}
*{
-webkit-print-color-adjust: exact;
print-color-adjust: exact;
}

.logoImage {max-height: 3cm;max-width: 4cm;height:2cm;width:auto;padding:0;margin:0 0  1cm 0;}
td {border-collapse: collapse;}
td {font-weight: 300}
.well {padding:0}
/*table tbody tr td , .well{border-collapse: collapse;border-color:#263238 }*/
tr[soldeInfos] {display: <?php print $SHOWCREDIT;?>;}
</style>

</head>
<body>

<div class="container-fluid noprint">
<h0 class="lime afs"> BON DE LIVRAISON >> <span class="name"></span></h0>
</div>
<?php if ($logoShow == "1" && $logosrc !== '') {?>
<div align="center" style="padding:0;margin:0">
  <img src="ajax/up/<?php print $logosrc;?>" class=" onlyprint logoImage" />
</div>
<?php }?>

<h1 align="center" class="onlyprint" style="margin:0;padding:0;font-size:30px;line-height:36px;font-weight:300">BON DE LIVRAISON</h1>
<div class="container">

<div class="row">
<div class="col-md-3">
<div class="well well-sm">
<b class="big1"><?php print $infos[4]['value'];?></b><br>
<b><?php print $infos[5]['value'];?><br>
<?php print $infos[6]['value'];?></b>
</div>
</div>

<div class="col-md-3 half">
<div class="well well-sm">
<b>CLIENT : <span class="name"></span><br>
ADRESSE : <span class="adress"></span><b>

</div>
</div>

<div class="col-md-3 half">
<div class="well well-sm">
<b>DATE:  <span class="pull-right date"></span><br>
N° : <span class="pull-right fnumber"></span><br>
MONTANT : <span class="pull-right total"></span></b>
</div>
</div>


<div class="col-md-3 noprint">
<div class="well well-sm" align="center">
<a href="javascript:window.print()" class="btn btn-default btn-block"><i class="fa fa-print"></i> Imprimmer</a>
</div>
</div>




</div>



<div class="well well-sm noborder maintbl">

<table class="rel">
<tbody>

</tbody></table>






<br><br>


</div>
<h4 class='lettre'></h4>



</div>


</body>
  <script src="assets/js/moment.int.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
  <script src="dist/js/bootstrap.min.js"></script>
  <script src="assets/js/script_money.js"></script>
<script type="text/javascript">
var relid = '<?php print $fid?>';
	function loadrel(xid) {
$('.maintbl').html('<h3 align="center">CHARGEMENT...</h3>').load('op/load_facture_v.php?fid='+xid ,function(){
var lettre = $('.lettre').text();
var text_lettre = ConvNumberLetter_fr(parseInt(lettre), false);
$('.lettre').html(text_lettre)	;
});
	}

function reload(x) {
	relid = x.value;
	loadrel(x.value);
}


	$(function(){
loadrel(relid);
	});

</script>

</html>
