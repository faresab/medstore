<?php
error_reporting(0);
include 'user.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>EM14</title>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
        <script src="assets/js/Chart.js"></script>
        <style media="screen">

body {
        -webkit-user-select: none; /* webkit (safari, chrome) browsers */
    -moz-user-select: none; /* mozilla browsers */
    -khtml-user-select: none; /* webkit (konqueror) browsers */
    -ms-user-select: none; /* IE10+ */
}
*{border-radius: 0!important;}

.printing ,.printing2{display:none;}
.red{color:red;}
.orange{color:orange;}
.green{color:green;}
a,a:hover,a:focus{text-transform: none;color:#333;}
a.barcode {text-decoration: none;color:#333;background-color: #fff;}
        </style>
<style media="print">
.container {display:none;}
@page{width:100%;min-width: 7.5cm}
.printing,.printing2 {display:block;}
.logo , .logo img{display:block;margin:0 auto;}
.magname {font:bold 12px arial;text-align: center;}
.pproduct {font:normal 14px arial;text-align: center;}
.magaddr,.bc{font:normal 10px arial;text-align: center; margin: 0 auto;}
.prprice {margin-top:2px;font:bold 26px Arial;text-align: center;}
td{padding: 2px !important;font-size: 10px;}
th{padding: 2px !important;font-size: 12px;}
.so {display: none!important;}
.blnk {color:transparent;}
</style>
  </head>

  <body oncontextmenu="return false">

    <!-- Fixed navbar -->
	      <div class="container-fluid">

<?php include 'menu-ui.php';?>

    </div>
<h0 class="so"><i class="fa fa-angle-double-left"></i>PRODUITS /STOCK </h0>
    <div class="container-fluid">





	   <div class="well white flat so" style="min-height:600px;">



<div class="row">
<div class="col-md-8">
<div class="well">
<div class="title-header">LISTE DE STOCK</div>
<div class="input-group">
      <input type="text" class="form-control filter flat" placeholder="Recherche..">
      <span class="input-group-btn">
        <button class="btn btn-default printtbl flat" type="button"><i class="fa fa-print"></i> Imprimmer</button>
      </span>
    </div>


<div class="tblstock"></div></div></div>





































<div class="col-md-4 so">
<div class="well">
<div class="title-header">AJOUTER UN PRODUIT</div>

<form id="addform" method="post" action="ajax/add_submit.php">

  <div class="input-group">
      <input type="text" name="BARCODE" class="form-control bcbc" placeholder="Code a barre" >
      <span class="input-group-btn">
        <a class="btn btn-default" type="button" id="randombc" ><i class="fa fa-random"></i></a>
      </span>
    </div><br>

    <input type="text" name="PRODUCT" class="form-control" placeholder="Produit" required><br>


    <input type="text" name="REF" class="form-control " placeholder="Reference" required><br>





   <input type="text" name="PRICE_A" class="form-control number " placeholder="prix d'achat" required><br>
   <input type="text" name="PRICE_V" class="form-control number " placeholder="prix de vente" required><br>
   <input type="text" name="QT" class="form-control " placeholder="Qtt" required><br>
<br>
<input type="checkbox" name="wp" value="yes" />  perissable
<br><br>
<div class="perm" style="display:none;" >
<table width="100%">
<tr><td>
DATE </td><td>
<input type="date" name="date_exp" class="form-control date-exp" >
</td>
<tr>
<tr>
<td>  m'aventir avant (jours) </td>
<td><input type="number"  name="av-exp" class="form-control av-exp" placeholder="date" >
</td>
</tr></table>

<br><br>
</div>





  <button type="submit" class="btn btn-primary btn-lg btn-block addsubmit">AJOUTER (F3)</button>


</form>


</div>
</div>
</div>
</div>






















	  <div id="null"></div>

<div class="modal fade" id="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">ALERTE</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" data-dismiss="modal">Fermer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="printing">
  <div class="logo"></div>
  <div class="magname"></div>
  <div class="magaddr"></div>

    <div class="pproduct"></div>
  <div class="bc"></div>
  <div class="prprice"></div>
</div>
<div class="printing2"></div>
<script src="assets/js/main-functions.js"></script>
    <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery-barcode.min.js"></script>
  <script src="assets/js/jquery.number.min.js"></script>
    <script src="assets/js/mousetrap.js"></script>
    <script src="assets/js/stock.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/holder.js"></script>

      <script src="assets/js/ALL.js"></script>

  <?php include "plug.php";?>
</body>
</html>
