
<?php
error_reporting(0);
include 'user.php';
include 'ajax/safe.php';
include 'conf/inc.php';
include $db;

$ini_display = ($ini['DISPLAY'] == '1'?'checked':'');
$ini_autoref= ($ini['AUTOREF'] == '1'?'checked':'');
$ini_audio= ($ini['AUDIO'] == '1'?'checked':'');
$ini_cloud= ($ini['CLOUD'] == '1'?'checked':'');
$ini_touch= ($ini['TOUCHSCREEN'] == '1'?'checked':'');
$ini_printer = ($ini['PRINTER'] == '1'?'checked':'');
$LOGOBL = ($ini['LOGOBL'] == '1'?'checked':'');
$SHOWCREDIT = ($ini['SHOWCREDIT'] == '1'?'checked':'');
$LOGORECU = ($ini['LOGORECU'] == '1'?'checked':'');
$logoImgSrc = ($ini['LOGOSRC'] == ''?'null.png':$ini['LOGOSRC']);
$ini_printername= $ini['PRINTERNAME'] ;

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PARAMETRES</title>
<link rel="stylesheet" type="text/css" href="assets/css/material.blue-light_blue.min.css">

 <link rel="stylesheet" type="text/css" href="assets/css/mtl.css">
<link href="assets/css/font-awesome.min.css" rel="stylesheet">


<style>
body{overflow: hidden;}
#notifi {
  position:fixed;left:10px;bottom:10px;
  width:220px;background-color: #CDDC39;color:#333;padding:15px;font-size:16px;
  font-weight: bold;text-transform: uppercase;

  display:none;
}
#notifi.error {
background-color: #F44336;color:#fff;
}


.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
}

.btn-default {
    color: #333333;
    background-color: #ffffff;
    border-color: #cccccc;
}


table.options {  width:100%;}
table.options tr td{  border-top:1px solid rgba(0,0,0,0.1);border-bottom:1px solid rgba(255,255,255,0.5);padding:10px}
h6 {font-weight:bold;}



.fileUpload {
   position: relative;
   overflow: hidden;
   margin:0 10px ;
}
.fileUpload input.upload {
   position: absolute;
   top: 0;
   right: 0;
   margin: 0;
   padding: 0;
   font-size: 20px;
   cursor: pointer;
   opacity: 0;
   filter: alpha(opacity=0);
}
</style>

    </head>

    <body>


























      <ul class="swiftmenu">
        <li>
          Fichier
          <ul>
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Tableau de bord</a></li>
             <li class="divider"></li>
          <?php if (intval($POWER) >= 2 ) {?>  <li><a href="history.php"><i class="fa fa-history"></i> Historique</a></li><?php }?>
            <li><a href="searchform.php"><i class="fa fa-search"></i> Recherche</a></li>
            <li><a href="index.php"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
            <li><a href="javascript:window.close()"><i class="fa fa-times"></i> Quiter</a></li>

          </ul>
        </li>

      <li>
         Liste
          <ul>
          <?php if (intval($POWER) >= 2 ) {?>
            <li><a href="pstock.php"><i class="fa fa-cubes"></i> Stock</a></li>
            <li><a href="fournisseurs.php"><i class="fa fa-user"></i> Fournisseurs</a></li>
            <li><a href="clients.php"><i class="fa fa-users"></i> Clients</a></li>
            <li><a href="promotions.php"><i class="fa fa-flag"></i> Promotions</a></li>
                  <li class="divider"></li>
      <?php }?>
            <li><a href="listefa.php"><i class="fa fa-history"></i> Bons de reception</a></li>
            <li><a href="listefv.php"><i class="fa fa-history"></i> Bons de livraison</a></li>
            <li><a href="userlist.php"><i class="fa fa-users"></i> Utilisateurs</a></li>

          </ul>

      </li>








        <li>
          Achat
          <ul>
            <li><a href="fa.php"><i class="fa fa-file-text-o"></i> Bon de reception </a></li>
                <li><a href="listefa.php"><i class="fa fa-history"></i> Historique d'achat</a></li>
       <?php if (intval($POWER) >= 2 ) {?><li class="divider"></li>   <li><a href="frel.php"><i class="fa fa-file-o"></i> Relevé des fournisseurs</a></li>
      <?php }?>
          </ul>
        </li>

        <li>
          Vente
          <ul>
                <li><a href="comptoir.php"><i class="fa fa-desktop"></i> Vente au comptoir</a></li>

                    <li><a href="fv.php"><i class="fa fa-file-text-o"></i> Bon de livraison / Facture</a></li>
                <li><a href="listefv.php"><i class="fa fa-history"></i> Historique de vente</a></li>
            <li><a href="return.php"><i class="fa fa-eraser"></i> Retours</a></li>
      <li class="divider"></li>
      <?php if (intval($POWER) >= 2 ) {?> <li><a href="total.php"><i class="fa fa-calendar-o"></i> Recettes</a></li><li class="divider"></li><?php }?>

       <?php if (intval($POWER) >= 2 ) {?>     <li><a href="crel.php"><i class="fa fa-file"></i> Relevé des clients</a></li><?php }?>
            <li><a href="facture_simple.php"><i class="fa fa-file-o"></i> Facture simple</a></li>


          </ul>
        </li>




      <li>
          Action
          <ul>
           <?php if (intval($POWER) >= 2 ) {?>    <li><a href="inventory.php"><i class="fa fa-list"></i> INVENTAIRE</a></li><?php }?>
            <li><a href="vers.php"><i class="fa fa-eraser"></i> VERSEMENTS</a></li>
        <?php if (intval($POWER) >= 2 ) {?>    <li><a href="dep.php"><i class="fa fa-money"></i> DEPENSES</a></li><?php }?>
            <li><a href="bcp.php"><i class="fa fa-barcode"></i> IMPRIMMER CODE-BARRE</a></li>
          </ul>
        </li>


        <li>
          Outils
          <ul>

       <?php if (intval($POWER) >= 2 ) {?>    <li><a href="userlist.php"><i class="fa fa-users"></i>UTILISATEURS</a></li><?php }?>
       <?php if (intval($POWER) >= 2 ) {?>    <li><a href="addons.php"><i class="fa fa-puzzle-piece"></i>ADDONS</a></li>  <li class="divider"></li><?php }?>

            <li><a href="javascript:phpdesktop.ToggleFullscreen()"><i class="fa fa-arrows-alt"></i> PLEIN ECRAN</a></li>
            <li><a href="settings.php"><i class="fa fa-wrench"></i> PARAMETRES</a></li>
          </ul>
        </li>

      </ul>


















<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header mdl-layout--fixed-tabs">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <!-- Title -->
      <span class="mdl-layout-title"> <i class="fa fa-cog"></i> PARAMETRES</span>
    </div>
    <!-- Tabs -->
    <div class="mdl-layout__tab-bar mdl-js-ripple-effect">
      <a href="#scroll-tab-0" class="mdl-layout__tab is-active">GENERAL</a>
      <a href="#scroll-tab-1" class="mdl-layout__tab">MOT DE PASSE</a>
      <a href="#scroll-tab-2" class="mdl-layout__tab">NOM D'UTILISATEUR</a>
      <a href="#scroll-tab-3" class="mdl-layout__tab">INFORMATIONS</a>
      <a href="#scroll-tab-4" class="mdl-layout__tab">SUPPRESSION DES DONNÉES</a>
      <a href="#scroll-tab-5" class="mdl-layout__tab">IMPRIMANTES</a>
      <a href="#scroll-tab-6" class="mdl-layout__tab">ACTIVATION DE LOGICIEL</a>

    </div>
  </header>


  <main class="mdl-layout__content">





    <section class="mdl-layout__tab-panel is-active" id="scroll-tab-0">
      <div class="page-content">

<div class="setting-well well-200 mdl-shadow--2dp" />
<h6>Options</h6>


<table class="options">
<tr><td>
Referencement automatique
</td><td>
  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect switch-autoref" for="switch-autoref">
    <input type="checkbox" id="switch-autoref" data-ini='AUTOREF' class="mdl-switch__input " <?php print $ini_autoref;?> />
    <span class="mdl-switch__label"></span>
  </label>
</td></tr>





<tr><td>
Sauvegarde automatique</td><td>
  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-3">
    <input type="checkbox" id="switch-3" data-ini='CLOUD' class="mdl-switch__input"  <?php print $ini_cloud;?>/>
    <span class="mdl-switch__label"></span>
  </label>
</td></tr>

<!-- <tr><td>
Ecran tactile
</td><td>
  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-touch">
    <input type="checkbox" id="switch-touch" data-ini='TOUCHSCREEN' class="mdl-switch__input"  <?php print $ini_touch;?>/>
    <span class="mdl-switch__label"></span>
  </label>
</td></tr> -->

<tr><td>
Afficheur des prix</td><td>
  <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-displ">
    <input type="checkbox" id="switch-displ" data-ini='DISPLAY' class="mdl-switch__input"  <?php print $ini_display;?>/>
    <span class="mdl-switch__label"></span>
  </label>
</td></tr>







</table>
</div>
















      </div>
    </section>





















    <section class="mdl-layout__tab-panel " id="scroll-tab-1">
      <div class="page-content">

<div class="setting-well well-350 mdl-shadow--2dp" />
<h6>Changer le mot de passe</h6>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="passold" />
    <label class="mdl-textfield__label" for="passold">Ancien</label>
  </div>
<br>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="passnew1" />
    <label class="mdl-textfield__label" for="passnew1">Nouveau</label>
  </div>

<br>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="passnew2" />
    <label class="mdl-textfield__label" for="passnew2">Nouveau (confirmer)</label>
  </div>
<br>


<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " id="changepass">
  Changer le mot de passe
</button>





      </div>
      </div>
    </section>











    <section class="mdl-layout__tab-panel" id="scroll-tab-2">
      <div class="page-content">







<div class="setting-well well-200 mdl-shadow--2dp" />
<h6>Changer le nom d'utilisateur (<?php print $USER;?>)</h6>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="userChange"  value="<?php print $USER;?>" />
    <label class="mdl-textfield__label" for="userChange">UTILISATEUR</label>
  </div>
<br>

<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " id="changeuser">
  CHANGER
</button>
      </div>












      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="scroll-tab-3">
      <div class="page-content">


<div class="setting-well well-350 mdl-shadow--2dp" />
<form role="form" id="changeInfos">
<h6>Changer les informations</h6>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="Cname" value=" " />
    <label class="mdl-textfield__label" for="Cname">Nom d'enterprise</label>
  </div>
<br>

<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="Cadress" value=" " />
    <label class="mdl-textfield__label" for="Cadress">Adresse</label>
  </div>
<br>


<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="Cphone" value=" " />
    <label class="mdl-textfield__label" for="Cphone">Telephone</label>
  </div>
<br>



<button  type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " >
  CHANGER
</button>

</form>


</div>

<div class="setting-well well-350 mdl-shadow--2dp" />


<form role="form">
<h6>Logo</h6>

<div class="fileUpload btn btn-default">
         <span>Changer la photo</span>
         <input type="file" class="upload" id="pictureUpload">

     </div>

     <button type="button" name="button" class="btn btn-default" onclick="unloadPhoto()"><i class="fa fa-trash"></i></button>


<!-- <hr> -->
<img id="logoImage" src='ajax/up/<?php print $logoImgSrc;?>' style="height:100px;width:auto;max-width:260px;display:block;margin:30px auto;" />




<br>





</form>
      </div>


<br><br>



      </div>
    </section>
    <section class="mdl-layout__tab-panel" id="scroll-tab-4">
      <div class="page-content">


<div class="setting-well well-200 mdl-shadow--2dp" />
<h6>Réinitialiser les données</h6>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="resetpass" />
    <label class="mdl-textfield__label" for="resetpass">Mot de passe</label>
  </div>
<br>

<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " id="reset">
  Réinitialiser
</button>
      </div>





      </div>
    </section>




    <section class="mdl-layout__tab-panel" id="scroll-tab-5">
      <div class="page-content">

        <div class="setting-well well-200 mdl-shadow--2dp" />
        <h6>Imprimante des recus</h6>
        <table  class="options">
          <tr><td>
          Activer l'imprimante
          </td><td>
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect switch-autoref" for="switch-printer">
              <input type="checkbox" id="switch-printer" data-ini='PRINTER' class="mdl-switch__input " <?php print $ini_printer;?> />
              <span class="mdl-switch__label"></span>
            </label>
          </td></tr>


          <tr><td>
          Afficher le logo sur le ticket de Caisse</td><td>
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="s8">
              <input type="checkbox" id="s8" data-ini='LOGORECU' class="mdl-switch__input"  <?php print $LOGORECU;?>/>
              <span class="mdl-switch__label"></span>
            </label>
          </td></tr>

          <tr><td colspan="2">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input class="mdl-textfield__input printername" type="text" value="<?php print $ini_printername;?>" id="printername" />
              <label class="mdl-textfield__label" for="printername">Nom d'imprimante</label>
            </div>

          </td></tr>


        </table>


        </div>




        <div class="setting-well well-200 mdl-shadow--2dp" />
        <h6>Imprimante des factures</h6>
        <table  class="options">

          <tr><td>
          Afficher le logo sur le bon de livraison</td><td>
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="s4">
              <input type="checkbox" id="s4" data-ini='LOGOBL' class="mdl-switch__input"  <?php print $LOGOBL;?>/>
              <span class="mdl-switch__label"></span>
            </label>
          </td></tr>

          <tr><td>
          Afficher l'etat de crédit sur le bon de livraison</td><td>
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="s7s">
              <input type="checkbox" id="s7s" data-ini='SHOWCREDIT' class="mdl-switch__input"  <?php print $SHOWCREDIT;?>/>
              <span class="mdl-switch__label"></span>
            </label>
          </td></tr>

        </table>

        </div>






      </div>
    </section>









    <section class="mdl-layout__tab-panel" id="scroll-tab-6">
      <div class="page-content">





<div class="setting-well well-200 mdl-shadow--2dp activationTab" />
<h6>Votre code est <span class="tempo"></span></h6>
<h6 acInfos></h6>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input ac-key" type="text" id="given" />
    <label class="mdl-textfield__label" for="given">code d'activation</label>
  </div>
<br>

<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent " id="Act">
  ACTIVER
</button>
      </div>











      </div>
    </section>


  </main>
</div>






















<div id="null" style="display:none"></div>
<div id="notifi" class="mdl-shadow--2dp">

</div>
    </body>



    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/settings.js"></script>

    <script src="assets/js/material.js"></script>

      <!-- <script src="assets/js/ALL.js"></script> -->


    </html>
