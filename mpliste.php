<?php
include 'user.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LISTE DES COURS</title>
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/liste.css" media="screen" title="no title" charset="utf-8">


  </head>
  <body>

    <?php include 'menu-ui.php';?>

<div class="control">
  <div class="row">
<div class="col-xs-6">
  <input type="text" autofocus name="name" value="" class="form-control search" >
</div>

<div class="col-xs-6">
  <a href="#" class="btn btn-default btn-sm" onclick="popup(0)" >Ajouter</a>
  <span id="count"></span>
</div>

</div>




</div>


<div class="maintable">
<table class="tbl">
  <tr><th width="60px">N°</th><th  width="120px">CODE</th><th>TITRE</th><th  width="200px">ENSEIGNANTS</th><th  width="60px">PAGES</th><th   width="60px">FORMAT</th></tr>
<tbody id="res">

  <tr class="current"><td width="50px"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>



</tbody>


</table>
</div>



<script src="js/liste.js" charset="utf-8"></script>
<script src="assets/js/jquery.js"></script>

<script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
