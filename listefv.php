<?php
include 'user.php';

include 'ajax/safe.php';
include $db;

// GET FUOURN
$Fourns = Array();
$selectfilter = "<select class='form-control fselect selectpicker' data-live-search='true'  onchange=\"x=this.value;filter();\"><option value=''>TOUT LES CLIENTS</option>";
 $result = $file_db->query("SELECT * FROM clients");
foreach($result as $row) {
$ID =  $row['ID'];
$NAME =  $row['NAME'];
$Fourns[$ID] = $row['NAME'];
$selectfilter.= "<option value='$ID'>$NAME</option>";
}
$selectfilter.="</select>";

$listfa = '<table class="rel">';
$listfa.= '<tr><th>N°</th><th>DATE</th><th>FOURNISSEUR</th>
<th>VALEUR</th><th>REGLE</th><th>RESTE</th><th>ACTION</th><th>ACTION</th></tr>';

 $resultf = $file_db->query("SELECT * FROM ACHAT ORDER BY ID DESC");
foreach($resultf as $row) {
$FID =  $row['ID'];
$FOURNID =  $row['FOURNID'];
$FOURNAME =  $Fourns[$FOURNID];
$N_FACTURE =  $row['N_FACTURE'];
$DATE_FACTURE =  $row['DATE_FACTURE'];
$TOTAL =  $row['TOTAL'];
$REGLE =  $row['REGLE'];
$RESTE =  $row['RESTE'];
$listfa.= "<tr><td>$N_FACTURE</td><td>$DATE_FACTURE</td>
<td>$FOURNAME</td><td>".nf($TOTAL)."</td><td>".nf($REGLE)."</td><td>".nf($RESTE)."</td><td class='text-center'><a class='btn btn-default'>Imprimmer</a></td>";

}

$listfa.="</table>";


function nf($x){
  return number_format($x, 2, ',', ' ');
}


?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>FACTURES DE VENTE</title>
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="add.css" rel="stylesheet">
        <script src="assets/js/Chart.js"></script>
        <style media="screen">

body {
        -webkit-user-select: none; /* webkit (safari, chrome) browsers */
    -moz-user-select: none; /* mozilla browsers */
    -khtml-user-select: none; /* webkit (konqueror) browsers */
    -ms-user-select: none; /* IE10+ */
}
*{border-radius: 0!important;}
.total{text-align: center;font-size: 18px;color:#1717FF;font-weight: bold;}
.leftpanel {
position: fixed;
  top: 60px;
  left: 0;
  background-color: #292c2f;
  width: 250px;
  height: 100%;
  padding: 20px 0;
  /*text-align: center;*/
  padding:10px 10px;
  pointer-events: auto;
}
.leftpanel h4 {  color:#eee;text-shadow: 1px 1px #000;}
.leftpanel hr {height:0;border-bottom: 1px solid rgba(0,0,0,0.5);border-top: 1px solid rgba(255,255,255,0.1);margin:10px 0;}
.leftpanel table tr td {color:#eee;}
.leftpanel.inv {pointer-events: none;}

.rightpanel{
	padding-left:250px;
	display:block;
}

#res {overflow-y: auto;width:100%;min-height: 500px;height: 500px;
border:1px solid rgba(0,0,0,0.5);background-color: #fff;
  box-shadow: 0 4px 2px -2px rgba(0,0,0,0.4);}
#res::-webkit-scrollbar-track {border-radius: 0px; background-color: #FFF;}
#res::-webkit-scrollbar{height: 10px;width: 20px;background-color: #FFF; }
#res::-webkit-scrollbar-thumb{border-radius: 0px;background-color: #292C2F;}
table.rel{-webkit-user-select: none;width:100%;background-color: #fff; width:100%;cursor:pointer;font-family: 'Segoe UI', Tahoma, sans-serif;font-size: 14px;}
table.rel tr{margin:0;padding: 0;height:28px;}
table.rel tr td,table.rel tr th{border:1px solid #CECECE;padding:1px;}
table.rel tr th{background-color:#EFEFEF;text-align: center;padding:5px; }
table.rel tr th {border-top: none}
table.rel tr th.order {background-color: #18FFFF}
table.rel tr td:first-child,table.rel tr th:first-child {border-left: none}
table.rel tr[data-pid]:hover {background-color: #F0F0F0 }

td[right] {text-align:right}
td[center] {text-align:center}
/*td[bold] {font-weight: bold}*/
table.rel tr td.warning{background-color:#FBD560;}
table.rel tr td.danger{background-color:#FF5B5B; }
tr.curr {  background-color: #DAE8F3;}
tr.curr td{ font-weight: bold;}
td[min] {background-color:#FEDFDA;-webkit-animation: clign 1s ease infinite; }
.lime a {text-decoration: none;color:#000}

table tr:hover td {background-color: #DAE8F3}

.details {position: fixed;right:20px;bottom: 0;height:0;background: #263238;width: 500px;opacity:0;pointer-events: none;transition: all 0.8s ease;color:#fff;padding:10px;}
.details.ready {opacity:1;pointer-events: auto;min-height: 40%;height:auto;transition: all 0.8s ease;padding-bottom: 40px;}
.table-inverse th {color:#111}




        </style>

  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container-fluid">

<?php include 'menu-ui.php';?>
<h0 class="fv"><a href="#toggle"><i class="fa fa-bars"></i></a> BONS DE LIVRAISON / FACTURES DE VENTE</h0>

    </div>


    <div class="leftpanel">

    <h4>FILTRE</h4>
    <input type="date" class="form-control datebegin" placeholder="recherche"><br>

    <hr>
    <h4>FILTRE / CLIENTS</h4>

  <?php print $selectfilter;?>


    <hr>



    <table width="100%">
      <tr><td> CHIFFRE D'AFFAIRE</td><td class="text-right"><span id="allc"></span></td></tr>
      <tr><td>TOTAL REGLE</td><td class="text-right"><span id="allreg"></span></td></tr>
      <tr><td>TOTAL RESTE </td><td class="text-right"><span id="allrest"></span></td></tr>

    </table>


    </div>














    <div class="rightpanel">
      <div id="res">
      <?php print $listfa;?>
      </div>

    </div>







<div class="details">


<div class="btn-group btn-group-justified" style="position:absolute;bottom:0;left:0;padding:5px;" role="group" aria-label="Justified button group">
  <a href="#" class="btn btn-warning btn-edit" role="button">Modifier</a>
  <a href="#" class="btn btn-info btn-print" role="button" target="blank" >Imprimmer</a>
  <!-- <a href="#" class="btn btn-default" role="button">Right</a> -->
</div>


client : <span clientname></span><br>
date : <span date></span> <br>
<table class="table table-bordered table-inverse">
<tr><th>REF</th><th>DÉSIGNATION</th><th>PRIX</th><th>QT</th><th>MONT</th></tr>
<tbody id="detres">

</tbody>
</table>



</div>

	  <div id="null"></div>

<div class="modal fade dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button  class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>



    <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery-barcode.min.js"></script>
  <script src="assets/js/jquery.number.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
     <script src="assets/js/bootstrap-select.min.js"></script>
  <script src="assets/js/ALL.js"></script>




<script type="text/javascript">

var x = d1 = d2 = '';

document.querySelector('.datebegin').addEventListener('change',function(){
d1 = this.value ;
filter();
});



$(function(){
filter();
});

function filter() {
$('#res').html('<h4 align="center"><br><br><br>Charegement...</h4>').load('op/load_list_fv.php?fid='+x+'&d1='+d1+'' , function(){

 });
}


$(function(){
  $('.selectpicker').selectpicker();
$('#d1').on('change',function(){
d1 = $(this).val();
filter();
});





});











function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}










$(function(){
  leftout();

$('a[href="#toggle"]').on('click',function(){


if (panlefton) {
leftout();
} else {
  leftin();

}

});

return false;
});


var panleftoff = false;
var panlefton = true;


function leftout(){
  $('.leftpanel').animate({
      width: "0",
      opacity:"0"
    }, 500, function() {

$('.rightpanel').css('padding-left','0');
panleftoff = true; panlefton = false;
document.querySelector('.leftpanel').classList.add('inv');

    });
return false;
}


function leftin(){
  $('.leftpanel').animate({
      width: "250",
      opacity:"1"
    }, 500, function() {
$('.rightpanel').css('padding-left','250px');
document.querySelector('.leftpanel').classList.remove('inv');
panleftoff = false; panlefton = true;

    });
}



$(function(){
var windowHeight = $(window).height();
$('#res').css('height',(windowHeight - 60));
});



var currentfid;
function showdetails(fid){
  if (fid == currentfid)  {
    document.querySelector('.details').classList.toggle('ready');
  } else {

    document.querySelector('.details').classList.add('ready');

  currentfid = fid;

 fetchJSONFile('ajax/jsonavvente.php?fid='+fid , function(d){
console.log(d);

var detailsHtml = '';
if (d.DETAILS && d.DETAILS.length) {
for (var i = 0; i < d.DETAILS.length; i++) {
  detailsHtml += '<tr><td>'+d.DETAILS[i].REF+'</td><td>'+d.DETAILS[i].PRODUCT +'</td><td class="text-right">'+fm(d.DETAILS[i].PRICE)+'</td><td>'+d.DETAILS[i].QT+'</td><td class="text-right">'+fm(d.DETAILS[i].MONT)+'</td></tr>'
  //d.DETAILS[i]
}
}
document.getElementById('detres').innerHTML = detailsHtml;
document.querySelector('.btn-print').setAttribute('href','printfv.php?fid='+fid);
document.querySelector('.btn-edit').setAttribute('href','fv.php?pid='+fid);
document.querySelector('[clientName]').innerText = d.MAIN.clientname;
document.querySelector('[date]').innerText = d.MAIN.date;
 })

   }
}



function fetchJSONFile(path, callback) {
   var httpRequest = new XMLHttpRequest();
   httpRequest.onreadystatechange = function() {
       if (httpRequest.readyState === 4) {
           if (httpRequest.status === 200) {
             spydata = JSON.stringify(httpRequest.responseText);
               var data = JSON.parse(httpRequest.responseText);
               if (callback) callback(data);
           }
       }
   };
   httpRequest.open('GET', path);
   httpRequest.send();
}
</script>










  <?php include "plug.php";?>
</body>
</html>
