<?php

$l = array();

$l['LANGUAGE'] = '';
$l['DIR'] = '';
$l['LOADING'] = 'LOADING';
$l['WELCOME'] = 'BIENVENUE';
$l['BARCODE'] = 'CODE A BARRE';
$l['STOCK'] = 'STOCK';
$l['SEARCH'] = 'RECHERCHE';
$l['RETURNS'] = 'RETOURS';
$l['INCOME'] = 'RECETTES';
$l['TOOLS'] = 'OUTILS';
$l['ADD_ONS'] = 'ADD_ONS';
$l['EXPENSES'] = 'DÉPENSES';
$l['SETTINGS'] = 'PARAMETRES';
$l['QUIT'] = 'QUITER';
$l['PASSWORD'] = 'MOT DE PASSE';
$l['CONFIRM'] = 'CONFIRMER';
$l['SERIAL'] = 'NUMERO DE SERIE';
$l['ACTIVATE'] = 'ACTIVER';
$l['INCORRECT_KEY'] = 'NEMURO INCORRECT';
$l['PRODUCT'] = 'PRODUIT';
$l['PRICE'] = 'PRIX';
$l['REF'] = 'REFERENCE';
$l['QT'] = 'QT';
$l['AMOUNT'] = 'MONTANT';
$l['TOTAL'] = 'TOTAL';
$l['UNDO'] = 'ANNULER';
$l['NEXT'] = 'SUIVANT';
$l['PRINT'] = 'IMPRIMMER';
$l['DETTES'] = 'DETTES';




?>