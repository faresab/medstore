<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CODE A BARRE</title>
<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
    <link href="assets/css/receip.css" rel="stylesheet" media="print">

    <link href="add.css" rel="stylesheet" media="screen">
<style type="text/css" media="screen">
.onlyprint {display:none;}
@media screen {
  .printdiv {display:none;}
  *{border-radius:0!important;}
}
</style>

  </head>

  <body>

	      <div class="container-fluid no-print">
<?php include 'menu-ui.php';?>

    </div>
<!--<h0 class="lime"><span class="mn"></span>  <b class="datetime"></b> <b class="time"></b> </h0>-->
    <div class="container-fluid  no-print">
      <div class="jumbotrontop">
<div class='row' style="height:55px;">	  

<div class="col-md-2 col-lg-2 ">
    <input type="text" class="form-control barcodefield flat" placeholder="code a barre">
</div>
<div class="col-md-6 col-lg-6 " >
 <input type="text" class="form-control focused productfield flat" placeholder ="Produit"/>
 
	

  </div>


<div class="col-md-2 col-lg-2 ">
    <input type="text" class="form-control focused pricefield flat" placeholder ="Prix" >
</div>
  <div class="col-md-2 col-lg-2 ">

    <input type="number" min="1" class="form-control focused qtfield flat" value="1" >
</div>
</div>
	
 


<div class="row" style="margin-bottom:20px;">
<div class="col-md-7 col-lg-7 ">


    <div class="info">
<span class="mn"></span> <br> <b class="datetime"></b> <b class="time"></b><br>
<span class="rst"></span>
</div>

</div>
<div class="col-md-5 col-lg-5 ">
<div class="price pull-right reflectBelow">0.00</div>

</div> 
</div>  
<br>







	   </div>
	   

	   
	   <div class="jumbotronmid"><br>

  





	  
<table class="table vtable">
<tr class="active"><th>N</th><th>REF</th><th width="50%">Produit</th><th>Prix</th><th width="100px">Qt</th><th>montant</th></tr>
<tbody>

</tbody>
</table>



	  
	  
	 </div>  </div>
	 </div>  <!-- /container -->

	 
	 
	 
	 
	 <div class="modal fade printshow">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TOTAL</h4>
      </div>
      <div class="modal-body">
        <h1 id="modaltot">0.00</h1>

		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">anuller</button>
        <a type="button" href="bc.php" class="btn btn-primary"><i class="fa fa-angle-double-right"></i> Suivant</a>
        <button type="button" class="btn btn-primary" onclick="PrintElem('.vtable')" ><i class="fa fa-print"></i> Imprimmer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
	 
	 <div id="logo" align="center"></div>
	 
	 
	 
	 
	 
<div id="null"></div>
<div class="printdiv"></div>
<div class="onlyprint" >
<h4 style="border:1px dotted #000; text-align:center;padding:0.2cm;"></h4>
<span class="addr" style="display:block"></span>
</div>
<audio id="errorplayer" >
<source id="wav" src="assets/error.wav" type="audio/wav">
</audio>

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/sugg.js"></script>
  <script src="assets/js/moment.int.js"></script>
  <script src="assets/js/dblc.js"></script>
  <script src="assets/js/script_money.js"></script>
    <script src="assets/js/app-bc.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>

    <script>

$(function(){
$('#null').load('ajax/vnotifi.php');
});

</script>
<?php include "plug.php";?>
</body>
</html>
