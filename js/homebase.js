var liste,all,printers;
var price ;


function filter(){
var searchtext = document.getElementsByClassName('key')[0].value;
fetchJSONFile('ajax/mpjsonhome.php?key='+encodeURIComponent(searchtext) , function(x){
liste = x.liste;
all = x.alldata;
price = parseFloat(x.settings[3].val);
printers = x.printers;
  buildTable();
  buildPrinters();
  console.log(price);

});
}

document.getElementsByClassName('key')[0].addEventListener('keyup',function(){
  filter();
});

filter();




function buildTable () {
  var table = '';
  if (liste && liste.length) {
  for (var i = 0; i < liste.length; i++) {
var fileformat = (liste[i].format == 'word'?'<i class="fa fa-file-word-o"></i>':'<i class="fa fa-file-powerpoint-o"></i>')
    table += '<tr title="QT:'+liste[i].qt+'" qt="'+liste[i].qt+'" pid="'+liste[i].id+'" onclick="addtosales(this)"><td>'+liste[i].code+'</td><td>'+liste[i].titre+'</td><td>'+liste[i].ensei+'</td><td>'+liste[i].rota+'</td><td>'+liste[i].pages+'</td><td align="center">'+fileformat+'</td></tr>';
  }
}
document.getElementById('res').innerHTML = table;
}


var currentPrinter = 0;

function buildPrinters(){
  var prHtml  ='';
if (printers && printers.length) {
  for (var i = 0; i < printers.length; i++) {
    var zz = i + 1;
    var curr = (zz == 1?'current first':'')
  prHtml += '<div class="pr '+curr+'" onclick="selectPrinter(this)" printeri="'+i+'" printerid="'+printers[i].id+'" >'  + zz +'<span>'+printers[i].name+'</span> </div>';
  }
}
document.getElementsByClassName('printers')[0].innerHTML = prHtml;
console.log(printers);
}






function selectPrinter(elem){
document.querySelector('.pr.current').classList.remove('current');
elem.classList.add('current');
currentPrinter = parseInt(elem.getAttribute('printeri'));
}

function nextPrinter(){
var nextPn = currentPrinter + 1 ;
if (document.querySelector('[printeri="'+nextPn+'"]')) {
  selectPrinter(document.querySelector('[printeri="'+nextPn+'"]'));
} else {
  selectPrinter(document.querySelectorAll('.pr')[0]);
}
}




function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
               callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}
