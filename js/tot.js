var generalData;

function setDate(){
  var begin  = document.querySelector('[name="begin"]').value;
  var end  = document.querySelector('[name="end"]').value;
  GetJSON('php/totjson.php?b='+begin+'&e='+end , function(data){
console.log(data);
generalData = data;
buildTable();
  });
}



setDate();


function buildTable (){
  var mainInfos  = {totalR : 0 , totalA : 0 , totalP : 0}
  htmlTable  = '';
  if (generalData && generalData.length) {
    for (var i = 0; i < generalData.length; i++) {
if (generalData[i].type == 'r') mainInfos.totalR += mf(generalData[i].value);
if (generalData[i].type == 'v') mainInfos.totalP += mf(generalData[i].value);
if (generalData[i].type == 'n') mainInfos.totalA += mf(generalData[i].value);

var classAndName = filterArray(generalData[i].type);
      htmlTable += '<tr class="'+classAndName[1]+'"><td></td><td>'+generalData[i].name+'</td><td>'+classAndName[0]+'</td><td class="text-right">'+fm(generalData[i].value)+'</td><td>'+generalData[i].user+'</td></tr>';
    }
  }

  document.getElementById('details').innerHTML = htmlTable;
  document.querySelector('[totalA]').innerHTML = fm(mainInfos.totalA);
  document.querySelector('[totalR]').innerHTML = fm(mainInfos.totalR);
  document.querySelector('[totalP]').innerHTML = fm(mainInfos.totalP);
}





function filterArray(x){
var arr = [];
if (x == 'r'){ arr.push('Recharge') ;  arr.push('danger') ; }
if (x == 'v'){ arr.push('Paiement') ;  arr.push('warning') ; }
if (x == 'n'){ arr.push('Abbonement') ;  arr.push('info') ; }

return arr;

}




function GetJSON(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
              spydata = JSON.stringify(httpRequest.responseText);
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}

function fm(Money) {
  return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

function mf(Str) {
  return parseFloat(Str.replace(',','.').replace(' ',''));
}
