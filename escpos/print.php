<?php
require_once(dirname(__FILE__) . "/Escpos.php");
$now = date('d/m/Y H:i:s');




try {
$connector = new WindowsPrintConnector("$ini_printername");
$printer = new Escpos($connector);
$printer->selectPrintMode ( Escpos::MODE_DOUBLE_HEIGHT | Escpos::MODE_DOUBLE_WIDTH );
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
    $printer -> text("FREQUENCY TM \n");
$printer -> feed(2);
$printer->selectPrintMode ();
    $printer -> text("OULED BRAHEM SETIF 19000 \n");
    $printer -> text($now." \n");
$printer -> setJustification(Escpos::JUSTIFY_LEFT);

//$printer -> text(new item('10  CANDIA', '95.00'));
//$printer -> text(new item('3  ISIS', '125.00'));

for ($i = 0; $i < count($PRINTARRAY); $i++) {
  //print $array[$i];
  $printer -> text(new item($PRINTARRAY[$i][0].'  '.$PRINTARRAY[$i][1], ($PRINTARRAY[$i][2])));

}



$printer -> feed();
	$printer -> setEmphasis(false);
  $printer -> text(new item('TOTAL', '215.00'));





	$printer -> feed();
	$printer -> text("MERCI \n");
	$printer -> feed(4);
	$printer -> cut();
    $printer -> close();
} catch(Exception $e) {
    echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
}



























class item {
	private $name;
	private $price;
	private $dollarSign;

	public function __construct($name = '', $price = '', $dollarSign = false) {
		$this -> name = $name;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 10;
		$leftCols = 38;
		if($this -> dollarSign) {
			$leftCols = $leftCols / 2 - $rightCols / 2;
		}
		$left = str_pad($this -> name, $leftCols) ;

		$sign = ($this -> dollarSign ? '$ ' : '');
		$right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
