<?php
$PID = $_GET['pid'] ;
// $defaultPID = $_GET['pid'] ;
$selctedFormat1 = $selctedFormat2 = '';
$defaultFile = $defaultCode = $defaultTitre = $defaultEnsei = $defaultPages = $defaultRota = $defaultFormat = $defaultNote = $defaultQt ='';
 

$fdb = new PDO('sqlite:ajax/core.dll');
if  ($PID!== '0') {

$result_one = $fdb->query("SELECT * FROM liste WHERE id='$PID' LIMIT 1");
foreach($result_one as $row) {
$defaultFile = $row['emplacement'];
$defaultCode = $row['code'];
$defaultQt = $row['qt'];
$defaultTitre = $row['titre'];
$defaultEnsei = $row['ensei'];
$defaultPages = $row['pages'];
$defaultNote = $row['notes'];
$defaultRota = ($row['rota'] == '1'?'checked':'');
$selctedFormat2 = ($row['format'] !== 'word' ? 'selected':'');
}
}


$ensei_list = array();
$result_one = $fdb->query("SELECT DISTINCT ensei FROM liste ");
foreach($result_one as $row) {
$ensei_list[] =  $row['ensei'];
}

$datalist = implode(',',$ensei_list);



 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AJOUTER</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/liste.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="css/awesomplete.css" />

<style media="screen">
  body{padding:10px;overflow-x: hidden}
  .fileUpload {
  position: relative;
  overflow: hidden;
  margin: 10px;
}
.fileUpload input.upload {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  font-size: 20px;
  cursor: pointer;
  opacity: 0;
  filter: alpha(opacity=0);
}
</style>
  </head>

  <body>


<div class="row">
    <input type="hidden" name="pid" value="<?php print $PID;?>">
    <input type="hidden" name="file" value="<?php print $defaultFile;?>">
<div class="col-sm-8">
  <form id="addform">
  <div class="panel panel-default">
    <div class="panel-heading">Ajouter un cours</div>
    <div class="panel-body">
<table class='tblform'>
<tr><td>CODE</td><td><input type="text" name="code" value="<?php print $defaultCode;?>" class="form-control" autofocus></td></tr>
<tr><td>TITRE</td><td><input type="text" name="titre" value="<?php print $defaultTitre;?>" class="form-control" required></td></tr>
<tr><td>ENSEIGNANT</td><td><input  type="text" name="ensei" value="<?php print $defaultEnsei;?>" class="form-control awesomplete" required data-list="<?php print $datalist;?>">
<br><br>



</td></tr>




<tr><td>NOMBRE DES PAGES</td><td><input type="number" min="1" name="pages" value="<?php print $defaultPages;?>" class="form-control half" required></td></tr>


<tr><td>Filiere</td><td>
<select class="form-control" name="filiere">
<option value="m1">Medecine 1</option>
<option value="m2">Medecine 2</option>
<option value="m3">Medecine 3</option>
<option value="m4">Medecine 4</option>
<option value="m5">Medecine 5</option>
<option value="m6">Medecine 6</option>
<option value="p1">Pharmacie 1</option>
<option value="p2">Pharmacie 2</option>
<option value="p3">Pharmacie 3</option>
<option value="p4">Pharmacie 4</option>
<option value="p5">Pharmacie 5</option>
<option value="p6">Pharmacie 6</option>
<option value="c1">Chirurgie dentaire 1</option>
<option value="c2">Chirurgie dentaire 2</option>
<option value="c3">Chirurgie dentaire 3</option>
<option value="c4">Chirurgie dentaire 4</option>
<option value="c5">Chirurgie dentaire 5</option>
<option value="c6">Chirurgie dentaire 6</option>
</select>

</td></tr>


<tr><td>ROTATION ACTUELLE</td><td> <input type="checkbox" value="" <?php print $defaultRota;?> id="ra"> </td></tr>
<tr><td>FORMAT</td><td> <select class="form-control" name="format"><option value="word">WORD</option><option value="ppt" <?php print $selctedFormat2;?>>PPT</option></select></td></tr>
<tr><td>QUANTITÉ</td><td> <input type="number" min="1" name="qt" value="<?php print $defaultQt;?>" class="form-control half" required></td></tr>


</table>
<hr>
<textarea name="notes" rows="3" cols="40" name="notes" class="form-control"><?php print $defaultNote;?></textarea>


    </div>
  </div>
  <div class="formbuttons">
  <button type="submit" name="button" class="btn btn-info"> <span class="glyphicon glyphicon-floppy-saved"></span>  SAUVEGARDER</button>
  <a href="add.php?pid=0" class="btn btn-default">NOUVEAU</a>
  </div>
</form>
</div>



<div class="col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">Fichier</div>
    <div class="panel-body" align="center">
      <div class="fileUpload btn btn-info" >
          <span>Charger le fichier</span>
          <input type="file" class="upload" id="pictureUpload">
      </div>
    </div>
    </div>
</div>



<div class="col-sm-4">
  <div class="panel panel-info">
    <div class="panel-heading">CODEBARRE</div>
    <div class="panel-body" align="center">
      <img src="" bcimg alt=""  style="height:auto;margin-bottom:8px;"/>
<a href="path_to_file" dbtn download="proposed_file_name" class="btn btn-default btn-block">Download</a>
    </div>
    </div>
</div>


</div>




<script src="js/awesomplete.js"></script>

<script src="js/add.js" charset="utf-8"></script>
  </body>
</html>
