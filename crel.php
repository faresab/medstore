<?php
include 'user.php';
include 'ajax/safe.php';
include $db;
$fid = $_GET['fid'];

$infos = Array();
$infos[] = null;
$result = $file_db->query("SELECT * FROM settings ");
foreach($result as $row) {
$infos[] = $row;
}



$soldec = $number = $cv =$cs =0;
$table = $headtable = '<table class="rel"><tbody><tr><th>N°</th><th>DATE</th><th>DESCRIPTION</th><th>SOLDE</th><th>VERSEMENT</th><th>NOUV BALANCE</th><th class="edition"></th></tr>';

$result = $file_db->query("SELECT * FROM clients WHERE ID='$fid'");
foreach($result as $row) {
$FOURNID = $row['ID'];
$NAME = $row['NAME'];
$ADRESS = $row['ADRESS'];
$WILAYA = $row['WILAYA'];

$SOLDE_INIT = $row['SOLDE_INIT'];
}
$cs+= $SOLDE_INIT;
$soldec+= $SOLDE_INIT;
$table.="<tr><td colspan='2'></td><td>CREDIT INITIAL</td>
<td class='text-right'>".nf($SOLDE_INIT)."</td><td></td><td class='text-right'>".colorise($soldec)."</td></tr>";

$result = $file_db->query("SELECT * FROM COP WHERE CLIENTID='$FOURNID' ORDER BY FADATE ASC");
foreach($result as $row) {
$DATE = $row['DATE'];
$DESCRIPTION = $row['DESCRIPTION'];
$SOLDE = $row['SOLDE'];
$VERSEMENT = $row['VERSEMENT'];
$MONT = $row['MONT'];
$number++;
$cv+= $VERSEMENT;
$cs+= $SOLDE;




$soldec+= $MONT;
$table.= "
<tr><td>$number</td><td>".showDate($DATE)."</td><td>$DESCRIPTION</td><td class='text-right'>".nf($SOLDE)."</td><td class='text-right'>".nf($VERSEMENT)."</td><td class='text-right'>".colorise($soldec)."</td>
";

}


$table.="<tr><th colspan='3'></th><th class='text-right'>".nf($cs)."</th><th class='text-right'>".nf($cv)."</th><th class='text-right'>".nf($soldec)."</th></tr></table>";








function colorise($num) {
	if ($num >= 0) {
		$ret = '<b class="ngreen">'.nf($num).'</b>';
	} else {
	$ret = '<b class="nred">'.nf($num).'</b>';
	}
return $ret;
}

function nf ($x){
	return number_format($x, 2, ',', ' ');
}

function showDate($dt){
	$expdat = explode('-', $dt);
	return $expdat[2] . '/'.$expdat[1] . '/'.$expdat[0];
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title></title>
	    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">
		<link href="add.css" rel="stylesheet" media="screen">
    <link href="assets/css/animate.min.css" rel="stylesheet">

<style>

	table.rel{width:100%;}
table.rel tr{margin:0;padding: 0;}
table.rel tr td,table.rel tr th{border:1px solid #969696;padding:3px;}
td.edition , th.edition{display:none;}


</style>

<style type="text/css" media="screen">
.ngreen {color:#D50000;}
.nred {color:#00C853;}
	.col-md-3 .well {min-height: 80px;}
	.newp {width:0;height:0;display: none;}
</style>


<style type="text/css" media="print">
.noprint{display:none!important;}

body {padding:0;margin:0;}
.big1 {font-size: 1.2em}
.noborder {border:none !important; padding:0;}
.newp {page-break-after: always;}
</style>

</head>
<body>


<div class="container-fluid noprint">
<?php include "menu-ui.php";?>
<h0 class="yellow"> relevé des clients >> <span class="name"></span></h0>
<br>
</div>


<div class="container">

<div class="row">
<div class="col-md-3">
<div class="well well-sm">
<b class="big1"><?php print $infos[4]['value'];?><br>
<?php print $infos[5]['value'];?><br>
<?php print $infos[6]['value'];?></b>
</div>
</div>

<div class="col-md-3">
<div class="well well-sm">
<b><span class="name"></span><br>
<span class="adress"></span><br>
<span class="wilaya"></span></b>
</div>
</div>

<div class="col-md-3">
<div class="well well-sm">
<b>DATE:  <span class="pull-right"><?php print date('d/m/Y');?></span><br>
CREDIT (TOTAL) : <span class="pull-right cs"></span><br>
VERSEMENT (TOTAL) : <span class="pull-right cv"></span></b>
</div>
</div>


<div class="col-md-3 noprint">
<div class="well well-sm" align="center">
<a href="javascript:window.print()" class="btn btn-default btn-block"><i class="fa fa-print"></i> Imprimmer</a>
<a href="javascript:loadrel('')" class="btn btn-default btn-block"><i class="fa fa-list"></i> Changer le client</a>
</div>
</div>




</div>



<div class="well well-sm noborder maintbl">
<!--<?php print $table;?>-->
<br><br>



</div>



</div>


</body>
  <script src="assets/js/moment.int.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/modernizr.custom.js"></script>
  <script src="dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
var relid = '<?php print $fid?>';
	function loadrel(xid) {
$('.maintbl').html('<h3 align="center">CHARGEMENT...</h3>').load('op/load_rel_c.php?fid='+xid ,function(){
});
	}

function reload(x) {
	relid = x.value;
	loadrel(x.value);
}


	$(function(){
loadrel(relid);
	});

</script>

</html>
