<?php
error_reporting(0);
include 'user.php';
include 'ajax/safe.php';
include $db;

$ROLS = array('','UTILISATEUR','GERANT','ADMINISTRATEUR');


$listplus="";
 $result_twoo = $file_db->query("SELECT * FROM users ");
foreach($result_twoo as $row) {
$ID = $row['ID'];
$USERNAME = $row['USERNAME'];
$USERPOWER = $row['POWER'];
if ($USERPOWER <= $POWER) $listplus.= "<tr><td>$USERNAME</td><td>$ROLS[$USERPOWER]</td><td align='center'><a href='#' onClick='removeUser($ID)' class='btn btn-sm btn-danger' title='Suprimmer' ><i class='fa fa-trash'></i> </a></td></tr>";
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<link rel="shortcut icon" href="assets/ico/icon.ico" />
    <title>EM14</title>
    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-switch.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet">


    <link href="add.css" rel="stylesheet">

  </head>

  <body>

    <!-- Fixed navbar -->
	      <div class="container-fluid">

<?php include 'menu-ui.php';?>

    </div>
<h0 class="pink">LISTE D'UTILISATEURS</h0>
    <div class="container-fluid">






<br>

<div class="container">
<div class='row'>
<div class="col-md-6">
<div class="well well-sm">
<h0 class="pink div">LISTE DES UTILISATEURS</h0><br>
<table class="table table-bordered rel">
<!--  <tr>
  <th>UTILISATEUR</th>
  <th>ROLE</th>
  <th>ACTION</th>
  </tr>
-->
<?php print $listplus;?>

</table>
</div>
</div>

<div class="col-md-6">
<div class="well well-sm">
<h0 class="pink div">AJOUTER UN UTILISATEUR</h0><br>

<form id="adduser" method="POSt" action="ajax/user-add.php">
<input type="text" name="user" class="form-control" placeholder="UTILISATEUR" required><br>
<input type="text" name="pass" class="form-control" placeholder="MOT DE PASSE" required><br>
<select name="role" class="form-control">
<?php
for ($i=1; $i < $POWER; $i++) {
  print "<option value='$i'>".$ROLS[$i]."</i>";
}
if ($POWER == 3) {
 print "<option value='3'>ADMINISTRATEUR</i>";
}
?>
</select><br>
<button type="submit" class="btn btn-primary btn-block submit"><i class="fa fa-plus"></i> AJOUTER</button>
</form>
</div>

</div>


</div>


</div>




<div id="null"></div>


    <script src="assets/js/jquery.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
      <script src="assets/js/ALL.js"></script>
    <script type="text/javascript">

$(function(){
$('#adduser').on('submit',function(){
$('.submit').attr('disabled','disabled');
$.ajax({
      type: $('#adduser').attr('method'),
      url: $('#adduser').attr('action'),
      data: $('#adduser').serialize(),
      success: function(data) {
alert(data);
window.location.reload();
 $('.submit').removeAttr('disabled');
      }
    });
return false;
});





});


function removeUser(x){

  $('#null').load('ajax/remove-user.php?id='+x ,function(data){

  });
return false;
}

    </script>
  <?php include "plug.php";?>
</body>
</html>
