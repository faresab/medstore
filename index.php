﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>EM 2016</title>
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <link href="assets/css/animate.css" rel="stylesheet">
     <link rel="stylesheet" href="assets/css/master.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>

<div class="logoField">

<div class="frequency" align="center">
  <img src="assets/frequency.png" alt="" style="height:100px;width:auto;" /><br>
<i class="fa fa-spin fa-spinner"></i> <br> Chargement..
</div>



</div>



  </body>
<script type="text/javascript">

window.onerror =function(){
  window.location.reload();
}

goshowEm();
function goshowEm(){
window.setTimeout(function(){
  document.querySelector('.logoField').innerHTML = '<div class="cont">EM 2016</div>';
  document.querySelector('.logoField').classList.add('small');
  var audio = new Audio('audio/the-little-dwarf.ogg');
   audio.volume = 0.5;
  audio.play();
},2000);

window.setTimeout(function(){
  checkManifiest();
},4000);
}

function gologin(){
  var htmlLogIn ='<form onsubmit="checklogin();return false;"><h3 align="center"> <i class="fa fa-user"></i> Connexion</h3>  <select class="form-control log" name="loginuser" >';
  fetchJSONFile('ajax/userslist.php',function(x){
for (var i = 0; i < x.length; i++) {
  htmlLogIn += '<option value="'+x[i].USERNAME+'">'+x[i].USERNAME+'</option>';
}
htmlLogIn += '</select><div class="input-group ac"><input type="password" class="form-control" name="loginpass"><span class="input-group-btn"><button class="btn btn-default" type="button" onclick="togglePassView()"><i class="fa fa-eye eyeclass"></i></button></span></div><button type="submit" name="button" class="btn btn-default btn-block pull-right ac" style="margin-right:10%;width:100px">Connexion</button></form>';
  });


window.setTimeout(function(){
   document.querySelector('.logoField').innerHTML = htmlLogIn;
  document.querySelector('.logoField').classList.remove('activate');
  document.querySelector('[name="loginpass"]').focus();
  document.querySelector('.logoField').classList.add('login');
  document.querySelector('.logoField').classList.innerHTML = '<form><select class="form-control"> <input type="password" class="form-control"></form>';
},2000);
}



function togglePassView(){
  var currentType = document.querySelector('[name="loginpass"]').type ;
  if (currentType == 'password') {
    document.querySelector('.eyeclass').classList.remove('fa-eye');
    document.querySelector('.eyeclass').classList.add('fa-eye-slash');
    currentType = document.querySelector('[name="loginpass"]').type = 'text' ;
  } else {

    document.querySelector('.eyeclass').classList.add('fa-eye');
    document.querySelector('.eyeclass').classList.remove('fa-eye-slash');
    document.querySelector('[name="loginpass"]').type = 'password';
  }
}

var loginTr = 0;
function checklogin(){
var user = document.querySelector('[name="loginuser"]').value ;
var pass = document.querySelector('[name="loginpass"]').value ;

var httpRequest = new XMLHttpRequest();
httpRequest.onload  = function () {
	if (httpRequest.readyState==4 && httpRequest.status==200){
var reponse = httpRequest.responseText;

if (reponse == '0') {
  document.querySelector('.logoField').classList.add('shake');
  document.querySelector('.logoField').classList.add('animated');
  document.querySelector('.logoField').classList.add('error');
  window.setTimeout(function(){
  document.querySelector('.logoField').classList.remove('shake');
  document.querySelector('.logoField').classList.remove('animated');
  document.querySelector('.logoField').classList.remove('error');
document.querySelector('[name="loginpass"]').value = '';
document.querySelector('[name="loginpass"]').focus();

loginTr++;

if (loginTr > 4) window.close();

},1000);

} else {
  var link =  'set.php?set='+ reponse;
  document.querySelector('.logoField').innerHTML = '';
  document.querySelector('.logoField').classList.remove('small');
  document.querySelector('.logoField').classList.remove('login');
  // document.querySelector('.logoField').classList.add('lastAnim');
window.setTimeout(function(){window.location.href = link},600);
}


}
}

httpRequest.open('POST', 'frequency/pass.php');
httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
httpRequest.setRequestHeader( "Pragma", "no-cache" );
httpRequest.setRequestHeader( "Cache-Control", "no-cache" );
httpRequest.setRequestHeader( "Expires", 0 );
httpRequest.send('user='+user+'&pass='+pass);

return false;
}












function goactivate(gn){
  document.querySelector('.logoField').innerHTML ='  <div class="actContent"><div class="text">  Votre Code d\'activation <h2 align=center style="font-weight:bold;">'+gn+'</h2></div><form onsubmit="activate();return false;"><input type="text" class="form-control ac ac-key" value="" required >      <button type="submit" name="button" class="btn btn-default btn-block ac">Activer</button></form><div class="text">  Pour plus d\'information <br> 036.66.98.88 </div></div>';
window.setTimeout(function(){
  document.querySelector('.logoField').classList.add('activate');
  document.querySelector('.ac-key').focus();
},1000);
window.setTimeout(function(){
  document.querySelector('.actContent').style.opacity = '1';
 },2500);
}



// checkManifiest();




function checkManifiest(){
fetchJSONFile('manifest/newsta.php', function(x){
   var ac = x.manifest.active;
 var gen = x.manifest.gn;
console.log(x);
// if (ac == '1') loadJson();
if (ac == '0') goactivate(gen);
if (ac == '1') gologin();
});
}






function activate(){
var dataz = [];
dataz.push('key='+document.querySelector('.ac-key').value);

var httpRequest = new XMLHttpRequest();
httpRequest.onload  = function () {
	if (httpRequest.readyState==4 && httpRequest.status==200){
  console.log( httpRequest.responseText);
 //switchrep (httpRequest.responseText);
var rep = httpRequest.responseText;
if (rep.split(';;')[0] == '0') {
  document.querySelector('.logoField').classList.add('shake');
  document.querySelector('.logoField').classList.add('animated');
  document.querySelector('.logoField').classList.add('error');

  window.setTimeout(function(){
  document.querySelector('.logoField').classList.remove('shake');
  document.querySelector('.logoField').classList.remove('animated');
  document.querySelector('.logoField').classList.remove('error');
document.querySelector('.ac-key').value = '';
document.querySelector('.ac-key').focus();
},1000);
} else{

var days = rep.split(';;')[1];
var activatedString =  (parseInt(days) > 200 ?'':' pour '+days+' jours')
activatedString = '<div class="text40">Em 2016 est activé ' + activatedString +'</div>';
document.querySelector('.logoField').classList.add('success');
document.querySelector('.logoField').innerHTML = activatedString;

window.setTimeout(function(){
document.querySelector('.logoField').classList.remove('shake');
document.querySelector('.logoField').classList.remove('animated');
document.querySelector('.logoField').classList.remove('success');
document.querySelector('.ac-key').value = '';
document.querySelector('.ac-key').focus();
},1500);


gologin();
}

}
}

httpRequest.open('POST', 'manifest/ac.php');
httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
httpRequest.setRequestHeader( "Pragma", "no-cache" );
httpRequest.setRequestHeader( "Cache-Control", "no-cache" );
httpRequest.setRequestHeader( "Expires", 0 );
httpRequest.send(dataz);

}













function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}


</script>
</html>
