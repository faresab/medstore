<?php






error_reporting(0);
include 'user.php';
include 'conf/inc.php';
$audioclass= ($ini['AUDIO'] == '1'?'glyphicon glyphicon-volume-up':'glyphicon glyphicon-volume-off');
$audiost= ($ini['AUDIO'] == '1'?'true':'false');
$touchst= ($ini['TOUCHSCREEN'] == '1'?'true':'false');
$display= ($ini['DISPLAY'] == '1'?'true':'false');



include 'ajax/safe.php';
include $db;
$stock = 'var stock = new Array();';
$xcount = 0;

$result_one = $file_db->query("SELECT * from stock ");
foreach($result_one as $row) {
$stock.= 'stock['.$xcount.'] = ["'.$row['ID'].'", "'.fltr($row['REF']).'", "'.fltr($row['BARCODE']).'","'.fltr($row['PRODUCT']).'","'.fltr($row['PRICE_V']).'","'.$row['QT'].'","'.$row['PRICE_A'].'","'.$row['QTMIN'].'","'.$row['WP'].'","'.$row['PDATE'].'","'.$row['FAM'].'"];';
$xcount++;
}


function fltr($str) {
  return str_replace('"','',$str);
}





$INFOS = array();
$result_one = $file_db->query("SELECT * FROM settings ");
foreach($result_one as $row) {
$INFOS[$row['key']] = $row['value'];
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">

    <title>VENTE AU COMPTOIR</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">

    <link href="add.css" rel="stylesheet" media="screen">
    <link href="comptoir/comptoir.css" rel="stylesheet" media="screen">
<!--    <link href="comptoir/theme.css" rel="stylesheet" media="screen">-->
<style type="text/css" media="screen">
.onlyprint {display:none;}
@media screen {
  .printdiv {display:none;}
}


/*.pannel,.catlist {height:300px;overflow-y: scroll}*/
</style>

  </head>

  <body>

    <!-- Fixed navbar -->
        <div class="container-fluid no-print">
<?php include 'menu-ui.php';?>
    </div>

<div class="comptoirtopnav">
<div class="comptoirtopnavInfos">
<div class="row">
    <div class="col-sm-5">
<span class="Cname"><?php print $INFOS['Cname'] . '  </span>
   -   <span class="Cadress">' . $INFOS['Cadress'] . '</span>';
?>
</div>


 <div class="col-sm-7 ">

<span class="currentProd"></span>
   <span class="currentQtAvaible"></span>

</div>

</div>
</div>




<div class="comptoirtopnavmainbar">
<div class="row">
<div class="col-md-8">


<div class="row">

<div class="col-sm-3">
<input type="text" data-order ="1" class="form-control refcode" id="inputref" placeholder="REF/CODE"/>
</div>

<div class="col-sm-3">
<input type="text" data-order ="2" class="form-control numbersOnly qt" id="inputqt" placeholder="QUANTITÉ" />
</div>

<div class="col-sm-3">
<input type="text" data-order ="3" class="form-control priceInput numbersOnly" id="inputprice" placeholder="PRIX"/>
</div>
</div>

<div>
<input type="text" data-order ="4" class="form-control prodInput" id="inputprod" placeholder="PRODUIT"/>
  <div class="suggestionsBox" id="suggestions" style="display: none;">
 <div class="suggestionList " id="autoSuggestionsList"></div></div>


</div>



</div>

<div class="col-sm-4">
<div class="comptoirPrix" id="comptoirPrix">0.00</div>
</div>


</div>




</div>



</div>



<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
<table class="table vtable table-bordered">
<tr class="active"><th width="8%">N°</th><th width="10%">REF</th><th>DESIGNATION</th><th width="20%">PRIX</th><th width="10%">QT</th><th width="15%">MONT</th></tr>
<tbody>



</tbody>
</table>
</div>
</div>
</div>

   <div class="bottomcontrol animated fadeInUp" >
   <div class="elem"><a href="#"  class="prevButton"><span class="glyphicon glyphicon-backward" tooltip data-placement="top" title="Précédent (CTRL + <-)"></span></a></div>
   <div class="elem"><a href="#" tooltip data-placement="top" title="Pause (CTRL+ESPACE)" class="pauseBtn"><span class="glyphicon glyphicon-pause"></span></a></div>
   <div class="elem" ><a href="#" tooltip data-placement="top" class="nextButton" title="Suivant (CTRL+ ->)"><span class="glyphicon glyphicon-forward"></span></a></div>
   <div class="elem clock">12:00</div>
   <div class="elem">
<a href="#" tooltip class="f8" id="f8" data-placement="top" title="Remise"><span class="glyphicon glyphicon-stats"></span></a></div>
   <div class="elem">
<a href="#" tooltip class="f10" data-placement="top" title="Valider   (F10)"><span class="glyphicon glyphicon-shopping-cart "></span></a></div>
   <div class="elem">
<a href="#" tooltip class="f9" data-placement="top" title="Valider et imprimer (F9)"><span class="glyphicon glyphicon-print"></span></a></div>
  <div class="elem">
<a href="#" tooltip onclick="removerow()" data-placement="top" title="Supprimer (SUPPR)"><span class="glyphicon glyphicon-trash"></span></a></div>
   <div class="elem">
<a tooltip href='#Vers' data-toggle="modal" data-placement="top" title="Versement (CTRL + V)"><span class="glyphicon glyphicon-pencil"></span></a></div>
<div class="elem">
<a href='#' onclick='toggleTactile()'><span class="glyphicon glyphicon-th-list"></span></a></div>
<div class="elem error" style="display:none;">
<a tooltip href='#'  data-placement="top" title="Erreur"><span  class="glyphicon glyphicon-remove"></span></a></div>






<inf>


</inf>

   </div>




<div style="display:none;width:0;height:0">
<form id="sales">
<input type="hidden" name="HID" ID="HID" value="0" />
<input type="hidden" name="remise"  value="0" />

</form>

</div>












<div class="modal" id="Vers">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">VERSEMENT</h4>
      </div>
      <div class="modal-body">
      <form id="FormVers">
 <table width="100%">
   <tr width="100px"><td>VERSEMENT</td><td><input data-v="0" type="text" class="form-control number" name="versvalue" /></td></tr>
    <tr><td>REST</td><td><input data-v="1" type="text" class="form-control number"  name="restvalue" /></td></tr>
    <tr><td>Nom de client</td><td><input data-v="2" type="text" class="form-control "  name="ClientName" /></td></tr>
  <tr><td>Commentaire </td><td><input data-v="3" type="text" class="form-control "  name="Comment" /></td></tr>
 </table>
<versform></versform>
</form>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal">Annuler</a>
        <a  class="btn btn-success versAdd">Ajouter un Versement</a>
      </div>
    </div>
  </div>
</div>





<div class="modal " id="dialog" aria-labelledby="largeModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TOTAL</h4>
      </div>
      <div class="modal-body">
<div class="row">
<div class="col-xs-6">
<div align="center">A PAYER</div>

    <div class="comptoirPrix small topay" style="">0.00</div>
</div>

<div class="col-xs-6">
<div align="center">MONNAIE</div>
    <div class="comptoirmoney small error change "></div>
</div>

</div>
<hr/>
<table width="100%">
<tr><td width="33%">REMISE</td><td width="33%">MONTANT</td><td width="33%">RECU</td></tr>
<tr><td><input data-order="6" class="form-control number numbersOnly" name="iremise"></td>
<td><input data-order="7" class="form-control number numbersOnly" name="topay"></td>
<td><input data-order="8" class="form-control number numbersOnly" name="given"></td></tr>
</table>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
        <a  class="btn btn-success f9"><span class="glyphicon glyphicon-print"></span></a>
        <a  class="btn btn-success f10"><span class="glyphicon glyphicon-floppy-disk"></span></a>
      </div>
    </div>
  </div>
</div>









<div class="modal" id="subtext">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">TEXT</h4>
      </div>
      <div class="modal-body">
      <form id="Sub">
<input rows="5" class="form-control" name="subdata" />
<br><a href="#" class="btn btn-default subApply">MODIFIER</a>
</form>
      </div>

    </div>
  </div>
</div>

<script>
 <?php  print $stock;?>



</script>



<script  src="assets/js/jquery.js" type="text/javascript" ></script>
<script  src="assets/js/sugg.js" type="text/javascript" ></script>
<script  src="assets/js/moment.int.js" type="text/javascript" ></script>
<script  src="assets/js/mousetrap.js" type="text/javascript" ></script>

<script  src="assets/js/comptoir-vanilla.js" type="text/javascript" ></script>
<!--<script  src="comptoir/inputs.js" type="text/javascript" ></script>-->
<script  src="comptoir/versement.js" type="text/javascript" ></script>
<script  src="dist/js/bootstrap.min.js" type="text/javascript" ></script>

<script  src="api/api.js" type="text/javascript" ></script>


<script>
var audiostat = <?php print $audiost;?>;
var lcd_display = <?php print $display;?>;

$(function(){
if (navigator.onLine) {$('.audio-elem').show();$('#audiop').attr('class','<?php print $audioclass;?>');}

if (<?php print $touchst;?>) {
  enableTactile();
}


});

</script>


</body>
</html>
