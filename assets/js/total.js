var loadicon = '<span style="position:relative;top:100px;left:46%;"><i class="fa fa-spin fa-spinner" ></i>  chargement..</span>';
   $.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/ /g , ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration == undefined ? 1000 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00');
                                }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00' );
}
}
}
            });
        });
    };



var stocklist , hids , ventes;
 $(document).ready(function() {
moment.lang('fr');
                  $('#daterange').daterangepicker();
               });





           $(document).ready(function() {
                  $('#reportrange').daterangepicker(
                     {
                        startDate: moment(),
                        endDate: moment(),
                        minDate: '01/01/2012',
                        maxDate: '12/31/2026',
                        dateLimit: { days: 1360 },
                        showDropdowns: true,
                        showWeekNumbers: true,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: true,
                        ranges: {
                           'aujourd\'hui': [moment(), moment()],
                           'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],
                           'Les 7 derniers jours': [moment().subtract('days', 6), moment()],
                           '30 derniers jours': [moment().subtract('days', 29), moment()],
                           'ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                           'Le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                        },
                        opens: 'right',
                        buttonClasses: ['btn btn-default'],
                        applyClass: 'btn-small btn-info',
                        cancelClass: 'btn-small',
                        format: 'MM/DD/YYYY',
                        separator: ' A ',
                        locale: {
                            applyLabel: 'OK',
                            fromLabel: 'de',
                            toLabel: 'A',
                            customRangeLabel: ' personnaliser',
                            daysOfWeek: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                            monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                            firstDay: 1
                        }
                     },
                     function(start, end) {
                      console.log("Callback has been called!");
                      $('#reportrange').val(start.format(' dddd D MMMM YYYY') + ' jusq\'a ' + end.format('dddd D MMMM YYYY'));
                      $('#reportrange').attr('data-start',start.format('YYYYMMDD'));
                      $('#reportrange').attr('data-end',end.format('YYYYMMDD'));
                     $(xdate);
                     }
                  );
                  //Set the initial state of the picker label
                  //$('#reportrange').val(moment().subtract('days', 29).format('D MMMM YYYY') + ' - ' + moment().format('D MMMM YYYY'));
                  $('#reportrange').val(moment().format('dddd D MMMM YYYY'));
               $(xdate);
               });



var xdate = function(){
var startdate = $('#reportrange').attr('data-start');
var endtdate = $('#reportrange').attr('data-end');
$('.jumbotronmid .row').html(loadicon).load('ajax/totaltable.php?start='+startdate+'&end='+endtdate , function(){
$('a[href="#det"]').on('click',function(){
// $('.details').html('chargement..').load('ajax/details.php?start='+start+'&end='+end);
fetchJSONFile('ajax/detailsjson.php?start='+start+'&end='+end , function(x){
  stocklist = x.stock;
  ventes = x.ventes;
  hids = x.hids;
creatTable();
});
});

});

return false;

}

$(function() {
$.get("ajax/infos.php",function(data){
var infos = data.split(',');
$('title').text(infos[0]);
});
});




function tdColor(x,y) {
  var res = 'green';
  if ((parseInt(x) * 0.9) <=  parseInt(y) && parseInt(x) >parseInt(y) ) res = 'orange';
  if ((parseInt(x) * 0.9) >=  parseInt(y)) res = 'red';
return res;
}

function countRem(a , b) {
return  parseFloat(a) - parseFloat(b) ;
}

function countPourcent(x,y) {
  var pour;
  if (y > 0) {
pour = Math.round(x * 100 / y);
  } else {
    pour = 0 ;
  }
  return pour;
}


var sansRem ,  remTotal , hidPrices ;
function creatTable(){
sansRem   = hidPrices = remTotal = 0 ;
var prevHid  = 0;

var table = '<table class="table table-bordered"><tr></tr>';
for (var i = 0; i < ventes.length; i++) {
  if (prevHid !== ventes[i].HID && prevHid !== 0) {
    table += runHid(prevHid , hidPrices);
    hidPrices = 0 ;
  }

  var sid =  ventes[i].SID  ;


//var prv =  parseFloat(stocklist[sid].PRICE_V) * parseFloat(ventes[i].QT);
var prv = (stocklist[sid] !== undefined?parseFloat(stocklist[sid].PRICE_V) * parseFloat(ventes[i].QT) : parseFloat(ventes[i].PRICE) ) ;

 if (sid === null)  continue;
//   table += '<tr><td>'+ventes[i].DATETIME +'</td><td>'+ventes[i].PRODUCT +'</td><td>'+ventes[i].QT +'</td><td class="text-right ">'+fm(ventes[i].PRICE) +'</td><td class="text-right">'+ fm(prv)  +'</td><td class="text-right '+tdColor(prv , ventes[i].PRICE  )+'">'+ fm(remise)  +'</td><td>'+ ventes[i].user  +'</td></tr>';


prevHid = ventes[i].HID;
var ISVC = (hids[prevHid].ACTION == 'VC');

if (ISVC) {
var remise = countRem(prv,ventes[i].PRICE);
remTotal += remise;
console.log(remise);
sansRem += prv ;
hidPrices += prv ;
   table += '<tr><td>'+ventes[i].DATETIME +'</td><td>'+ventes[i].PRODUCT +'</td><td>'+ventes[i].QT +'</td><td class="text-right ">'+fm(ventes[i].PRICE) +'</td><td class="text-right">'+ fm(prv)  +'</td><td class="text-right '+tdColor(prv , ventes[i].PRICE  )+'">'+ fm(remise)  +'</td><td>'+ ventes[i].user  +'</td></tr>';
}
}
if (ISVC)  table += runHid(prevHid , hidPrices);
table += '</table>';
table  = '<table class="table table-bordered"><tr><td>VALEUR (SR)</td><td><h3> '+fm(sansRem)+'</h3></td></tr><tr><td>REMISE TOTALE</td><td><h3>'+fm(remTotal)+' ('+countPourcent (remTotal,sansRem)+' %)</h3></td></tr></table>' + table;

document.querySelector('.details').innerHTML = table ;
};


function runHid(x,y){
  var value = hids[x].VALUE;
return '<tr class="'+tdColor(y,value)+'"><td colspan="3"></td><td class="text-right"><strong>'+fm(value)+'</strong></td><td class="text-right"><strong>'+fm(y)+'</strong></td><td class="text-right"><b>'+fm(y - parseInt(value))+'</b></td><td></td></tr>';
}



function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

function fetchJSONFile(path, callback) {
   var httpRequest = new XMLHttpRequest();
   httpRequest.onreadystatechange = function() {
       if (httpRequest.readyState === 4) {
           if (httpRequest.status === 200) {
             spydata = JSON.stringify(httpRequest.responseText);
               var data = JSON.parse(httpRequest.responseText);
               if (callback) callback(data);
           }
       }
   };
   httpRequest.open('GET', path);
   httpRequest.send();
}
