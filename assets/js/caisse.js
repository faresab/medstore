var loadicon = '<span style="position:relative;top:100px;left:46%;"><i class="fa fa-spin fa-spinner" ></i>  chargement..</span>';
   $.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/ /g , ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration == undefined ? 1000 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00');                          
                                }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00' );              
}
}
}
            });
        });
    };




 $(document).ready(function() {
moment.lang('fr');
                  $('#daterange').daterangepicker();
               });





           $(document).ready(function() {
                  $('#reportrange').daterangepicker(
                     {
                        startDate: moment(),
                        endDate: moment(),
                        minDate: '01/01/2012',
                        maxDate: '12/31/2026',
                        dateLimit: { days: 360 },
                        showDropdowns: true,
                        showWeekNumbers: true,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: true,
                        ranges: {
                           'aujourd\'hui': [moment(), moment()],
                           'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],
                           'Les 7 derniers jours': [moment().subtract('days', 6), moment()],
                           '30 derniers jours': [moment().subtract('days', 29), moment()],
                           'ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                           'Le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                        },
                        opens: 'right',
                        buttonClasses: ['btn btn-default'],
                        applyClass: 'btn-small btn-info',
                        cancelClass: 'btn-small',
                        format: 'MM/DD/YYYY',
                        separator: ' A ',
                        locale: {
                            applyLabel: 'OK',
                            fromLabel: 'de',
                            toLabel: 'A',
                            customRangeLabel: ' personnaliser',
                            daysOfWeek: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                            monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                            firstDay: 1
                        }
                     },
                     function(start, end) {
                      console.log("Callback has been called!");
                      $('#reportrange').val(start.format(' dddd D MMMM YYYY') + ' jusq\'a ' + end.format('dddd D MMMM YYYY'));
                      $('#reportrange').attr('data-start',start.format('YYYYMMDD'));
                      $('#reportrange').attr('data-end',end.format('YYYYMMDD'));
                     $(xdate);
                     }
                  );
                  //Set the initial state of the picker label
                  //$('#reportrange').val(moment().subtract('days', 29).format('D MMMM YYYY') + ' - ' + moment().format('D MMMM YYYY'));
                  $('#reportrange').val(moment().format('dddd D MMMM YYYY'));
               $(xdate);
               });



var xdate = function(){
var startdate = $('#reportrange').attr('data-start');
var endtdate = $('#reportrange').attr('data-end');
$('.jumbotronmid .row').html(loadicon).load('ajax/totaltable.php?start='+startdate+'&end='+endtdate , function(){
$('a[href="#det"]').on('click',function(){
$('.details').html('chargement..').load('ajax/detailscaisse.php?start='+start+'&end='+end);
});

});

return false;

}

$(function() {
$.get("ajax/infos.php",function(data){
var infos = data.split(',');
$('title').text(infos[0]);
});
});