//variables
var total = pid = rest = SumQt = minfos = 0;
var print_table = tabletoptint = '';
var product  = lastIns = ref = currentId = currentPrice = expired = null;

$(function(){
$('.number').number( false, 2,'.',' ' );
$.ajaxSetup ({
    cache: false
});
});


function setcursor() {
     $('.barcodefield').focus();   
    }

function plusOn(pom) {
    if(lastIns !== null && (checkifnull(currentId))){
 $("#null").load("ajax/bc-last.php?action="+encodeURIComponent(pom)+"&id="+encodeURIComponent(lastIns)  , function () {
$('#suggestions:visible').slideUp('fast');
}); 
   }
}


function checkifnull(idnstock){
    var c= parseInt($('tr[rel="'+idnstock+'"] td').eq(4).text());
    if (c === 0){
$('tr[rel="'+idnstock+'"]').remove();
lastIns =null;
return false;
    } else{
        return true;
    }
}

/*Mousetrap.bind('+', function() { plusOn('+1');return false; });*/
Mousetrap.bind('f3', function() { refrech();});
Mousetrap.bind('enter', function() { $('.barcodefield').focus(); });
Mousetrap.bind('esc', function() { $('#suggestions').slideUp('fast'); });

Mousetrap.bind('right', function() { 
var order = parseInt($('input:focus').attr('data-order'));
if (order >=1 && order < 4) order++;
$('input[data-order="'+order+'"]').focus();
    return false;
 });

Mousetrap.bind('left', function() { 
var order = parseInt($('input:focus').attr('data-order'));
if (order > 1 && order <=4) order--;
$('input[data-order="'+order+'"]').focus();
    return false;
 });


Mousetrap.bind('alt', function() {
  $('.modal').modal('show');
    return false;
});







function activemag(){
    $('.barcodefield').select();
    alert('Activer la touche majuscule');
}


function add_row(table,apid,id_instock,aref,aproduct,aprice,arqt,amont){
if ($('tr[rel="'+id_instock+'"]').length){
var existingQt = parseInt($('tr[rel="'+id_instock+'"] td').eq(4).text());
var existingMnt = parseInt($('tr[rel="'+id_instock+'"] td').eq(5).text().replace(' ','').replace(',','.'));
existingMnt+= amont;
existingQt+= arqt;
$('tr[rel="'+id_instock+'"] td').eq(4).text(existingQt);
$('tr[rel="'+id_instock+'"] td').eq(5).children('b').text(existingMnt.formatMoney(2,' ',','));

$('tr').removeClass("success");
$('tr[rel="'+id_instock+'"]').addClass('success');
//var printqt = 
window.setTimeout(function(){ $('tr[rel="'+id_instock+'"]').removeClass("success");}, 600);
var oldqt = $('td[data-id="'+id_instock+'"]').text();
var newqt = parseInt(oldqt) + arqt ;
$('td[data-id="'+id_instock+'"]').text(newqt);
print_table = $('.printdiv').html();
if (($('tr[rel="'+id_instock+'"] td').eq(4).text()) == '0') {
    $('tr[rel="'+id_instock+'"]').remove();
}
} else {
 if(arqt >= 1) {
content_table = "<tr rel='"+id_instock+"'><td>"+apid +"</td><td>"+aref+"</td><td>" + aproduct + "</td><td>"+aprice+"</td><td>"+arqt+"</td><td><b  class='pull-right'>"+amont.formatMoney(2,' ',',')+"</b></td></tr>";
var Tbody = $(table).children('tbody').eq(1);
Tbody.prepend(content_table);
$('tr').removeClass("success");
Tbody.children('tr:first').addClass('success');
window.setTimeout(function(){ Tbody.children('tr:first').removeClass("success");}, 600);

// print_table += "<tr><td class='tdqt' data-id='"+id_instock+"'>"+arqt+"</td><td class='tdprod'>"+aproduct+"</td><td class='tdprice'>"+aprice.formatMoney(2,' ',',')+"</td></tr>";
//$('.printdiv').html(print_table);
tabletoptint+= "<tr><td>"+aproduct+"</td><td>"+arqt+"</td><td class='third'>"+aprice.formatMoney(2,' ',',')+"</td></tr>";
} 
}
currentId = id_instock;
currentPrice = amont;
}




function refrech(){
$('.vtable tr').not('.active').remove();
$('.barcodefield ,.productfield ,.pricefield').val('');
$('.pricefield').val('');
$('.qtfield').val('');
$('#modaltot').text( "0.00" );
$('.barcodefield').focus();
$('#suggestions').slideUp('fast');
$('.price').text('0.00');
 total = pid = rest = SumQt = minfos = 0;
 print_table = tabletoptint = '';
 product  = lastIns = ref = currentId = currentPrice = expired = null;
}







$(function(){
$('.barcodefield').focus();

$('.tgl').on('click',function(){
$(this).children('i').toggleClass('fa-chevron-circle-down fa-chevron-circle-up');
$('#extra').slideToggle();
$('.barcodefield').focus();
return false;
});

});





$(function(){
$('.barcodefield').on('keypress',function(e){
    if (e.which == 13) {
var qt = $('.qtfield').val(); 
var price = $('.pricefield').val(); 
var bc = $(this).val();
if (bc == 'ttt') window.location.replace("total.php");
$('#null').load('ajax/bcsubmit.php?qt='+qt+'&bc='+bc+'&price='+price , function(){
});
return false;
}
});



var doublespace = 0;
$("body").bind('keydown', function(e){
    if(  e.keyCode==32 ){
	doublespace++;
	if (doublespace >= 2){
	refrech();
	doublespace = 0;
	return false;
	}
	setTimeout(function(){doublespace = 0;}, 300);
    }
});



/*
$('body').dblc("shift",function(){
  $('.modal').modal('show');
},300);
*/
});





$(function(){
$('.productfield').sugg({
requested_page: 'ajax/product-sugg.php',
requested_key: 'q',
});

$('.productfield').arrawUD(null,
function(sel){
    var stex = sel.text();
    var sid = sel.attr('data-id');
    filltoproduct(null,sid );
}
);




});


//fill product
function filltoproduct (valuex,Id) {
$('.productfield').val(valuex);
var sqt = $('.qtfield').val();
var sprice = $('.pricefield').val();
$("#null").load("ajax/bcreq.php?id="+encodeURIComponent(Id)+"&price="+encodeURIComponent(sprice)+"&qtt="+encodeURIComponent(sqt) , function (data) {
$('#suggestions').slideUp('fast');
});
}



Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator) {
    var n = this,
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSeparator = decSeparator === undefined ? "." : decSeparator,
    thouSeparator = thouSeparator === undefined ? "," : thouSeparator,
    sign = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};




function newreset() {
    var will_exp = expired.split(':');
$('.barcodefield ,.productfield ,.pricefield').val('');
$('.qtfield').val('');
$('.price , #modaltot').text(total.formatMoney(2,' ',','));
$('span.rst').html(product+ ' ('+ref+') Reste '+rest);
if (will_exp[0] == '1' && will_exp[1] != '') {
    $('span.rst').append(', expiré ' + moment(will_exp[1], "YYYY-MM-DD").fromNow() );
}
return false;
}




$(function(){
	moment.lang('fr');
$('.datetime').text(moment().format('dddd D MMMM YYYY').toUpperCase());
$('.time').text(moment().format(' HH:mm'));
setInterval(function(){
$('.datetime').text(moment().format('dddd D MMMM YYYY').toUpperCase());
$('.time').text(moment().format(' HH:mm'));

}, 30000);
});







function playbeep(){
music("beep",100);
}



     function playerror() {
    music("error",100);

$('.price').addClass('error');
  $('*').addClass('redClass');
window.setTimeout(function(){
  $('*').removeClass('redClass');
$('.price').removeClass('error');
}, 1000);
   
 }

 // getting infos
$(function() {
$.get("ajax/infos.php",function(data){
var infos = data.split(',');
$('.onlyprint h4').text(infos[0]);
$('title').prepend(infos[0] + ' - ');
$('.onlyprint .addr').text(infos[1]+ '  TEL:'+infos[2]);
$('.mn').text(infos[0] + ' '+infos[1]);
minfos = infos[0];
    });
});




    $.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/ /g , ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration === undefined ? 1000 : duration,
                easing: ease === undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00');                          
                                }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text(parseFloat($this.text()).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ") );              
}
}
}
            });
        });
    };

/*

   $.fn.animateNumber = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var sepa = (parseFloat($this.text()) * 100) - (parseInt($this.text()) * 100) ;
            if (sepa <= 9) sepa = '0'+sepa; 
            var start = parseInt($this.text().replace(/ /g , ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration == undefined ? 1000 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00' );                          
                                }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text(parseFloat($this.text()).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ") );              
}
}
}
            });
        });
    };
*/