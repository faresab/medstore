var pid = product = ref = null;
var total = sommereste = sommeregle = 0;
var regle = false;
var solde_init = solde = prix_a = 0;

$(function(){
  $('.clients').selectpicker();
  $.ajaxSetup ({
    cache: false
  });
  });


function clearForms(){
  pid = product = ref = prix = null;
  // $('.qt').val('1');
  // $('.product,.prix').val(null);
  document.querySelector('.qt').value = '';
  document.querySelector('.product').value = '';
  document.querySelector('.prix').value = '';
  $(".suggestionsBox").slideUp("fast");
}


// ad to table




function addToForm(x){
var row = x.split(';');
pid  = row[0];
ref  = row[1];
product  = row[2];
prix  = row[3];
prix_a  = row[4];
var selectCl = document.querySelector('.clients');
var clt = selectCl.options[selectCl.selectedIndex].getAttribute("data-type");

if (clt == '2' && row[5] != '') prix  = row[5];
if (clt == '3' && row[6] != '') prix  = row[6];
console.log(clt);
$(".suggestionsBox").slideUp("fast");
// $('.product').val(product);
// $('.prix').val(prix);
// $('.qt').val(1).focus();
document.querySelector('.product').value = product;
document.querySelector('.prix').value = prix;
document.querySelector('.qt').value = '';
document.querySelector('.qt').focus();
}


function addToTable(){
if (pid != null) {
// if ($('tr[data-id="'+pid+'"]').length >= 1) removeItem(pid);
if (document.querySelectorAll('tr[data-id="'+pid+'"]').length > 0) removeItem(pid);

var qtt = parseFloat(document.querySelector('.qt').value);
qtt = (isNaN(qtt) ?'1':qtt);
// var pr = parseFloat($('.prix').val());
var pr = parseFloat(mfd(document.querySelector('.prix').value));
var mnt = qtt * pr;
var htmltr = "<td >"+pid+"</td><td>"+ref+"</td><td>"+product+"</td><td class='text-right'>"+money(pr)+"</td><td class='text-center'>"+qtt+"</td><td ref='mnt' class='text-right'>"+money(mnt)+"</td>";
var input = "<input type='hidden' rel='"+pid+"' name='prod_"+pid+"' value='"+pr+";"+qtt+";"+product+";"+ref+";"+prix_a+"' />";
// $('tr[data-id="0"]').first().html(htmltr).attr('data-id',pid);
document.querySelectorAll('tr[data-id="0"]')[0].innerHTML = htmltr ;
document.querySelectorAll('tr[data-id="0"]')[0].setAttribute('data-id',pid);

// $('#pinputs').append(input);
document.getElementById('pinputs').innerHTML += input;
clearForms();
countTotal();
updateReste();
setTimeout(function(){ document.querySelector('.product').focus(); }, 300);

}
}



function autoAddToTable(autopid,autoref,autoproduct,autoqt,autopr,automnt,autopa){
var htmltbl = "<td>"+autopid+"</td><td>"+autoref+"</td><td>"+autoproduct+"</td><td class='text-right'>"+money(autopr)+"</td><td class='text-center'>"+autoqt+"</td><td ref='mnt' class='text-right'>"+money(automnt)+"</td>";
var input = "<input type='hidden' rel='"+autopid+"' name='prod_"+autopid+"' value='"+autopr+";"+autoqt+";"+autoproduct+";"+autoref+";"+autopa+"' />";
// $('tr[data-id="0"]').first().html(htmltbl).attr('data-id',autopid);
document.querySelectorAll('tr[data-id="0"]')[0].innerHTML = htmltbl ;
document.querySelectorAll('tr[data-id="0"]')[0].setAttribute('data-id',autopid);
// $('#pinputs').append(input);
document.getElementById('pinputs').innerHTML += input;
countTotal();
updateReste();
}


//
// $(function(){
// $('.qt').on('keypress',function(e){
//     if (e.which == 13) {
// addToTable();
// return false;
//     }
//   });
//   });

document.querySelector('.qt').addEventListener('keypress',function(e){
 if (e.which == 13) {
addToTable();
return false;
 }
});




function removeItem(Ipid) {
  if (parseInt(Ipid) != 0) {
$('tr[data-id="'+Ipid+'"]').remove();
$('input[rel="'+Ipid+'"]').remove();
countTotal();
}
var xreg = parseFloat($('input[name="regle"]').val());
if (xreg > total) $('input[name="regle"]').val(total);
updateReste();
setTimeout(function(){ $('.product').focus(); }, 300);
}

$(function(){
$('tr[data-id]').on('dblclick',function(){
  var this_id = $(this).attr('data-id');
  removeItem(this_id);
});
});


function countTotal(){
	var ctot = 0;
  var tdref = document.querySelectorAll('td[ref="mnt"]');
if (tdref && tdref.length) {
  for (var i = 0; i < tdref.length; i++) {
    var thismont = parseFloat(mfd(tdref[i].innerText));
    ctot = ctot + thismont;
  }
}

// $('td[ref="mnt"]').each(function(){
// var thismont = parseFloat($(this).text().replace(' ','').replace(' ',''));
// });
total = ctot;
// $('.total').text(money(total));
document.querySelector('.total').innerText =  fm(total) ;
// $('input[name="total"]').val(total);
document.querySelector('input[name="total"]').value = total;
}




function UpdateSolde(){
solde_init = parseFloat($('.clients option:selected').attr('data-solde'));
sommereste = parseFloat(mfd(document.querySelector('.reste').value));
solde = solde_init + sommereste;
// $('.solde').text(money(solde_init));
document.querySelector('.solde').innerText =  fm(solde_init);
// $('.soldeMaj').text(money(solde));
document.querySelector('.soldeMaj').innerText =  money(solde);
}

function updateReste(){
sommereste = total - parseFloat(mfd(document.querySelector('.regle').value));
// $('.reste').val(sommereste);
document.querySelector('.reste').value = fm(sommereste);
UpdateSolde();
}

function updateRegle(){
sommeregle = total - parseFloat(mfd(document.querySelector('.reste').value));
if (sommeregle <= 0) {
    document.getElementById('toutreg').checked = true;

}
document.querySelector('.regle').value =  fm(sommeregle) ;
UpdateSolde();
}

function setPid(number){
pid = number;
// $('input.pid').val(number);
document.querySelector('input.pid').value = number ;

$('.printbtn').attr('href','printfv.php?fid='+pid).attr('target','blank');
}


$(function(){
$('.regle').on('input', function(){
	updateReste();
});
$('.reste').on('input',function(){
	updateRegle();
});
});


$(function(){
UpdateSolde();
$('.clients').on('change',function(){
UpdateSolde();
});


$('#toutreg').click(function() {
    var $this = $(this);
    if ($this.is(':checked')) {
    	regle = true;
$('.regle').val(total);
$('.reste').val('0');
 } else {
 	regle = false;
$('.regle').val('0');
$('.reste').val(total);
    }
    UpdateSolde();
});
});




$(function(){

$('.number').number( true, 2,'.',' ' );
$('.product').sugg({
requested_page: 'fastsearch/fv-sugg.php',
requested_key: 'q',
});

$('.product').arrawUD(
function(sel){
    var stex = sel.text();
    var sid = sel.attr('data-id');
   addToForm(sid );
}
);
});

Mousetrap.bind('f6', function() {
  document.querySelector('.clients').focus();

 });

Mousetrap.bind('f1', function() {
$('.regle').val(total).focus().select();
updateReste();
 });
Mousetrap.bind('f2', function() {
$('.regle').val('0');
$('.reste').focus().select();
updateReste();
 });
Mousetrap.bind('f3', function() { save(); });
Mousetrap.bind('f4', function() {
var link = $('.printbtn').attr('href');
var linktarget = $('.printbtn').attr('target');
window.open(link,linktarget);
 });

Mousetrap.bind('right', function() { keyboard('next') });
Mousetrap.bind('left', function() { keyboard('prev') });

function keyboard(kp){
  var $this_inp = $('input:focus');
if (kp == 'next'){
$(':input:eq(' + ($(':input').index($this_inp) + 1) + ')').focus();
} else {
$(':input:eq(' + ($(':input').index($this_inp) - 1) + ')').focus();
}
return false;
}

function save() {

   var clienName = document.querySelector('.clients').value;
if (clienName == undefined) {
  alert('selectionner un client');
} else {
   document.querySelector('.save').setAttribute('disabled','disabled');

var formAdd = document.getElementById('addform');
var dataToSend =serialize(formAdd);
var httpRequest = new XMLHttpRequest();
httpRequest.onload  = function () {
	if (httpRequest.readyState==4 && httpRequest.status==200){
    var res = httpRequest.responseText.split(';;');
 console.log( res);
setPid(res[0]);
   document.querySelector('.save').removeAttribute('disabled');
   alert2('',res[1]);
   music("gets-in-the-way.ogg",100);

}
}
httpRequest.open('POST', 'ajax/fv-submit.php');
httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
httpRequest.setRequestHeader( "Pragma", "no-cache" );
httpRequest.setRequestHeader( "Cache-Control", "no-cache" );
httpRequest.setRequestHeader( "Expires", 0 );
httpRequest.send(dataToSend);
}
    return false;
}



function alert2(title,msg){
// document.querySelector('h4.modal-title').innerHTML = title;
document.querySelector('.modal-body').innerHTML = msg;
$('.dialog').modal();
}



document.querySelector('#addform').onsubmit = function(){
  save();return false;
};











/*
.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")

*/

function money(x) {
    return parseFloat(x).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
}

function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

function mfd(sss) {
  return parseFloat(sss.replace(',','.').replace(' ','').replace(' ','').replace(' ',''));
}





$(function(){
var windowHeight = $(window).height();
$('#res').css('height',(windowHeight - 250));
});



function serialize(form) {
    var field, s = [];
    if (typeof form == 'object' && form.nodeName == "FORM") {
        var len = form.elements.length;
        for (i=0; i<len; i++) {
            field = form.elements[i];
            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
              if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
if (field.hasAttribute('mfd')) {
  s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(mfd(field.value));

} else {
  s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
}
                }
            }
        }
    }
    return s.join('&').replace(/%20/g, '+');
}
