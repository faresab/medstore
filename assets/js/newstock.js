var orderBy = 'ID';
var asc = 'DESC';


function updateInfos(obj) {

document.getElementById('allpa').innerText = fm(obj.totalPa);
document.getElementById('allpv').innerText = fm(obj.totalPv);
document.getElementById('allqt').innerText =  obj.totalQt ;
var zak =  fm(obj.totalPa * 0.025) ,
 		zak2 =  fm(obj.totalPv * 0.025) ;
document.getElementById('zakat').innerHTML =  zak + '<br>'+zak2;
}


function filter(){
filterJson();
}

function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

function mfd(sss) {
  return parseFloat(sss.replace(',','.').replace(' ',''));
}


function newWind(p){
  window.open('pstock_add.php?pid='+p, '', 'width=800px,height=800px');
  }


function writeMont(pa,q) {
  var xt = mfd(pa) * mfd(q) ;
  return fm(xt);
}

function filterJson(){
	var INFOS = {totalPa : 0 , totalPv : 0 , totalQt : 0 }
var seachKey = encodeURIComponent(document.querySelector('.search').value);
var fam = encodeURIComponent(document.querySelector('.fam').value);
var loc = encodeURIComponent(document.querySelector('.loc').value);
var link = 'ajax/stock_filter_json.php?fam='+ fam +'&loc='+ loc +'&key='+ seachKey +'&order='+encodeURIComponent(orderBy)+'&asc='+encodeURIComponent(asc)+'&show='+showOnly;
fetchJSONFile(link,function(x){
var datatable = "";
var blanks = '<tr data-pid="0" ondblclick="newWind(0)"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
if (x && x.length) {
for (var i = 0; i < x.length; i++) {
	var peromp = peromption(x[i].PDATE ,x[i].AVDATE  );
	var vprice = checkPrice(x[i].PRICE_V ,x[i].PRICE_A  );
var checkmin = checkMinimum(x[i].QT , x[i].QTMIN);

datatable += '<tr onclick="selectTr(this)" ondblclick="newWind('+x[i].ID+')" data-pid='+x[i].ID+'><td center>'+peromp+'</td><td>'+x[i].REF+'</td><td>'+ x[i].PRODUCT +'</td><td>'+ x[i].FAM +'</td><td  class=text-center '+checkmin+' >'+ x[i].QT+'</td><td class="so  text-right"   >'+ fm(x[i].PRICE_A) +'</td><td class=" text-right" '+vprice+'>'+ fm(x[i].PRICE_V) +'</td><td class="blnk text-right">'+writeMont(x[i].PRICE_A ,x[i].QT )+'</td>' ;
INFOS.totalPa += parseFloat(x[i].PRICE_A) * parseFloat(x[i].QT);
INFOS.totalPv += parseFloat(x[i].PRICE_V) * parseFloat(x[i].QT);
INFOS.totalQt += parseFloat(x[i].QT);

}
}

for (var i = 0; i < 50 ; i++) {
  datatable += blanks;
}
document.getElementById('stocklist').innerHTML = datatable ;
updateInfos(INFOS);
});
}



function selectTr(sel) {
 	var trs = document.querySelectorAll('[data-pid]');
	if(trs && trs.length) {
for (var i = 0; i < trs.length; i++) {
	trs[i].classList.remove('curr');
}
	}
	sel.classList.add('curr');
}



function peromption(pdate,avdate) {
var res = '<i class="fa fa-check"></i>';
if (pdate.length > 0) {
	if (moment().isAfter(avdate)) res = '<i class="fa fa-warning warning"></i>';
if (moment().isAfter(pdate)) res = '<i class="fa fa-times danger"></i>';
}
return res;
}

function checkMinimum(a,b) {
	return (parseFloat(a) <= parseFloat(b)?'min':'');
}
function checkPrice(a,b) {
	return (parseFloat(a) <= parseFloat(b)?'min':'');
}


function fetchJSONFile(path, callback) {
       var httpRequest = new XMLHttpRequest();
       httpRequest.onreadystatechange = function() {
           if (httpRequest.readyState === 4) {
               if (httpRequest.status === 200) {
                 spydata = JSON.stringify(httpRequest.responseText);
                   var data = JSON.parse(httpRequest.responseText);
                   if (callback) callback(data);
               }
           }
       };
       httpRequest.open('GET', path);
       httpRequest.send();
   }





$(function(){
	filter();

$('.search').on('keyup',function() {
filter();
});

$('.fam , .loc').on('change',function(){
filter();
});

// ORDER
$('[data-order]').on('click',function(){
var neworder = $(this).attr('data-order');
if (orderBy == neworder) asc = (asc == 'DESC'?'ASC':'DESC');
orderBy = neworder;
filter();
$('[data-order]').removeClass('order');
$('[data-order="'+neworder+'"]').addClass('order');

});


Mousetrap.bind('del', function() {
var selectedElem = $('tr.curr').attr('data-pid');
if (selectedElem > 0 ) {
  $.ajax({
       url : 'ajax/stock_unlike.php?pid='+encodeURIComponent(selectedElem),
       type : 'GET',
       success : function(xz){
           if (xz == '1'){
           	$('[data-pid="'+selectedElem+'"]').addClass('animated').addClass('fadeOutRight');
           	setTimeout(function(){filter();}, 800);
           }
       }
    });

	}
 });



});


$(function(){
var windowHeight = window.screen.availHeight;
var z = document.getElementById('res').getBoundingClientRect().top;
var resH = parseInt(windowHeight) - parseInt(z) ;
document.getElementById('res').style.height =  resH  + 'px';
console.log(windowHeight + '  ' + resH);

});
