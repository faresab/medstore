var loadingIcon = '<i class=" fa fa-spin fa-spinner" style="margin-left:50%;margin-top:10%;"></i>';
var magname = null;


function RandomInt(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


$(function(){
      $('.number').number( true, 2,'.',' ' );

$.ajaxSetup ({
    cache: false
});

wp();
});

function wp(){
  if($('input[name="wp"]').is(':checked')) {
    $('.perm').slideDown('slow');
    $('.av-exp,.date-exp').prop("required", true);
  }
$('input[name="wp"]').change(function() {
    if($('input[name="wp"]').is(':checked')) {
    $('.perm').slideDown('slow');
            $('.av-exp,.date-exp').prop("required", true);
  } else{
        $('.perm').slideUp('slow');
    $('.av-exp,.date-exp').removeAttr('required');
  }
});
}

function save(){
  $('.addsubmit').toggleClass('btn-primary','btn-success').attr('disabled','disabled');
$.ajax({
      type: $('#addform').attr('method'),
      url: $('#addform').attr('action'),
      data: $('#addform').serialize(),
      success: function(data) {
        
        $('.modal-body').html(data);
        $('#dialog').modal();
refhome($('.filter').val());
        $('.addsubmit').toggleClass('btn-primary','btn-success').removeAttr('disabled');

      }
    });
    return false;
}



function checkwp(tof){
if (tof == 1) {
  $('input[name="wp"]').prop( "checked", true );
} else {
  $('input[name="wp"]').prop( "checked", false );
}
wp();
}

function printhat(ref,qt,price,barcode) {
  var link = 'bcp.php?bc='+barcode+'&pr='+price+'&ref='+ref+'&qt='+qt ;
 $('.printbtn').fadeIn('slow');
  $('.printbtn').children('a').attr('href',link);

 return false;
}

var refhome = function(xz) {
$('.tblstock').html(loadingIcon).load('ajax/stock_list.php?key='+encodeURIComponent(xz) ,function() {
$('.trash').on('click',function() {
var rel = $(this).attr('data-rel');
$(this).parents('tr').addClass('danger');
$(this).html('<i class=" fa fa-spin fa-spinner"></i>').load('ajax/trash.php?rel='+rel,function(){
$(this).parents('tr').slideUp('slow');
});
return false;
});
$('[data-toggle="tooltip"]').tooltip();


$('.printf').on('click',function(){
var datainfos = $(this).attr('data-infos').split(',');
var pprix = datainfos[2];
  $('.printing2').html('');
$('.pproduct').text(datainfos[1]);
$('.prprice').html(pprix + " DA");
$(".bc").barcode(datainfos[0] , "code128",{barHeight:30});     
window.print();
});

$('.printtbl').on('click',function(){
  var htmltable = $('.tblstock').html();
  $('.pproduct,.prprice').html('');
$('.printing2').html(htmltable);
window.print();
});


});
};

$(function() {
  $('.filter').focus();
$(refhome(''));
$('a[href="#home"]').on('click',function(){

   refhome('');
  window.setTimeout(function(){
  $('.filter').val('').focus();
}, 400);


});
$('.filter').on('keyup',function(){
refhome($(this).val());
});
});




$(function(){

$('body').bind('keydown',function(e){
if (e.keyCode == 13) {
  return false;
}
});






$('#addform').on('submit',function(){
save();
return false;
  });

	  });



Mousetrap.bind('f3', function() { $('#addform').submit();return false; });




$(function(){
	$('#randombc').on('click',function() {
var target = $('.bcbc');
var x = RandomInt(1000000000000,9999999999999);

if (target.val() === '') {
target.val(x);
} else {
var r = confirm("Changer le code-barre");
if (r === true) {
target.val(x);
} else {
}


}
return false;
});
});


$(function() {
$.get("ajax/infos.php",function(data){
var infos = data.split(',');
$('title').prepend(infos[0] + ' - ');
$('.magname').text(infos[0]) ;
$('.magaddr').text(infos[1]) ;
});





$('input[name="REF"] , input[name="BARCODE"]').focusout(function(){
var bcc = $(this).val();
var reff = $('input[name="REF"]').val();
$('#null').load('ajax/vstock.php?bc='+bcc+'&ref='+reff , function(){

});

});
});


function clearInputs(){
  $('input').each(function(){
$(this).val('');
  });
}


