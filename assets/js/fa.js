var pid = product = ref = null;
var total = sommereste = sommeregle = 0;
var regle = false;
var prev_qt = prev_pa = 0;
var solde_init = solde = 0;

$(function(){
    $('.fourniseur').selectpicker();

  $.ajaxSetup ({
    cache: false
  });
  });


function clearForms(){
  pid = product = ref = prix = null;
  $('.qt').val('1');
  $('.product,.prix').val(null);
  $(".suggestionsBox").slideUp("fast");
}


// ad to table

function addToForm(x){
var row = x.split(';');
pid  = row[0];
ref  = row[1];
product  = row[2];
prix  = row[3];
prev_pa  = row[3];
prev_qt  = row[4];
$(".suggestionsBox").slideUp("fast");
$('.product').val(product);
$('.prix').val(prix);
$('.qt').val(1).focus();
}


function addToTable(){
if (pid != null) {
if ($('tr[data-id="'+pid+'"]').length >= 1) removeItem(pid);
var qtt = parseFloat($('.qt').val());
var pr = parseFloat($('.prix').val());
var mnt = qtt * pr;
var htmltr = "<td>"+pid+"</td><td>"+ref+"</td><td>"+product+"</td><td class='text-right'>"+money(pr)+"</td><td class='text-center'>"+qtt+"</td><td ref='mnt' class='text-right'>"+money(mnt)+"</td>";
var input = "<input type='hidden' rel='"+pid+"' name='prod_"+pid+"' value='"+pr+";"+qtt+";"+product+";"+ref+";"+prev_pa+";"+prev_qt+"' />";
$('tr[data-id="0"]').first().html(htmltr).attr('data-id',pid);
$('#pinputs').append(input);
  $('.product').focus();

clearForms();
countTotal();
updateReste();
}
}


function autoAddToTable(autopid,autoref,autoproduct,autoqt,autopr,automnt,autopa){
var htmltbl = "<td>"+autopid+"</td><td>"+autoref+"</td><td>"+autoproduct+"</td><td class='text-right'>"+money(autopr)+"</td><td class='text-center'>"+autoqt+"</td><td ref='mnt' class='text-right'>"+money(automnt)+"</td>";

var input = "<input type='hidden' rel='"+autopid+"' name='prod_"+autopid+"' value='"+autopr+";"+autoqt+";"+autoproduct+";"+autoref+";"+autopa+"' />";
$('tr[data-id="0"]').first().html(htmltbl).attr('data-id',autopid);
$('#pinputs').append(input);
countTotal();
updateReste();
}


function removeItem(Ipid) {
  if (parseInt(Ipid) != 0) {
$('tr[data-id="'+Ipid+'"]').remove();
$('input[rel="'+Ipid+'"]').remove();
countTotal();
}
var xreg = parseFloat($('input[name="regle"]').val());
if (xreg > total) $('input[name="regle"]').val(total);
updateReste();
setTimeout(function(){ $('.product').focus(); }, 300);
}

Mousetrap.bind('f6', function() {
$('select').focus();
 });

Mousetrap.bind('f1', function() {
$('.regle').val(total).focus().select();
updateReste();
 });
Mousetrap.bind('f2', function() {
$('.regle').val('0');
$('.reste').focus().select();
updateReste();
 });

Mousetrap.bind('f3', function() { save(); });
Mousetrap.bind('f4', function() {
var link = $('.printbtn').attr('href');
var linktarget = $('.printbtn').attr('target');
window.open(link,linktarget);
 });






$(function(){
$('tr[data-id]').on('dblclick',function(){
  var this_id = $(this).attr('data-id');
  removeItem(this_id);
});
});


$(function(){
  $('.product').focus();
$('.qt').on('keypress',function(e){
    if (e.which == 13) {
addToTable();
return false;
    }
  });
  });



function countTotal(){
	var ctot = 0;
$('td[ref="mnt"]').each(function(){
var thismont = parseFloat($(this).text().replace(' ','').replace(' ',''));
ctot = ctot + thismont;
});
total = ctot;
$('.total').text(money(total));
$('input[name="total"]').val(total);
}




function UpdateSolde(){
solde_init = parseFloat($('.fourniseur option:selected').attr('data-solde'));
sommereste = parseFloat($('.reste').val());
solde = solde_init + sommereste;
$('.solde').text(money(solde_init));
$('.soldeMaj').text(money(solde));
}

function updateReste(){
sommereste = total - parseFloat($('.regle').val());
$('.reste').val(sommereste);
UpdateSolde();
}

function updateRegle(){
sommeregle = total - parseFloat($('.reste').val());
$('.regle').val(sommeregle);
UpdateSolde();
}

function setPid(number){
pid = number;
$('input.pid').val(number);
$('.printbtn').attr('href','printfa.php?fid='+pid).attr('target','blank');
}


$(function(){
$('.regle').on('input', function(){
	updateReste();
});
$('.reste').on('input',function(){
	updateRegle();
});
});


$(function(){
UpdateSolde();
$('.fourniseur').on('change',function(){
UpdateSolde();
});


$('#toutreg').click(function() {
    var $this = $(this);
    if ($this.is(':checked')) {
    	regle = true;
$('.regle').val(total);
$('.reste').val('0');
 } else {
 	regle = false;
$('.regle').val('0');
$('.reste').val(total);
    }
    UpdateSolde();
});
});




$(function(){
$('.number').number( true, 2,'.',' ' );
$('.product').sugg({
requested_page: 'fastsearch/fa-sugg.php',
requested_key: 'q',
});

$('.product').arrawUD(
function(sel){
    var stex = sel.text();
    var sid = sel.attr('data-id');
   addToForm(sid );
}
);
});



function save() {
var fname = $('.fourniseur option:selected').val();
if (fname == undefined) {
  alert('selectionner un fournisseur');
} else {

    $('.save').toggleClass('btn-primary btn-default').attr('disabled','disabled');
$.ajax({
      type: 'POST',
      url: 'ajax/fa-submit.php',
      data: $('#addform').serialize(),
      success: function(data) {
  $('.save').toggleClass('btn-primary btn-default').removeAttr('disabled');
alert2('BON DE LIVRAISON  A ETE AJOUTE',data);

      }
    });

}
    return false;
}



function alert2(title,msg){
$('h4.modal-title').html(title);
$('.modal-body').html(msg);
$('.dialog').modal();
}



$(function(){
$('form#addform').on('submit',function(){
  save();return false;
});

});









/*
.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")

*/

function money(x) {
    return parseFloat(x).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ");
}



$(function(){
var windowHeight = $(window).height();
$('#res').css('height',(windowHeight - 250));
});
