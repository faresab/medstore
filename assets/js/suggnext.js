$(function(){
$.fn.sugg = function(options) {
	var $this = $(this);
	var inp_width = $(this).parent().width();
	var suggb = $this.nextAll('.suggestionsBox:first');
	var defaults = {
                requested_page: 'sugg.php',
                requested_key: 'q',
            };
options = $.extend(defaults, options);
suggb.width(inp_width);
$this.on('input', function(e){

 if( $this.val().length >= 1 ) { 
      $.ajax({
  	type : 'GET', 
	url : options.requested_page , 
	data : options.requested_key +'='+$this.val() ,
	beforeSend : function() { 
	},
	success : function(data){ 
	suggb.show('fast');
	suggb.children('.suggestionList').html(data);
	}
      });
    } else {
	suggb.slideUp('slow');
	}

});
}
});













