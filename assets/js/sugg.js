$(function(){
$.fn.sugg = function(options) {
	var $this = $(this);
	var defaults = {
                requested_page: 'sugg.php',
                requested_key: 'q',
            };
options = $.extend(defaults, options);

$this.on('input', function(e){
 if( $this.val().length >= 1 ) {
      $.ajax({
  	type : 'GET',
	url : options.requested_page ,
	data : options.requested_key +'='+$this.val() ,
	beforeSend : function() {
	},
	success : function(data){
	$('#suggestions').show('fast');
	$('#autoSuggestionsList').html(data);
	}
      });
    } else {
	$('#suggestions').slideUp('slow');
	}

});
}
});














$.fn.arrawUD = function (callback) {
var $that = $(this);
	$('body').on('keyup', function(e){
	if ($('#suggestions').is(':visible') ){
	var current = $('.suggestionList li.active');
if (e.keyCode==40 ) {
if( current.length  >= 1){
$('.suggestionList li').removeClass('active');
	current.next('li').addClass('active');
	if ($('.suggestionList li.active').length == 0) $('.suggestionList li').last().addClass('active');
} else{
$('.suggestionList li').first().addClass('active');
}

return false;
} else if(e.keyCode==38) {
if( current.length  >= 1){
$('.suggestionList li').removeClass('active');
current.prev('li').addClass('active');
if ($('.suggestionList li.active').length == 0) $('.suggestionList li').first().addClass('active');
} else{
$('.suggestionList li').last().addClass('active');
}

return false;

} else if(e.keyCode==13) {
	var sel = $('.suggestionList li.active');
callback(sel);
}
}
});

}
