var loadicon = '<span style="position:relative;top:100px;left:46%;"><i class="fa fa-spin fa-spinner" ></i>  chargement..</span>';
   $.fn.animateNumbers = function(stop, commas, duration, ease) {
        return this.each(function() {
            var $this = $(this);
            var start = parseInt($this.text().replace(/ /g , ""));
            commas = (commas === undefined) ? true : commas;
            $({value: start}).animate({value: stop}, {
                duration: duration == undefined ? 1000 : duration,
                easing: ease == undefined ? "swing" : ease,
                step: function() {
                    $this.text(Math.floor(this.value));
                    if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00');
                                }
                },
                complete: function() {
                   if (parseInt($this.text()) !== stop) {
                       $this.text(stop);
                       if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1 ")+'.00' );
}
}
}
            });
        });
    };



var stocklist , hids , ventes;
 $(document).ready(function() {
moment.lang('fr');
                  $('#daterange').daterangepicker();
               });





           $(document).ready(function() {
                  $('#reportrange').daterangepicker(
                     {
                        startDate: moment(),
                        endDate: moment(),
                        minDate: '01/01/2012',
                        maxDate: '12/31/2026',
                        dateLimit: { days: 1360 },
                        showDropdowns: true,
                        showWeekNumbers: true,
                        timePicker: false,
                        timePickerIncrement: 1,
                        timePicker12Hour: true,
                        ranges: {
                           'aujourd\'hui': [moment(), moment()],
                           'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],
                           'Les 7 derniers jours': [moment().subtract('days', 6), moment()],
                           '30 derniers jours': [moment().subtract('days', 29), moment()],
                           'ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                           'Le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                        },
                        opens: 'right',
                        buttonClasses: ['btn btn-default'],
                        applyClass: 'btn-small btn-info',
                        cancelClass: 'btn-small',
                        format: 'MM/DD/YYYY',
                        separator: ' A ',
                        locale: {
                            applyLabel: 'OK',
                            fromLabel: 'de',
                            toLabel: 'A',
                            customRangeLabel: ' personnaliser',
                            daysOfWeek: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
                            monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                            firstDay: 1
                        }
                     },
                     function(start, end) {
                      console.log("Callback has been called!");
                      $('#reportrange').val(start.format(' dddd D MMMM YYYY') + ' jusq\'a ' + end.format('dddd D MMMM YYYY'));
                      $('#reportrange').attr('data-start',start.format('YYYYMMDD'));
                      $('#reportrange').attr('data-end',end.format('YYYYMMDD'));
                     $(xdate);
                     }
                  );
                  //Set the initial state of the picker label
                  //$('#reportrange').val(moment().subtract('days', 29).format('D MMMM YYYY') + ' - ' + moment().format('D MMMM YYYY'));
                  $('#reportrange').val(moment().format('dddd D MMMM YYYY'));
               $(xdate);
               });



var xdate = function(){
var startdate = $('#reportrange').attr('data-start');
var endtdate = $('#reportrange').attr('data-end');
$('.jumbotronmid .row').html(loadicon).load('ajax/totaltable.php?start='+startdate+'&end='+endtdate , function(){
$('a[href="#det"]').on('click',function(){
// $('.details').html('chargement..').load('ajax/details.php?start='+start+'&end='+end);
fetchJSONFile('ajax/detailsjson2.php?start='+start+'&end='+end , function(x){
  stocklist = x.stock;
  ventes = x.ventes;
  hids = x.hids;
creatTable();
//console.log(ventes[48]);
});
});

});

return false;
}





var tbl = '';
var sumSr , remiseTotal = 0;
var totalDetails = {sumrem : 0, sumSansrem : 0}
function creatTable(){
  sumSr , remiseTotal = 0;
  totalDetails = {sumrem : 0, sumSansrem : 0}
tbl = '';

if (hids && hids.length) {
for (var i = 0; i < hids.length; i++) {
  tbl += '<table class="table table-bordered">';
 getHidDetailsFromVentes(ventes[hids[i].ID]);
 var hidRemise =  sumSr  - parseFloat(hids[i].VALUE) ;
  tbl += '<tr class="orange"><td>'+hids[i].USER+'</td><td></td><td></td><td right>'+fm(sumSr)+'</td><td right><b >'+fm(hids[i].VALUE)+'</b></td><td right>'+fm(hidRemise)+'</td></tr></tabl>';
totalDetails.sumrem += hidRemise ;
totalDetails.sumSansrem += sumSr ;
}

}
var remporcent = (totalDetails.sumSansrem == 0 ? 0 : Math.round(totalDetails.sumrem * 100 / totalDetails.sumSansrem) );
var inhtml = '<table class="table table-bordered"><tr><td>Vente au comptoir (sr)</td><td class="text-right">'+fm(totalDetails.sumSansrem)+'</td></tr><tr><td>Remise</td><td class="text-right">'+fm(totalDetails.sumrem)+' ('+remporcent+'%)</td></tr></table>' + tbl;
if (tbl.length) document.querySelector('.details').innerHTML = ''+inhtml+'</table>' ;




}





function getHidDetailsFromVentes(v) {
  var sum = 0 , remise = 0;
if (v && v.length) {
for (var i = 0; i < v.length; i++) {
  sum += parseFloat(v[i].PRICE);
  tbl += '<tr><td>'+v[i].PRODUCT+'</td><td>'+v[i].DATETIME+'</td><td></td><td>'+v[i].QT+'</td><td right p20>'+fm(v[i].PRICE)+'</td><td></td></tr>';
}


}


sumSr = sum;
}























function fm(Money) {
	return parseFloat(Money).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$& ');
}

function fetchJSONFile(path, callback) {
   var httpRequest = new XMLHttpRequest();
   httpRequest.onreadystatechange = function() {
       if (httpRequest.readyState === 4) {
           if (httpRequest.status === 200) {
             spydata = JSON.stringify(httpRequest.responseText);
               var data = JSON.parse(httpRequest.responseText);
               if (callback) callback(data);
           }
       }
   };
   httpRequest.open('GET', path);
   httpRequest.send();
}
