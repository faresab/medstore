var loadingIcon = '<i class=" fa fa-spin fa-spinner" style="margin-left:50%;margin-top:10%;"></i>';
moment().local();

moment.lang('fr');

function fromnowmoment(){
$('.date').each(function(){
var date = $(this).text();
var showFromnow = moment(date , "YYYY-MM-DD HH:mm").fromNow();
var now = moment().format('YYYY-MM-DD HH:mm');
$(this).html('<span title="'+now+' '+ date+'">'+showFromnow+'</span>');
});
}

function vtrash(selector){
	var id = $(selector).attr('data-id');
	$('.modal-body').load('ajax/vtrash.php?id='+id);
}

 function vkill(){
var rest = $('.restkill').val();
var id = $('.versid').text();
$('.modal-body').html(loadingIcon).load('ajax/vkill.php?id='+id+'&rest='+rest );

//alert(rest + '  '+id)

}


function countall(numb){

$('#total').text(numb);
}


function fetchJSONFile(path, callback) {
   var httpRequest = new XMLHttpRequest();
   httpRequest.onreadystatechange = function() {
       if (httpRequest.readyState === 4) {
           if (httpRequest.status === 200) {
             spydata = JSON.stringify(httpRequest.responseText);
               var data = JSON.parse(httpRequest.responseText);
               if (callback) callback(data);
           }
       }
   };
   httpRequest.open('GET', path);
   httpRequest.send();
}


var details ;
fetchJSONFile('ajax/jsonvers.php',function(z){
details = z ;
console.log(details);
});


function runsearch(sel) {
	var tbl = '';
var rel = sel.getAttribute('rel');
if (details && details.length) {

for (var i = 0; i < details.length; i++) {
	if (rel == details[i].HID) tbl+= '<tr hiddenrel="'+rel+'" ><td>'+details[i].REF+'</td><td>'+details[i].PRODUCT+'</td><td>'+details[i].PRICE_V+'</td><td>'+details[i].QT+'</td><td colspan="5"> </td></tr>';
}
}
  sel.outerHTML  = tbl + sel.outerHTML;
}


function openmodal(z) {

	var hid  = z.getAttribute('data-hid');
	var rst  = z.getAttribute('data-rest');
$('.modal-body').load('ajax/vmodal.php?id='+hid+'&rest='+rst,function(){


});
}

 function search(){
	var key = $('.search').val();
$('#liste').html(loadingIcon).load('ajax/vlist.php?q='+encodeURIComponent(key) ,function() {
	var trs = document.querySelectorAll('tr[rel]');
	console.log(trs);
	if (trs && trs.length) {
	for (var i = 0; i < trs.length; i++) {
		//trs[i]
		runsearch(trs[i]);
	console.log(trs[i]);
	}

	}


	fromnowmoment();

});


	$('.addv').on('click',function(){
		addtovers();
	});

}



function resetInput(){
$('.ref,.vers,.name,.comm').val('');
$('.ref').focus();
}




$(function(){
$(search());
$('.search').on('keyup',function(){
	search();
});

});



function addtovers(){
var ref = encodeURIComponent($('.ref').val());
var vers = encodeURIComponent($('.vers').val());
var name = encodeURIComponent($('.name').val());
var comm = encodeURIComponent($('.comm').val());
$('.modal-body').load('ajax/vadd.php?ref='+ref+'&vers='+vers+'&name='+name+'&comm='+comm ,function(){
$('#Modal').modal();
});
}


$(function(){
$('input').first().focus();
Mousetrap.bind('f3', function() { addtovers(); });

});
